##############################################################################
#                        Land Valuation Functions                            #
##############################################################################
# For the input valuationdata, modeling group and fitted value (input_col) need
# to exist, and the fitted values have to be not null in every row

land_valuation_grid <- function(df, group_cols, modeling_vals, land_val_method, input_col,
                                no_na_cols=c(input_col), not_all_na_cols = c()){
  # This function provides the estimation of land value for every group and condition specified
  
  # Inputs:
  #     df: (dataframe) valuationdata
  #     group_cols: (vector of strings) vector of columns used as the identification of group in land valuation
  #     modeling_vals: (vector of strings) specific values which is used in the modeling group
  #     land_val_method: (string) "l_8020" by default
  #     input_col: (string) column name to be used as the inputted fitted value
  #     no_na_cols: (list of strings) the columns required to be no null value in it
  #     not_all_na_cols: (list of strings) the columns required to have at least one non-null value in it 
  # Return: (dataframe) updated valuationdata or 
  #         (boolean) this function will return FALSE, if there is NA value in no_na_cols or 
  #                   there is no non-NA values in not_all_na_cols
  
  # Example:
  #     group_cols <- c("TOWN_CODE", "NBHD")
  #     modeling_vals <- c("SF", "MF")
  #     land_val_method <- "l_8020"
  #     test_val <- land_valuation_grid(valuationdata, group_cols, modeling_vals, land_val_method, input_col="fitted_value_4")  
  # The function will make land valuation for every group (with every possible combination of neighborhood, town)
  # with the median calcuated by the cases which satisfies the pre-decided conditions (modeling group is "single family(SF),
  # or "multi-family(MF)")
  
  # In this function, we will generate a group ID for each unique combination of grouping variables specified by
  # group_cols. For example, if no arguments are specified in group_cols, the function will perform the valuation 
  # for the entire data set. If TOWN_CODE is specified, the adjustment will be performed for each township in the data.
  # At the moment of deployment, the valuation was performed within TOWN, NBHD.
    
  # checking if the columns for grouping exists
  current_group_cols <- c()
  for (column in group_cols){
    if (column_existance(df, column)){current_group_cols <- c(current_group_cols, column)}}
  
  # checking if the modeling value in modeling_vals exists
  current_modeling_vals <- c()
  for (modeling_val in modeling_vals){
    if (modeling_val %in% unique(df$modeling_group)){
      print(paste0("modeling val: ", paste(modeling_val), " in modeling group column exists"))
      current_modeling_vals <- c(current_modeling_vals, modeling_val)}
  modeling_vals <- current_modeling_vals
  print(paste0("finally using modeling_vals: "))
  print(modeling_vals)}
     
  # checking if all the values in the non-na columns is not null
  for (no_na_col in no_na_cols){
    if (!value_existance(df, no_na_col, restriction="no_na")){
      return(FALSE)}}
  for (not_all_na_col in not_all_na_cols){
    if (!value_existance(df, not_all_na_col, restriction="not_all_na")){
      return(FALSE)}}
  
  # choosing method for the land valuation
  if (land_val_method == "l_8020"){
    df <- land_valuation_md1(df, current_group_cols, modeling_vals, input_col)}
    
  if (land_val_method == "XXXX"){
    df <- land_valuation_md2(df)}
  
  return(df)
}


land_valuation_md1 <- function(df, current_group_cols, modeling_vals, input_col){
  # This function provides the outer loop to determined the predetermined conditions and
  # grouping conditions for the land valuation. This function will be used as the part of
  # the grid function
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    current_group_cols: (vector of strings) existing columns uses for grouping purpose
  #    modeling_vals: (vector of strings) specific values which is used in the modeling group
  #    input_col: (string) column name to be used as the inputted fitted value
  # Return:
  #    df: (dataframe) updated valautiondata
  
  # Example:
  #    df <- land_valuation_md1(df, current_group_cols, modeling_vals, input_col)
  
  # setting predetermined condition for for modeling_vals
  condition_cols <- rep("modeling_group", length(modeling_vals))
  conditions <- list()
  index <- 1
  for (val in modeling_vals){
    condition <- ifelse(df[, "modeling_group"] == val, TRUE, FALSE)
    conditions[[index]] <- condition
    index <- index + 1}
  pred_cond <- predetermined_condition(df, condition_cols, conditions, method="or")
  
  # detecting group_cols which does not exist
  if (length(current_group_cols) >= 1){
    print("the final group conditions used for processing is: ")
    print(current_group_cols)
    group_cols <- current_group_cols
    df <- df %>% mutate(group = group_indices(df, !!!syms(group_cols)))}
  else{
    print("the final group conditions used for processing is the full dataset")
    df$group <- 1}
  
  for (group in unique(df$group)){
    print(paste0("Currently processing group: ", paste(group)))
    df <- land_valuation_process(df, group, pred_cond, input_col)}
  
  return(df)
}


land_valuation_process <- function(df, group_val, pred_cond, input_col){
  # This is a inner function for land valuation in the given group which satisfies the predetermined
  # conditions. This function will be used as the part of the outer function for land valuation
  
  # Inputs:
  #    df: (dataframe) valuationdata
  #    group_val: (integer) specific group value determined in the outer function
  #    pred_cond: predetermined condition for the selected rows
  #    input_col: (string) column name to be used as the inputted fitted value
  # Return:
  #    df: (dataframe) updated valautiondata
  
  # Example:
  #    df <- land_valuation_process(df, group, pred_cond, input_col)

  group_cond <- ifelse(df$group == group_val, TRUE, FALSE)
  if(is.na(median(subset(df, group_cond & pred_cond)[, input_col], na.rm = TRUE))){
    print(paste0("The group has n = ", nrow(subset(df,  group_cond))))
    next}

  else{
    median_fv <- median(subset(df, group_cond & pred_cond)[, input_col])
    median_lot_size <- median(subset(df, group_cond & pred_cond)$HD_SF)
    
    print(paste0("Median fitted value in group ", group_val, " is ", round(median_fv,2)))
    print(paste0("Median lot size in group ", group_val, " is ", round(median_lot_size,2)))
    print(paste0("Median $/sf in group ", group_val, " is ",median_fv/median_lot_size*.2 ))
    print(paste0("Median $/sf in group ", group_val, " is ", round(median_fv/median_lot_size*.2/.25)*.25))
    
    TF <- group_cond & pred_cond
    TF2 <- 0.2*(round(median_fv/median_lot_size*.2/.25)*.25)*df$HD_SF <= 0.5*(df[, input_col])
    df$land_value[TF & TF2] <- 0.2*(round(median_fv/median_lot_size*.2/.25)*.25)*df$HD_SF[TF & TF2]
    df$land_value[TF & !TF2] <- 0.5*(df[, input_col][TF & !TF2])
    
    # TRUE/FALSE 1 - Property in target subgroup?
    TF <- group_cond & df$CLASS == 299
    # TRUE/FALSE 2 - Would land value be greater than half the value of the unit?
    TF2 <- 0.2*(round(median_fv/median_lot_size*.2/.25)*.25)*df$HD_SF*df$PER_ASS <= 0.5*(df[, input_col])
    # If Yes, no, then calculate land value by 80/20 rule
    df$land_value[TF & TF2] <- 0.2*(round(median_fv/median_lot_size*.2/.25)*.25)*df$HD_SF[TF & TF2]*df$PER_ASS[TF & TF2]
    # If Yes Yes then land value is 50% of previous fitted value.
    df$land_value[TF & !TF2] <- 0.5*(df[, input_col][TF & !TF2])
    
    TF <- group_cond & df$CLASS %in% c(200,201,241)
    df$land_value[TF] <- 0.2*(round(median_fv/median_lot_size*.2/.25)*.25)*df$HD_SF[TF]
    
    print(paste0(nrow(subset(df, !is.na(land_value))), " non blank land vals"))
  }
  return(df)
}


land_valuation_md2 <- function(df){
  # Might need to assume that models, final_nchars_model, final_df_model dataframe and columns modeling_group, 
  # land_value, FOCF1 exist in df
  if(any(grepl(c("HD_SF"), unlist(names(coef(final_nchars_model)))))==TRUE){
    i <- which(grepl(c("HD_SF"), unlist(names(coef(final_nchars_model)))))
    if(any(grepl(c("log(sale_price)"), models[5]))==TRUE & any(grepl(c("log(HD_SF)"), models[5]))==FALSE){
      print("log-lin")
      df$land_value[df$modeling_group=="NCHARS"] <- subset(df, modeling_group=="SF")$land_value+
        10^(coef(final_nchars_model)[i]*subset(df, modeling_group=="NCHARS")$LOCF1)
    }
    if(any(grepl(c("log(sale_price)"), models[5]))==FALSE & any(grepl(c("log(HD_SF)"), models[5]))==FALSE){
      print("lin-lin")
      df$land_value[df$modeling_group=="NCHARS"] <- subset(df, modeling_group=="SF")$land_value+
        coef(final_nchars_model)[i]*subset(df, modeling_group=="NCHARS")$LOCF1
    }
  }
  # SF Land location factor
  if(any(grepl(c("LOCF"), unlist(names(coef(final_sf_model)))))==TRUE){
    i <- which(grepl(c("LOCF"), unlist(names(coef(final_sf_model)))))
    if(any(grepl(c("log(sale_price)"), models[5]))==TRUE & any(grepl(c("log(LOCF1)"), models[5]))==FALSE){
      df$land_value[df$modeling_group=="SF"] <- subset(df, modeling_group=="SF")$land_value+
        10^(coef(final_sf_model)[i]*subset(df, modeling_group=="SF")$LOCF1)
    }
    if(any(grepl(c("log(sale_price)"), models[5]))==FALSE & any(grepl(c("log(LOCF1)"), models[5]))==FALSE){
      df$land_value[df$modeling_group=="SF"] <- subset(df, modeling_group=="SF")$land_value+
        coef(final_sf_model)[i]*subset(df, modeling_group=="SF")$LOCF1
    }
  }
  
  # For neighborhood codes, need a model matrix of dummies
  if(any(grepl(c("TOWN_CODE:NBHD"), unlist(names(coef(final_sf_model)))))==TRUE |
     any(grepl(c("NBHD:TOWN_CODE"), unlist(names(coef(final_sf_model)))))==TRUE ){
    test <- model.matrix(~ NBHD:TOWN_CODE, data=df)
    
  }
  help(model.matrix)
}

group_cols <- c("TOWN_CODE", "NBHD")
modeling_vals <- c("SF", "MF")
land_val_method <- "l_8020"
test_val <- land_valuation_grid(valuationdata, group_cols, modeling_vals, land_val_method, input_col="fitted_value_4")