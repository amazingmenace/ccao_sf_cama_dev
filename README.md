
# How This Code Powers the Cook County Residential CAMA System

The duty of the Cook County Assessor is to value property in a fair and accurate way. The Assessor is committed to transparency throughout the assessment process, including online publishing of the code used to assess property values. This repository houses the code for the Computer Assisted Mass Appraisal (CAMA) system used to produce initial property values - the ones that are printed on your Residential Reassessment Notices you receive in the mail when your property is reassessed - for residential single-family homes and homeowner-owned condominiums in Cook County.   

The Cook County property assessment system divides the county into three triads (or regions). The parcels in each triad are assessed once every three years. We are re-assessing the North Suburbs of the county in 2019. The South Suburbs will be reassessed in 2020, and the City of Chicago in 2021. Each triad is further separated into townships, and each of those are divided into neighborhoods. The reassessment process follows a township-by-township basis, so each township is reassessed - and modeled - independently.

The Mass Appraisal method uses large amounts of data to estimate property values for all single-family and condominium residential properties in Cook County.


### The basic idea is this:

What would be a fair and accurate property value for a home if it had sold last year?

Because sale values of homes are affected by their characteristics - such as its square footage, age, and location - we use statistical modeling to use those characteristics to produce property values for each home. 

The Assessor's estimate of your home's property value is based on two things: your home's characteristics, and patterns between how lots of other homes' characteristics affected their sale values.

### How it works in detail:

Each township (for example, Evanston) is assessed separately, so the following steps happen on a township-by-township basis.

First, the code gathers the sale values and characteristics of homes in your township and similar townships that sold in the last five years. We include similar townships because the CAMA method depends on having a lot of data. For Evanston, we used home sales in Evanston, New Trier, and Rogers Park. We check for and remove any outlier sales.

Then the code tests to see whether there are consistent patterns. Smaller homes tend to have lower sale values and larger homes tend to have higher sale values -- but how do we quantify that? The code detects patterns in this data to mathematically estimate the dollar amounts of how each of these characteristics may have pushed sale values up or down. 

As purely hypothetical examples of three characteristics, each +1 square foot for a building might add \$50 in value, each +1 year in age might subtract \$500 in value, and having a porch might add \$5000 in value. Doing this is possible only when we have a sufficiently large data set with thousands of homes so that the code can, for example, look at sale values for homes with smaller square footage versus larger square footage to see what the pattern between square footage and sale value is. 

In all cases the code tries different methods to calculate how each of these characteristics, and how combinations of characteristics, are associated with changes in sale values. (Note that sometimes a change in a characteristic might not be associated with a change in value for a given township, and the code can detect linear and non-linear patterns.) 

Each of these characteristic estimates is part of a **statistical model** (a long equation) that adds up the values of each of these characteristics to produce a home's total fair market value. 

The code uses different ways to create a variety of different equations to see which one produces the most fair and accurate estimates in accordance with international standards for best practices for fair assessment.

Once we have this statistical model, we use it to estimate the values of all individual homes - those with and without a recent sale. 

Condominiums are valued somewhat differently for two reasons. First, we don't currently have nearly as much data about their characteristics, and second, because condos are percentages of buildings instead of whole buildings. (Your condominium's Articles of Incorporation states the exact percentage of building ownership for each unit.) So we must first determine the total value of the building, and then calculate the condo's value based on its percentage of ownership of the building it's in.

So far, we've produced estimates of fair market values for each home and each condominium in its neighborhood in that township. But we're still not done. Our code detects if the estimate for your home should be double-checked, like if your home's new estimated property value is drastically different from others. When this happens, information about this home is sent to one of the Office's analysts so that they can review your home individually and make any adjustments as needed.

After our analysts complete all reviews and update these property values, the initial values for that township are complete and a reassessment notice with their home's initial value is mailed to each homeowner. 

This repository houses the code that does the above. This code is provided as-is, and is under continual development as our Data Science Department refines improvements for fair, accurate, and transparent property assessments. As an example, updates have added new characteristics such as whether a home is on a floodplain, and whether it is affected by noise at O'Hare Airport. You can watch our refinements in real time here on GitLab.
