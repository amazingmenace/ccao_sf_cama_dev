# This script defines an object called a 'scope.' This object holds the replication data for the
# modeling proceedure used by the CCAO.

S_new <- function(townships=NULL,doload=TRUE) {
	# Check against list of townships. Stop if you have asked the function to operate on a township that
  # is not in the list of townships in the codebook.
	if (is.null(townships) || !all(townships%in%CODE$TOWNSHIPS$Township))
		stop("Invalid input");

	U_msg("[S_new] Creating new scope object\n");
	U_msg("\tIncluding ",length(townships)," township",ifelse(length(townships)==1,"","s"),": ",paste(townships,collapse="+"),"\n");
	obj <- list(townships=townships);
	
	U_msg("\tLocating raw files...\n");
		code <- subset(CODE$TOWNSHIPS,Township%in%townships);
		obj$RAWLOC <- list(
			DIR=paste0(dirs$data_pilot,code$HAL_Directory),
			FILES=list(MV=code$MV,
						REGT=code$REGT)
		);
		
	class(obj) <- "scope";

	if (!doload) {
		U_msg("\nCall DI(obj) to start loading data\n");		
	} else {
		obj <- DI(obj);
	}
	
	return(obj);
}
S_towncode <- function(township) {
	stopifnot(township%in%CODE$TOWNSHIPS$Township);
	return(subset(CODE$TOWNSHIPS,Township==township)$town);
}

print.scope <- function(obj,...) {
	U_msg("OBJECTS: \n");
	print(names(obj));

	U_hr();
	U_msg("$townships\n");
	print(obj$townships);

	U_hr();
	U_msg("$RAWLOC\n");
	print(obj$RAWLOC);
	
	if ("MODELDAT_FULL"%in%names(obj)) {
		U_hr();
		U_msg("$MODELDAT_FULL: ",nrow(obj$MODELDAT_FULL)," rows X ",ncol(obj$MODELDAT_FULL)," cols\n");
		
		if ("modeldat"%in%names(obj)) {
			U_hr();
			U_msg("$MODELDAT_FILTER\n");
			print(obj$MODELDAT_FILTER);

			U_hr();
			U_msg("$modeldat_extremeMV (",nrow(obj$modeldat_extremeMV),")\n");
			if (nrow(obj$modeldat_extremeMV)>0)
				print(subset(head(obj$modeldat_extremeMV,5),select=c(pin,X,Y,neighborhood,saleamt,mos,yr,sqftb,sqftl,mv,totval,mktval)));
			
			U_hr();
			U_msg("$modeldat: ",nrow(obj$modeldat)," rows X ",ncol(obj$modeldat)," cols\n");
			print(head(obj$modeldat,5));

			U_hr();
			U_msg("$regt: ",nrow(obj$regt)," rows X ",ncol(obj$regt)," cols\n");
		}
	}
	
	if ("modelobjects"%in%names(obj)) {
		U_hr();
		U_msg("$modelobjects: ",paste0(names(obj$modelobjects),collapse="|"),"\n");
	}
	
	if ("diagnostics"%in%names(obj)) {
		U_hr();
			U_msg("$diagnostics: ",length(obj$diagnostics)," models\n");
			print(obj$diagnostics);
	}
}

# This function returns the ordered polygon for a given x/y dataset
S_xyToPoly <- function(scope,datxy) {
	U_msg("[S_xyToPoly] Merging input 'datxy' (pin+X+Y) into polygon\n");

	if (!("pin"%in%colnames(datxy)))
		stop("Input 'datxy' dataset does not have 'pin'");
	if (!("X"%in%colnames(datxy)) || !("Y"%in%colnames(datxy)))
		stop("Input 'datxy' dataset does not have X/Y");
	if (!("POLY"%in%names(scope))) {
		stop("Input scope object does not have a polygon built");
	}
	
	# Extract polygon out, and only keep those matching datxy
	shp <- scope$POLY;
		ix <- shp$Name%in%datxy$pin;
		if (any(is.na(ix))) {
			U_msg("\tNot all SHP-pins are in input 'datxy'.  Dropping from SHP\n");
			shp <- shp[-which(is.na(ix)),];
		}
	
	# Index between dataset and polygon
		ix <- match(datxy$pin,shp$Name);
		ix_isNA <- which(is.na(ix));
		if (length(ix_isNA)>0) {
			U_msg("\tpin/X/Y in input 'datxy' not all found in scope$POLY: N=",length(ix_isNA),"\n");
			datxy_found <- datxy[-ix_isNA,,drop=FALSE];
			ix <- na.omit(ix);
		} else {
			datxy_found <- datxy;
		}
	
	# Create polygon object with all columns in datxy
		out <- shp[ix,];
		out@data <- datxy_found;
	
	return(out);
}

S_sandbox <- function(scope,newmodeldat,newmodelobjects,newdiagnostics) {
	U_msg("[S_sandbox] Creating a sandboxed object with all data elements\n");
	obj <- scope;
	
	obj$modeldat <- newmodeldat;
	obj$modelobjects <- newmodelobjects;
	obj$diagnostics <- newdiagnostics;
	
	class(obj) <- "scope";
	return(obj);
}