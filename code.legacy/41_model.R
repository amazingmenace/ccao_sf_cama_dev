M_runBaseline <- function(scope,itertrim=0) {
	U_msg("[M_runBaseline] Running baseline 'TYLER models' with argument itertrim=",itertrim,"...\n");
	
	modeldat <- scope$modeldat;
	assessed <- scope$regt;
	# Execute model
	U_msg("\tEstimating Regression model: additive (LL)...\n");
	U_msg("\tWARNING: categorically defined 'ROOMS', 'NUM' and 'FIREPL' dropped from models - TYLER MODELS ARE ILL-DEFINED\n");
		LL <- lm(saleamt~BSF+LSF+FULLBATH+RQOS+EXTCON+LOCF,data=modeldat);
		U_msg("\tApplying regression models to predict values on $modeldat and on $regt\n");
		modeldat$LL <- LL$fitted.values;
		assessed$LL <- predict(LL,newdata=assessed);

	U_msg("\tEstimating Regression model: log-log (MM)...\n");
		MM <- lm(log(saleamt)~log(BSF)+log(LSF)+log(FULLBATH)+RQOS+EXTCON+log(LOCF),data=modeldat);
		U_msg("\tApplying regression models to predict values on $modeldat and on $regt\n");
		modeldat$MM <- exp(MM$fitted.values);
		
		assessed$MM <- exp(predict(MM,newdata=assessed));

	# Keep relevant columns in the output dataset
	assessed <- subset(assessed,select=c(pin,X,Y,BSF,landval,age,bedrooms,rqos,LL,MM));

	U_msg("\tApplying Tyler comps algorithm onto sales ($modeldat)...\n");

	if (TRUE) {
		COMPS_MODEL <- COMPS <- list();	# preserves the comps found: one on modeling set, other on property set
		# Loop through each property.  Calculate distance with every other property.  Preserve the "comps" found with each of the 3 criteria
		for (iter in 1:nrow(modeldat)) {

			this <- modeldat[iter,];
			that <- M_findcomps(this,modeldat,method="Tyler");
			COMPS_MODEL[[iter]] <- that;
		}
		modeldat$COMPS <- sapply(COMPS_MODEL,function(mat) mean(mat[,"Est"]));
		U_msg("\n\tNow applying Tyler comps algorithm onto all parcels ($regt|$assessed)...\n");
		for (iter in 1:nrow(assessed)) {
			this <- assessed[iter,];
			that <- M_findcomps(this,modeldat,method="Tyler");
			COMPS[[iter]] <- that;
		}
		assessed$COMP <- sapply(COMPS,function(mat) mean(mat[,"Est"],na.rm=TRUE));
		attr(assessed,"COMPS") <- COMPS;
	}
	U_msg("...done\n");
	
	U_msg("\tStoring '$modeldat' and '$assessed' with fitted values, '$modelobjects' with raw models\n");
	scope$modeldat <- modeldat;
	scope$modelobjects <- list(LL=LL,MM=MM,COMPS=COMPS_MODEL);
	scope$assessed <- assessed;
	return(scope);	
}
M_findcomps <- function(property,salesdat,method="Tyler") {
  library(fields)
	# Create distances between 'property', and all sales
	distances <- data.frame(
				d=(rdist.earth(cbind(property$X,property$Y),subset(salesdat,select=c(X,Y)),miles=TRUE)*5280)[1,],
				BSF=salesdat$BSF/property$BSF-1,
				LANDVAL=salesdat$landval/property$landval-1,
				AGE=salesdat$age-property$age,
				BEDS=salesdat$bedrooms-property$bedrooms,
		stringsAsFactors=FALSE);

	# Determine which Filter Iteration to use
	ix_property <- match(property$pin,salesdat$pin);	# Do not include the property itself
	if (TRUE) {
		n_filter <- 1;
		ix <- with(distances,{ which(d<=1000 & abs(BSF)<=0.2 & abs(LANDVAL)<=0.2 & abs(AGE)<=20 & abs(BEDS)<=1) });
		ix <- setdiff(ix,ix_property);	# Remove the same property
		if (length(ix)<5) {	# Not enough comps from filter-1.  Apply filter 2
			n_filter <- 2;
			ix <- with(distances,{ which(d<=3000 & abs(BSF)<=0.4 & abs(LANDVAL)<=0.4); });
			ix <- setdiff(ix,ix_property);
			if (length(ix)<5) { # Not enough comps from filter-2.  Apply filter3
				n_filter <- 3;
				ix <- with(distances,{ which(d<=5000 & abs(BSF)<=1.0); });
				ix <- setdiff(ix,ix_property);
				if (length(ix)<3) {
					U_msg("...WARNING: ONLY ",length(ix)," comps found with Tyler-version3 (pin=",property$pin,", X|Y=",property$X,"|",property$Y,")\n");
				}
			}
		}
	}
	attr(ix,"n_filter") <- n_filter;
	
	# For the tyler method, just calculate out
	if (method=="Tyler") {
		if (length(ix)==0) {
			comps <- cbind(MRASubj=property$MM,CompPrice=NA,CompMRA=NA,CompResidError=NA,Est=NA);
		} else {
			comps <- cbind(MRASubj=property$MM,CompPrice=salesdat$saleamt[ix],CompMRA=salesdat$MM[ix],CompResidError=NA,Est=NA);
		}
		row.names(comps) <- salesdat$pin[ix];
		comps[,"CompResidError"] <- comps[,"CompMRA"]-comps[,"CompPrice"];
		comps[,"Est"] <- comps[,"MRASubj"]+comps[,"CompResidError"];
	}
	
	return(comps);
}

# PRIMARY FUNCTION
# Calculates model diagnostics, including all bootstrap replicates
M_diagnostics <- function(scope,models=c("LL","MM","COMPS"),iqrseq=CODE$IQR$IQRm) {
	U_msg("[M_diagnostics] Creating model diagnostics with IQR=",paste0(iqrseq,collapse="|"),"...\n");

	# Determine what to generate for: sandbox model, or production model
	if (class(scope)=="data.frame") {
		modeldat <- scope;
	} else {
		modeldat <- scope$modeldat;
	}
	Y <- modeldat$saleamt;

	# Subfunction to run one estimate.  'seed' is the random seed for the bootstrap replicate.  NA means no re-shuffling
	onediag <- function(seed=NA) {
		# Determine the indices of the vectors, depending on shuffling or not
			if (!is.na(seed)) {
				set.seed(seed);
				ix <- sample(seq_along(Y),replace=TRUE);
			} else {
				ix <- seq_along(Y);
			}
			y <- Y[ix];
			mdat <- modeldat[ix,];

		# Loop through each model and calculate.  Based on whether as-is or shuffled, run various scenarios
		RES <- list();
		for (iter_m in seq_along(models)) {
			yhat <- mdat[[models[iter_m]]];
			
			res <- list();
			for (iter_i in seq_along(iqrseq)) {
				this <- M_modeldiagnostics(y,yhat,iqrmult=iqrseq[iter_i],asis=(is.na(seed)));
				res[[iter_i]] <- this;
			}
			# Keep the ratios...only for point estimate case
			if (is.na(seed)) {
				ratios <- lapply(res,function(obj) attr(obj,"inputs"));
				ratios <- sapply(ratios,function(obj) obj$inRange);
				ratios <- cbind(y,yhat,ratios);
				colnames(ratios) <- c("Y","Yhat",paste0("IQRm_",iqrseq));
				
				suitesraw <- lapply(res,function(obj) attr(obj,"suitesraw"));
			}			
			res <- do.call("rbind",res);
			
			RES[[iter_m]] <- data.frame(Model=models[iter_m],res,stringsAsFactors=FALSE);
			if (is.na(seed)) {
				attr(RES[[iter_m]],"ratios") <- ratios;
				attr(RES[[iter_m]],"suitesraw") <- suitesraw;
			}
		}
		return(RES);
	}
	
	RES <- onediag(seed=NA);
	names(RES) <- models;
	U_msg("\t1000 bootstrap replicates...\n");
	RESBOOT <- list();
	for (iter in 1:1000) {
		this <- onediag(seed=iter);
		RESBOOT[[iter]] <- this;
	}
	U_msg("\n");
	# Attach bootstrap replicates to original
	for (iter in seq_along(RES))
		attr(RES[[iter]],"boot") <- lapply(RESBOOT,function(obj) obj[[iter]]);
	
	if (class(scope)=="data.frame") {
		return(RES);
	} else {
		scope$diagnostics <- RES;
		return(scope);
	}
}
M_modeldiagnostics <- function(Y,Yhat,iqrmult=Inf,doround=TRUE,asis=TRUE) {
	calc_PRB <- function(ratios,Y,Yhat) {
		prb_y <- (ratios-median(ratios))/median(ratios);
		prb_x <- log((Y/2+Yhat/2)/log(2));
		prb <- lm(prb_y~prb_x);	# Or should this be quantile regression?
		prb <- coefficients(prb)[["prb_x"]];
		return(prb);
	}

	inputs <- data.frame(Y=Y,Yhat=Yhat);
	ratios <- Yhat/Y;
	# Censor data based in iqr
	if (TRUE) {
		q25 <- quantile(ratios,probs=.25);
		q75 <- quantile(ratios,probs=.75);
		iqr <- q75-q25;
		if (is.infinite(iqrmult)) {
			lims <- quantile(ratios,probs=c(.01,.99));
		} else {
			lims <- c(q25-iqrmult*iqr,q75+iqrmult*iqr);
		}
		
		# Determine what falls in the range
		ix <- which(ratios>lims[1] & ratios<lims[2]);
		inputs$inRange <- (1:nrow(inputs))%in%ix;
		ratios <- ratios[ix];
		Y <- Y[ix];
		Yhat <- Yhat[ix];
	}
	
	out <- list(
			count=length(Y),
			IQR=iqr,IQRm=iqrmult,ratLO=lims[1],ratHI=lims[2],
			median=median(ratios),
			mean=mean(ratios),
			wgtmean=weighted.mean(ratios,Y)
			);
	out$R2 <- 1-sum((Y-Yhat)^2)/sum((Y-mean(Y))^2);
	out$COD <- 100 * sum(abs(ratios-median(ratios))) / (length(ratios)*median(ratios));
	out$PRD <- out$mean/out$wgtmean;
	if (asis) {
		out$PRB <- calc_PRB(ratios,Y,Yhat);
	} else {
		out$PRB <- NA;
	}
	# For suites, need a sorted order
	if (TRUE) {
		ix_sort <- order(Y);
		Ysort <- Y[ix_sort];
		Yhatsort <- Yhat[ix_sort];
		Ysort_cum <- c(0,cumsum(Ysort)/sum(Ysort));
		Yhatsort_cum <- c(0,cumsum(Yhatsort)/sum(Yhatsort));
		
		suitesraw <- cbind(Ysort_cum,c(Ysort_cum[-1],1),Yhatsort_cum,c(Yhatsort_cum[-1],1));
		suites <- sum((suitesraw[,2]-suitesraw[,1]) * ((suitesraw[,4]-suitesraw[,3])/2 + suitesraw[,3]));
		out$suites <-  1 - suites*2;
		# Clean up suitesraw
			suitesraw <- cbind(Ysort_cum,Yhatsort_cum);
	}
	
	out <- do.call("cbind",out);
	row.names(out) <- "1";
	if (doround) {
		for (iter in 1:ncol(out))
			out[,iter] <- round(out[,iter],3);
	}
	if (asis)
		attr(out,"inputs") <- inputs;
		
	attr(out,"suitesraw") <- suitesraw;
	
	return(out);
}