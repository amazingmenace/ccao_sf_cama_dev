#various township lists to test code
southern_tri <- c(11, 12, 13, 14, 15, 19, 21, 27, 30, 31,
                  32, 33, 34, 36, 37, 38, 39)
good_north_tri <- c(10, 17, 23, 25)
bootstrapp_iters <- 100
all_towns <- c(10:39, 70:77)


#functions to grab sales data
#randomly select PINS which existed in Tax Year 2018 and are in limited residential classes and selected townships
get_random_pins <- function(num, townships){
  result <- dbGetQuery(
    CCAODATA,
    paste0(
      "SELECT TOP (", num, ") HD_PIN
      FROM HEAD
      WHERE TAX_YEAR = 2018
      AND HD_CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295)
      AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN (", paste(townships, collapse = ", "), ")
      ORDER BY NEWID()
      "
    )
  )
  return(result)
}

get_random_sales <- function(num, townships){
  result <- dbGetQuery(
    CCAODATA,
    paste0(
      "SELECT TOP (", num, ") HD_PIN
      FROM HEAD
      LEFT JOIN IDORSALES ON HD_PIN = IDORSALES.PIN
      WHERE TAX_YEAR = 2018
      AND HD_CLASS IN (202, 203, 204, 205, 206, 207, 208, 209, 210, 234, 278, 295)
      AND CONVERT(INT, SUBSTRING(CONVERT(CHARACTER, HD_TOWN),1,2)) IN (", paste(townships, collapse = ", "), ")
      AND YEAR(RECORDED_DATE)>=2017
      AND SALE_PRICE > 10000
      AND MULT_IND != 'X'
      ORDER BY NEWID()
      "
    )
  )
  return(result)
}

#stats on pin
get_pin_info <- function(pin){
  result <- dbGetQuery(
    CCAODATA,
    paste0(
      "SELECT *
            FROM CCAOSFCHARS as SF
            LEFT JOIN BAMASTER ON SF.PIN = BAMASTER.PIN AND SF.TAX_YEAR = BAMASTER.TAX_YEAR + 1
            LEFT JOIN (SELECT Name, CAST(centroid_x AS FLOAT) as x, CAST(centroid_y AS FLOAT) as y FROM PINLOCATIONS) as geo ON Name = SF.PIN
            FULL OUTER JOIN (SELECT PIN as sales_pin, DOC_NO, SALE_PRICE, RECORDED_DATE FROM IDORSALES) as idor ON sales_pin = SF.PIN
            WHERE SF.PIN = CAST(", pin, " AS BIGINT) AND SF.TAX_YEAR = 2018 AND SALE_PRICE > 10000
            "
    )
  )
  print(pin)
  if (!is.na(result$RECORDED_DATE)){
    result <-
      result[result$RECORDED_DATE == max(result$RECORDED_DATE), ]
  }
  if (nrow(result) > 1){
    result <- result[1, ]
  }
  result["SALE_PRICE"] <- as.integer(result["SALE_PRICE"])
  return(result)
}

range_method <- function(universe, params, pin_info){
  range_comps <- universe
  new <- params[params["method"] == "range", ]
  for (col in new$Attribute) {
    if (nrow(range_comps) > 0){
      range_comps[col] <- c(apply(range_comps[col], 2, function(x) x - pin_info[col][[1]]))
      range_comps <- range_comps[(range_comps[col] >= new[new["Attribute"] == col, ]["min"][[1]]) &
                                   (range_comps[col] <= new[new["Attribute"] == col, ]["max"][[1]]), ]
    }
  }
  if (nrow(range_comps) > 0) {
    range_comps["method"] <- "range"
    range_comps["similarity_score"] <- 1
    #weight more recent sales higher
    weight <- 1
    for (i in sort(unique(range_comps$SALE_YEAR))){
      range_comps[range_comps["SALE_YEAR"] == i, ]["similarity_score"] <- weight / (weight + 1)
      weight <- weight + 1
    }
  } else {
    range_comps["method"] <- character(0)
  }
  return(range_comps)
}


alt_method <- function(universe, params, pin_info){
  alt_comps <- universe
  alt_comps["similarity_score"] <- 1
  alt_comps["method"] <- "alt"
  
  new <- params[params["method"] == "alt", ]
  for (col in new["Attribute"][[1]]){
    current_filter <- new[new["Attribute"] == col, ]
    if (col != "SALE_YEAR"){
      distribution <- ecdf(alt_comps[, col])
      alt_comps["similarity_score"] <- alt_comps["similarity_score"] -
        current_filter["importance"][[1]] *  abs(distribution(pin_info[col]) - distribution(alt_comps[, col]))
    } else {
      weight <- 1
      unique_years <- unique(alt_comps["SALE_YEAR"])[[1]]
      for (i in sort(unique_years)){
        alt_comps["similarity_score"] <- alt_comps["similarity_score"] -
          current_filter["importance"][[1]] * weight
        weight <- weight - 1 / length(unique_years)
      }
    }
  }
  alt_comps["similarity_score"] <- ecdf(alt_comps[, "similarity_score"])(alt_comps[, "similarity_score"])
  alt_comps <- alt_comps[alt_comps["similarity_score"] > 0.80, ]
  return(alt_comps)
}

kmean_method <- function(universe, params, pin_info){
  bad_cols <- c("APTS", "PIN", "TAX_YEAR", "VOL", "NBHD", "LANDVAL", "BLDGVAL", "TOTVAL", "Name", "sales_pin", "DOC_NO", "SALE_PRICE", "RECORDED_DATE", "TAXCDE", "SALE_YEAR", "TOWNSHIP", "NEIGHBORHOOD", "num_sales", "med_sale", "avg_sale", "similarity_score")
  good_cols <- setdiff(intersect(names(universe), names(pin_info)), bad_cols)
  pin_sub <- pin_info[, names(pin_info) %in% good_cols]
  comps_sub <- universe[, names(universe) %in% good_cols]
  subset_kmean <- rbind(pin_sub, comps_sub)
  subset_kmean["AGE"] <- subset_kmean["AGE"] %/% 10
  num_clusters <- max(ceiling(nrow(subset_kmean) / 350), 2)
  if (nrow(subset_kmean) > 10){
    subset_kmean <- scale(subset_kmean)
    subset_kmean <- subset_kmean[, colSums(is.na(subset_kmean)) == 0]
    kmean_result <- kmeans(subset_kmean, centers = num_clusters, iter.max = 20, nstart = 50)
    master_pin_cluster <- kmean_result$cluster[1]
    
    #diagnostic functions
    #fviz_nbclust(subset_kmean, kmeans, method = "wss", k.max=40)
    #fviz_cluster(kmean_result, data=subset_kmean)
    
    kmean_comps <- universe[tail(kmean_result$cluster, -1) == master_pin_cluster, ]
    kmean_comps["similarity_score"] <- 1
    kmean_comps["method"] <- "kmean"
    return(kmean_comps)
  } else {
    return(NA)
  }
}

run_all_methods <- function(pin){
  #distance universe
  params <- read.xlsx("ccao_dictionary_draft.xlsx", sheetName = "comp_tool_params", stringsAsFactors = FALSE)
  initial_distance <- params[params["Attribute"] == "DISTANCE", ]["max"][[1]] / 5280
  dist_info <- get_pin_and_neighbors(pin, initial_distance)
  pin_info <- dist_info[[1]]
  
  #get target neighborhoods
  adj_pin_AV <- pin_info$TOTVAL * 10
  filter_med_sale_percent <- 0.05
  
  #use combination of most recent sale and assessed value
  #if most recent sale is more than 25% below most recent AV, reweight
  if(!is.na(pin_info$SALE_PRICE)){
    if(((adj_pin_AV - pin_info$SALE_PRICE) / adj_pin_AV) < -0.25){
      sale_year <- year(pin_info$RECORDED_DATE)
      if(sale_year > 2014){
        adj_pin_AV <- pin_info$SALE_PRICE
      } else {
        adj_pin_AV <- 0.5 * pin_info$SALE_PRICE + 0.5 * adj_pin_AV
      }
    }
  }
  
  comp_neighborhoods <-
    sales_universe[[2]] %>% filter(med_sale >= adj_pin_AV * (1 - filter_med_sale_percent)) %>%
    filter(med_sale <= adj_pin_AV * (1 + filter_med_sale_percent))
  
  while (sum(comp_neighborhoods$num_sales) < 1000){
    filter_med_sale_percent <- filter_med_sale_percent * 1.01
    comp_neighborhoods <-
      sales_universe[[2]] %>% filter(med_sale >= adj_pin_AV * (1 - filter_med_sale_percent)) %>%
      filter(med_sale <= adj_pin_AV * (1 + filter_med_sale_percent))
  }
  
  #increase neighborhood size if too few neighbors found
  if (nrow(dist_info[[2]]) < 10){
    dist_info <- get_pin_and_neighbors(pin, 1.5 * initial_distance)
  }
  
  #define universes
  distance_universe <- as.data.frame(dist_info[[2]])
  township_sales <- as.data.frame(sales_universe[[1]] %>% filter(TOWNSHIP == pin_info$TOWN))
  town_nbhd_sales <- as.data.frame(township_sales %>% filter(NEIGHBORHOOD == pin_info$NBHD))
  medsale_universe <- as.data.frame(inner_join(sales_universe[[1]], comp_neighborhoods, by = c("NEIGHBORHOOD", "TOWNSHIP")))
  medsale_universe["SALE_PRICE"] <- lapply(medsale_universe["SALE_PRICE"], as.integer)
  
  #apply methods
  universe_ls <- c("township_sales", "distance_universe", "town_nbhd_sales", "medsale_universe")
  
  total <- data.frame()
  for (df_name in universe_ls){
    universe <- get(df_name)
    if (nrow(universe) > 0){
      range <- range_method(universe, params, pin_info)
      percentile <- percentile_method(universe, params, pin_info)
      alt <- alt_method(universe, params, pin_info)
      kmean <- kmean_method(universe, params, pin_info)
      print(paste(df_name, nrow(range), nrow(percentile), nrow(alt), nrow(kmean)))
      result <- rbind(range, percentile, alt, kmean)
      
      result["universe"] <- df_name
      result <- result[, !names(result) %in% c("DISTANCE", "num_sales", "med_sale", "avg_sale")]
      if (nrow(result) > 0){
        total <- rbind(total, result)
      }
    }
  }
  return(list(pin_info, total))
}

percentile_method <- function(universe, params, pin_info){
  percent_comps <- universe
  new <- params[params["method"] == "percentiles", ]
  for (col in new$Attribute) {
    if (nrow(percent_comps) > 0){
      percent_comps <-
        percent_comps[(percent_comps[col] >= as.double(as.double(pin_info[col]) * new[new["Attribute"] == col, ]["min"][[1]])) &
                        (percent_comps[col] <= as.double(as.double(pin_info[col]) * new[new["Attribute"] == col, ]["max"][[1]])), ]
    }
  }
  if (nrow(percent_comps) > 0) {
    percent_comps["method"] <- "percentiles"
    percent_comps["similarity_score"] <- 1
    #weight more recent sales higher
    weight <- 1
    for (i in sort(unique(percent_comps$SALE_YEAR))){
      percent_comps[percent_comps["SALE_YEAR"] == i, ]["similarity_score"] <- weight / (weight + 1)
      weight <- weight + 1
    }
  } else {
    percent_comps["method"] <- character(0)
  }
  return(percent_comps)
}

get_results <- function(pin){
  main <- data.frame(pin = character(), estimate_comps = integer(), AV = integer(), method = character(), universe = character())
  comps <- new_tool(pin)
  pin_info <- comps[[1]]
  comps_results <- comps[[2]]
  
  if (length(comps_results) == 0){
    print("no comps found")
    return(data.frame())
  }
  for (universe_filter in unique(comps_results$universe)){
    universe_filtered <- comps_results[comps_results["universe"] == universe_filter, ]
    for (method_filter in append(unique(comps_results$method), "universe_only")){
      if (method_filter == "universe_only"){
        tmp <- universe_filtered
      } else {
        tmp <- universe_filtered[universe_filtered["method"] == method_filter, ]
      }
      if (nrow(tmp) > 0){
        estimate <- sum( (tmp["SALE_PRICE"] * tmp["similarity_score"])[[1]]) / sum(tmp["similarity_score"])
        df <- data.frame(pin = pin, COMP_AV = estimate, ASSESSOR_AV = 10 * pin_info$TOTVAL, method = method_filter, universe = universe_filter)
        if (!is.na(pin_info["SALE_PRICE"])){
          df["PRIOR_Y_SALE_PRICE"] <- pin_info["SALE_PRICE"]
          df["RECORDED_DATE"] <- pin_info["RECORDED_DATE"]
          df["COMP_RATIO"] <- df["COMP_AV"] / df["PRIOR_Y_SALE_PRICE"]
          df["ASSESSOR_RATIO"] <- df["ASSESSOR_AV"] / df["PRIOR_Y_SALE_PRICE"]
        } else {
          df["PRIOR_Y_SALE_PRICE"] <- integer()
          df["RECORDED_DATE"] <- character()
          df["RATIO"] <- integer()
        }
        main <- rbind(main, df)
      }
    }
  }
  
  return(main)
}

#run_many_pins runs a random selection of pins and returns summary results
run_many_pins <- function(number, type_of_pins, townships){
  if (type_of_pins == "sales"){
    pins <- get_random_sales(number, townships)
  } else {
    pins <- get_random_pins(number, townships)
  }
  df <- data.frame(pin = character(), estimate_comps = character(), TOTAL_AV = character())
  for (pin in pins$HD_PIN){
    cur <- get_results(pin)
    if (nrow(cur) > 0){
      df <- rbind(df, cur)
    }
  }
  #filter pins with <100 dollar sales and <100 assessments
  df <- df %>% filter(PRIOR_Y_SALE_PRICE > 100) %>% filter(ASSESSOR_AV > 100)
  df <- df[complete.cases(df), ]
  return(df)
}

get_results_tbl <- function(townships, cnt){
  tbl <- data.frame(TOWNSHIP = character(), METHOD = character(), COVERAGE = integer(), COD = integer(), PRB = integer(), PRD = integer())
  for (township in townships){
    print(township)
    rslts <- run_many_pins(cnt, "sales", township)
    stats <- calc_iaao_stats(as.data.frame(rslts))
    tmp <- stats %>% mutate(TOWN = township,
                            METHOD = paste(evaluating, universe, method, sep="||"),
                            COVERAGE = completeness / cnt) %>% dplyr::select(TOWN, METHOD, COVERAGE, COD, PRB, PRD)
    tbl <- rbind(tbl, tmp)
    write.table(tmp, file="small_results.csv", append=TRUE, row.names=FALSE, col.names=FALSE)
  }
  return(tbl)
}

calc_iaao_stats <- function(property_data){
  
  stats <- data.frame(evaluating = character(), universe = character(), method = character(), COD = character(), COD_LOWER_CI = double(), COD_UPPER_CI = double(),
                      PRD = character(), PRD_LOWER_CI = double(), PRD_UPPER_CI = double(),
                      PRB = double(), PRB_LOWER_CI = double(), PRB_UPPER_CI = double(),
                      med_ratio = double(), sale_25 = double(), median_sale = double(), sale_75 = double(),
                      AV_25 = double(), AV_50 = double(), AV_75 = double())
  
  assessor_stats <- distinct(property_data, pin, .keep_all = TRUE) %>% 
    rename(RATIO = ASSESSOR_RATIO, TOTAL_AV = ASSESSOR_AV)
  stats <- get_stats(stats, assessor_stats, "ASSESSOR", "", "")
  for (universe_filter in unique(property_data$universe)){
    df <- property_data %>% rename(RATIO = COMP_RATIO, TOTAL_AV = COMP_AV) %>% filter(universe == universe_filter)
    for (mthd in unique(df$method)){
      stats <- get_stats(stats, as.data.frame(df %>% filter(method == mthd)), "COMP_TOOL", universe_filter, mthd)  
    }
  }
  return(stats)
}

get_stats <- function(df_to_bind, df_to_get_stats, criteria, universe_filter, mthd){
  cod_calcs <- cod_func(df_to_get_stats$RATIO)
  prd_calcs <- prd_func(df_to_get_stats, c("RATIO", "PRIOR_Y_SALE_PRICE"))
  prb_calcs <- prb_func(df_to_get_stats, c("RATIO", "TOTAL_AV", "PRIOR_Y_SALE_PRICE"))
  cod_ci <- parse_number(unlist(strsplit(unname(cod_calcs[3]), split = ",", fixed = T)))
  cod_lower_ci <- cod_ci[1]
  cod_upper_ci <- cod_ci[2]
  
  prd_ci <- parse_number(unlist(strsplit(unname(prd_calcs[3]), split = ",", fixed = T)))
  prd_lower_ci <- prd_ci[1]
  prd_upper_ci <- prd_ci[2]
  
  prb_ci <- parse_number(unlist(strsplit(unname(prb_calcs[3]), split = ",", fixed = T)))
  prb_lower_ci <- prb_ci[1]
  prb_upper_ci <- prb_ci[2]
  df_to_bind <- rbind(df_to_bind,
                      data.frame(
                        evaluating = criteria,
                        universe = universe_filter,
                        method = mthd,
                        completeness = nrow(df_to_get_stats),
                        COD = as.numeric(unname(cod_calcs[1])),
                        COD_LOWER_CI = cod_lower_ci,
                        COD_UPPER_CI = cod_upper_ci,
                        PRD = as.numeric(unname(prd_calcs[1])),
                        PRD_LOWER_CI = prd_lower_ci,
                        PRD_UPPER_CI = prd_upper_ci,
                        PRB = as.numeric(unname(prb_calcs[1])),
                        PRB_LOWER_CI = prb_lower_ci,
                        PRB_UPPER_CI = prb_upper_ci,
                        med_ratio = median(df_to_get_stats$RATIO),
                        sale_25 = quantile(df_to_get_stats$PRIOR_Y_SALE_PRICE)[[2]],
                        med_sale = median(df_to_get_stats$PRIOR_Y_SALE_PRICE),
                        sale_75 = quantile(df_to_get_stats$PRIOR_Y_SALE_PRICE)[[4]],
                        AV_25 = quantile(df_to_get_stats$TOTAL_AV)[[2]],
                        AV_50 = quantile(df_to_get_stats$TOTAL_AV)[[3]],
                        AV_75 = quantile(df_to_get_stats$TOTAL_AV)[[4]]
                      ))
  return(df_to_bind)
}


interpret_results_tbl <- function(file_name){
  tbl <- read.table(file_name)
  names(tbl) <- c("TOWN", "METHOD", "COVERAGE", "COD", "PRB", "PRD")
  tbl <- tbl %>% filter(COVERAGE > 0.98) %>% group_by(TOWN)
  
  
  best_cods <- as.data.frame(table(tbl %>% filter(COD == min(COD)) %>% pull(METHOD)))
  best_prd <- as.data.frame(table(tbl %>% filter(abs(PRD - 1) == min(abs(PRD - 1))) %>% pull(METHOD)))
  best_prb <- as.data.frame(table(tbl %>% filter(abs(PRB) == min(abs(PRB))) %>% pull(METHOD)))
  
  best_cods <- best_cods %>% rename(method = Var1,
                                    best_cod = Freq)
  best_prd <- best_prd %>% rename(method = Var1,
                                  best_prd = Freq)
  best_prb <- best_prb %>% rename(method = Var1,
                                  best_prb = Freq)
  
  best_count_df <- data.frame(method=unique(tbl$METHOD)) %>% full_join(best_cods) %>% full_join(best_prd) %>% full_join(best_prb)
  
  
  
  my_theme <- theme_classic() + theme(panel.grid.major = element_line(color = 'gray'), axis.line.x = element_line(color='gray'), axis.line.y = element_blank())
  
  cod_plt <- ggplot(data=tbl, aes(x=METHOD, y=COD)) + geom_boxplot() + ylim(0, 100) + my_theme + coord_flip()
  prd_plt <- ggplot(data=tbl, aes(x=METHOD, y=PRD)) + geom_boxplot() + ylim(0, 3) + my_theme + coord_flip()
  prb_plt <- ggplot(data=tbl, aes(x=METHOD, y=PRB)) + geom_boxplot() + ylim(-3, 3) + my_theme + coord_flip()
  cov_plt <- ggplot(data=tbl, aes(x=METHOD, y=COVERAGE)) + geom_boxplot() + my_theme + coord_flip()
  
  grid.arrange(cod_plt, prd_plt, prb_plt, cov_plt)
  
  return(best_count_df)
  
}
