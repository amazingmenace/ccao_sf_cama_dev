                                                     *SPSS Regression.
						           *OAK PARK RIVERSIDE RIVER FOREST REGRESSION  2017.


Get file='C:\Users\daaaron\documents\regts273334mergefcl2b.sav'.
*select if (amount>160000).
*select if (amount<1500000).
*select if (multi<1).
*select if sqftb<10000.
Compute year1=0.
If  (amount>0) year1=1900 + yr. 
if  (yr=16 and amount>0)  year1=2016.
if  (yr=15 and amount>0) year1=2015.
if  (yr=14 and amount>0) year1=2014.
if  (yr=13 and amount>0)  year1=2013.
if  (yr=12 and amount>0)  year1=2012.
if  (yr=11 and amount>0)  year1=2011.

exe.
COMPUTE FX = cumfile14151617.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1. 

*select if (year1 > 2011).
set mxcells=2500000.
Compute bs=0.
if age<10 and (amount<1600000 and (amount/sqftb)<75 and class<95) bs=1.
* if (pin=	16061070220000
or pin=	16061200330000
or pin=	16062010110000
or pin=	16062020200000
or pin=	16062050140000
or pin=	16062220320000
or pin=	16063010280000
or pin=	16063020130000
or pin=	16063030230000
or pin=	16063040200000
or pin=	16063100240000
or pin=	16063140090000
or pin=	16063180150000
or pin=	16063180220000
or pin=	16063230080000
or pin=	16063240230000
or pin=	16064020180000
or pin=	16064120250000
or pin=	16071090040000
or pin=	16071280230000
or pin=	16072100110000
or pin=	16072251000000
or pin=	16051100300000
or pin=	16051200150000
or pin=	16051230220000
or pin=	16051260200000
or pin=	16051270160000
or pin=	16053060200000
or pin=	16053150240000
or pin=	16053190290000
or pin=	16053200040000
or pin=	16053200130000
or pin=	16081030020000
or pin=	16081030140000
or pin=	16073040280000
or pin=	16073070280000
or pin=	16073120030000
or pin=	16074060210000
or pin=	16074120200000
or pin=	16074170060000
or pin=	16181050150000
or pin=	16181070030000
or pin=	16181090040000
or pin=	16181200140000
or pin=	16181250160000
or pin=	16182150220000
or pin=	16182200020000
or pin=	16182200150000
or pin=	16183010100000
or pin=	16183020360000
or pin=	16183040090000
or pin=	16183060210000
or pin=	16183070070000
or pin=	16183070190000
or pin=	16184040110000
or pin=	16184090110000
or pin=	16184140080000
or pin=	16184180350000
or pin=	16184200140000
or pin=	16184200380000
or pin=	16184290260000
or pin=	16071030090000
or pin=	16071030110000
or pin=	16071050060000
or pin=	16071150120000
or pin=	16072120050000
or pin=	16171000110000
or pin=	16171030210000
or pin=	16171120330000
or pin=	16171180250000
or pin=	16171230230000
or pin=	16171250170000
or pin=	16171260270000
or pin=	16081100160000
or pin=	16081210180000
or pin=	16081210200000
or pin=	16083020280000
or pin=	16083040010000
or pin=	16083070200000
or pin=	16083140380000
or pin=	16083170150000
or pin=	16083220020000
or pin=	16083220130000
or pin=	16083220280000
or pin=	16173100340000
or pin=	16173130150000
or pin=	16173190210000
or pin=	16173220210000
or pin=	16173220300000
or pin=	16173250130000
or pin=	15011130370000
or pin=	15012010140000
or pin=	15012090200000
or pin=	15014030400000
or pin=	15013200170000
or pin=	15121040210000
or pin=	15121110420000
or pin=	15122030050000
or pin=	15122040210000
or pin=	15122110200000
or pin=	15122120060000
or pin=	15114040240000
or pin=	15121160220000
or pin=	15123040070000
or pin=	15123050250000
or pin=	15123100120000
or pin=	15123210040000
or pin=	15013160180000
or pin=	15014120020000
or pin=	15014180120000
or pin=	15251060390000
or pin=	15261060160000
or pin=	15253000110000
or pin=	15253020300000
or pin=	15253030250000
or pin=	15253040170000
or pin=	15253060590000
or pin=	15253060920000
or pin=	15253080120000
or pin=	15254060010000
or pin=	15254180200000
or pin=	15354110040000
or pin=	15354110240000
or pin=	15362070390000
or pin=	15362090340000
or pin=	15362140120000
or pin=	15363020340000
or pin=	15363030570000
or pin=	15364010150000
or pin=	15364050190000
or pin=	15364060210000
or pin=	15364060750000
or pin=	15364080050000
or pin=	15364120090000
or pin=	15363120180000
or pin=	15363130330000
or pin=	15254030360000
or pin=	15352020500000
or pin=	15354140370000
or pin=	15361000280000
or pin=	15361030020000
or pin=	15353010060000
or pin=	15353200050000)  bs=1.
*select if bs=0.


Compute OP=0.
If town=27 OP=1.
Compute RF=0.
If town=33 RF=1.
Compute RS=0.
If town=34 RS=1.

Compute N=1.
Compute tnb=(town*1000)+nghcde.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute n27010=0.
compute n27020=0.
compute n27030=0.
compute n27040=0.
compute n27041=0.
compute n27050=0.
compute n27060=0.
compute n27070=0.
compute n27080=0.
compute n27090=0.
compute n27100=0.

compute n33015=0.
compute n33020=0.
compute n33030=0.
compute n33040=0.
compute n33050=0.

compute n34010=0.
compute n34020=0.
compute n34040=0.
compute n34050=0.
compute n34070=0.
compute n34080=0.


If tnb=27010 n27010=1.
If tnb=27020 n27020=1. 
If tnb=27030 n27030=1.
If tnb=27040 n27040=1.
If tnb=27041 n27041=1.
If tnb=27050 n27050=1.
If tnb=27060 n27060=1.
If tnb=27070 n27070=1.
If tnb=27080 n27080=1.
If tnb=27090 n27090=1. 
If tnb=27100 n27100=1.

If tnb=33015 n33015=1.
If tnb=33020 n33020=1.  
If tnb=33030 n33030=1. 
If tnb=33040 n33040=1.
If tnb=33050 n33050=1.


If tnb=34010 n34010=1.
If tnb=34020 n34020=1.
If tnb=34040 n34040=1.
If tnb=34050 n34050=1.
If tnb=34070 n34070=1.
If tnb=34080 n34080=1.


compute sb27010=0.
compute sb27020=0.
compute sb27030=0.
compute sb27040=0.
compute sb27041=0.
compute sb27050=0.
compute sb27060=0.
compute sb27070=0.
compute sb27080=0.
compute sb27090=0.
compute sb27100=0.

compute sb33015=0.
compute sb33020=0.
compute sb33030=0.
compute sb33040=0.
compute sb33050=0.

compute sb34010=0.
compute sb34020=0.
compute sb34040=0.
compute sb34050=0.
compute sb34070=0.
compute sb34080=0.




If tnb=27010 sb27010=sqrt(bsf).
If tnb=27020 sb27020=sqrt(bsf). 
If tnb=27030 sb27030=sqrt(bsf).
If tnb=27040 sb27040=sqrt(bsf).
If tnb=27041 sb27041=sqrt(bsf).
If tnb=27050 sb27050=sqrt(bsf).
If tnb=27060 sb27060=sqrt(bsf).
If tnb=27070 sb27070=sqrt(bsf).
If tnb=27080 sb27080=sqrt(bsf).
If tnb=27090 sb27090=sqrt(bsf). 
If tnb=27100 sb27100=sqrt(bsf).

If tnb=33015 sb33015=sqrt(bsf).
If tnb=33020 sb33020=sqrt(bsf).  
If tnb=33030 sb33030=sqrt(bsf). 
If tnb=33040 sb33040=sqrt(bsf).
If tnb=33050 sb33050=sqrt(bsf).

If tnb=34010 sb34010=sqrt(bsf).
If tnb=34020 sb34020=sqrt(bsf).
If tnb=34040 sb34040=sqrt(bsf).
If tnb=34050 sb34050=sqrt(bsf).
If tnb=34070 sb34070=sqrt(bsf).
If tnb=34080 sb34080=sqrt(bsf).


If tnb=27010 n=5.50.
If tnb=27020 n=5.41. 
If tnb=27030 n=3.82.
If tnb=27040 n=4.63.
If tnb=27041 n=5.30.
If tnb=27050 n=4.57.
If tnb=27060 n=4.01.
If tnb=27070 n=7.47.
If tnb=27080 n=4.00.
If tnb=27090 n=4.70. 
If tnb=27100 n=3.03.

If tnb=33015 n=6.25.
If tnb=33020 n=7.34.  
If tnb=33030 n=6.57. 
If tnb=33040 n=4.90.
If tnb=33050 n=7.66.
	
If tnb=34010 n=2.45.
If tnb=34020 n=3.72.
If tnb=34040 n=3.22.
If tnb=34050 n=5.81.
If tnb=34070 n=3.30.
If tnb=34080 n=2.84.


Compute lsf=sqftl.
*If town=27 and lsf > 8990  lsf = 8990 + ((lsf - 8990)/3).
*If town=33 and lsf > 16450  lsf=16450 + ((lsf -16450)/3).
*If town=34 and lsf > 12561  lsf=12561 + ((lsf -12561)/3).
*If (class=8 or class=9) lsf=sqftl.
Compute nlsf=n*lsf.
Compute nsrlsf=n*sqrt(lsf).

Compute OP=0.
Compute  RF=0.
Compute  RS=0. 

If town=27  OP=1.
If town=33  RF=1.
If town=34  RS=1.


Compute opage=0.
If OP=1 opage=OP*age.
Compute rfage=0.
If RF=1 rfage=RF*age.
Compute rsage=0.
If RS=1 rsage=RS*age.

Compute oplsf=0.
If OP=1 opsrlsf=OP*sqrt(lsf).
compute nopsrlsf=n*opsrlsf.
Compute rfsrlsf=0.
If RF=1 rfsrlsf=RF*sqrt(lsf).
compute nrfsrlsf=n*rfsrlsf.
Compute rsrlsf=0.
If RS=1 rsrlsf=RS*sqrt(lsf).
compute nrsrlsf=n*rsrlsf.

compute onharlem=0.
if (15014120130000<=pin and pin<=15014120240000) or
   (15014180100000<=pin and pin<=15014180170000) or
   (16061200240000<=pin and pin<=16061200350000) or
(pin=15122110190000 or
pin=16061070280000 or
pin=16061070340000 or
pin=16061070830000 or
pin=16061070540000 or
pin=16061070600000 or
pin=16061130260000 or
pin=16061130300000 or
pin=16061130320000 or
pin=16063000080000 or
pin=16063000110000 or
pin=16063000120000 or
pin=16063000130000 or
pin=16063000340000 or
pin=16063000350000 or
pin=16063000470000 or
pin=16063070030000 or
pin=16063070040000 or
pin=16063070060000 or
pin=16063070070000 or
pin=16063070080000 or
pin=16063070290000 or
pin=16063140080000 or
pin=16063140220000 or
pin=16063150010000 or
pin=16063150210000 or
pin=16063150280000 or
pin=16063170250000 or 
pin=16073000050000 or
pin=16073000070000 or
pin=16073000080000 or
pin=16073070110000 or
pin=16073140130000 or
pin=16073140140000 or
pin=16073140150000 or
pin=16073140160000 or
pin=16073140060000 or
pin=16073140070000 or
pin=16073140090000 or
pin=16181000080000 or
pin=16181090010000 or
pin=16181090020000 or
pin=16181090030000 or
pin=16181090040000 or
pin=16181090070000 or
pin=16181090120000) or
(16181260040000<=pin and pin<=16181260160000) onharlem=1.

Compute archlist=0.
If (pin=16063210320000
or pin=16063210380000
or pin=16063230210000
or pin=16063250140000
or pin=16064030120000
or pin=16064040210000
or pin=16064070240000
or pin=16064080030000
or pin=16064080070000
or pin=16064080080000
or pin=16064090060000
or pin=16064090070000
or pin=16064090160000
or pin=16064140140000
or pin=16064150190000
or pin=16064160030000
or pin=16064160120000
or pin=16064160130000
or pin=16064160210000
or pin=16064170060000
or pin=16064170200000
or pin=16064170210000
or pin=16064170230000
or pin=16064200030000
or pin=16064210030000
or pin=16064220030000
or pin=16064220050000
or pin=16064220090000
or pin=16064220170000
or pin=16064230010000
or pin=16071020050000
or pin=16071020060000
or pin=16071020080000
or pin=16071020180000
or pin=16071030010000
or pin=16071030080000
or pin=16071040050000
or pin=16071040080000
or pin=16071040170000
or pin=16071040430000
or pin=16071080260000
or pin=16071080320000
or pin=16071150060000
or pin=16071150110000
or pin=16071150200000
or pin=16072010030000
or pin=16072010130000
or pin=16072030130000
or pin=16072030140000
or pin=16072050010000
or pin=16072050020000
or pin=16072050030000
or pin=16072060100000
or pin=16072070060000
or pin=16072120040000
or pin=16072120050000
or pin=16072130030000
or pin=16072160080000
or pin=16073240030000
or pin=16073240310000
or pin=16071030070000
or pin=16071040100000
or pin=16072060090000
or pin=15112080060000
or pin=15121060060000
or pin=15121110110000
or pin=15361030220000) archlist=1.

Compute lowzoneoakpark=0.
if town=27 and  (nghcde=10 or nghcde=20  or nghcde=30  or nghcde=40 
 or nghcde=41 or nghcde=50 or nghcde=60 or nghcde=70 or
 nghcde=80 or nghcde=90 or nghcde=100) lowzoneoakpark=1.                         	
Compute srfxlowblockoakpark=0.
if lowzoneoakpark=1 srfxlowblockoakpark=srfx*lowzoneoakpark.

Compute lowzoneriver=0.
if town=34 and  (nghcde=10 or nghcde=20  or nghcde=40  or nghcde=50 
or nghcde=70 or nghcde=80)  lowzoneriver=1.                         	
Compute srfxlowblockriver=0.
if lowzoneriver=1 srfxlowblockriver=srfx*lowzoneriver.

Compute lowzonerf=0.
if town=33 and  (nghcde=15 or nghcde=20  or nghcde=30  or nghcde=40 
or nghcde=50)  lowzonerf=1.                         	
Compute srfxlowblockrf=0.
if lowzonerf=1 srfxlowblockrf=srfx*lowzonerf.


Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.
Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.
Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute winter1516=0.
if (mos > 9 and yr=15) or (mos <= 3 and yr=16) winter1516=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute summer16=0.
if (mos > 3 and yr=16) and (mos <= 9 and yr=16) summer16=1.
Compute jantmar12=0.
if (year1=2012 and (mos>=1 and mos<=3)) jantmar12=1. 
Compute octtdec16=0.
if (year1=2016 and (mos>=10 and mos<=12)) octtdec16=1.

Compute jantmar12cl234=jantmar12*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute winter1516cl234=winter1516*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute summer16cl234=summer16*cl234.
Compute octtdec16cl234=octtdec16*cl234.

Compute jantmar12cl56=jantmar12*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute winter1516cl56=winter1516*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute summer16cl56=summer16*cl56.
Compute octtdec16cl56=octtdec16*cl56.

Compute jantmar12cl778=jantmar12*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute winter1516cl778=winter1516*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute summer16cl778=summer16*cl778.
Compute octtdec16cl778=octtdec16*cl778.


Compute jantmar12cl89=jantmar12*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute winter1516cl89=winter1516*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute summer16cl89=summer16*cl89.
Compute octtdec16cl89=octtdec16*cl89.

Compute jantmar12cl1112=jantmar12*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute winter1516cl1112=winter1516*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute summer16cl1112=summer16*cl1112.
Compute octtdec16cl1112=octtdec16*cl1112.

Compute jantmar12cl1095=jantmar12*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute winter1516cl1095=winter1516*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute summer16cl1095=summer16*cl1095.
Compute octtdec16cl1095=octtdec16*cl1095.

Compute jantmar12clsplt=jantmar12*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute winter1516clsplt=winter1516*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute summer16clsplt=summer16*clsplt.
Compute octtdec16clsplt=octtdec16*clsplt.



if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.


compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute nbsf=n*bsf.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute nfrabsf=n*frabsf.
Compute nfrmsbsf=n*frmsbsf.
Compute nmasbsf=n*masbsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute nfrastbsf=n*frastbsf.
Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.

If firepl>=2  firepl=firepl -1.  
compute nfirepl=n*firepl.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute nobasful=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 or basment=3 or basment=4  nobasful=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute attcfull=0.
Compute attcpart=0.
Compute attcnone=0.
If attc=1 attcfull=1.
If attc=2 attcpart=1.
If attc=3 attcnone=1.
Compute atfnliv=0.
Compute atfnapt=0.
If atfn=1 atfnliv=1.
If atfn=2 atfnapt=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute nsitedet=n*sitedetr.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.

compute b=1.

*select if year1 = 2016.
*select if puremarket=1.
*Table observation = b
                amount
               /table = tnb by 
                           amount 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount 'MED SP')
              mean (amount 'MEAN SP')
                         validn (b '# PROPS').

select if town=27.

compute mv = 156767.9931	
+ 430.9220677*nsrlsf
+ 6077.338459*nbathsum
+ 1809.918466*nbsf56
+ 1.29705884*nbsfair
- 1797.572097*sb34050
+ 3.274363523*frastbsf
+ 5.891051793*masbsf
+ 4.068117078*frmsbsf
+ 1035.329568*nbsf1112
+ 1671.788842*nbsf1095
+ 2.327822*nluxbsf
- 29835.23898*garnogar
+ 1.719911*nrepabsf
+ 20674.17845*bsfnrec
- 20121.17096*garage1
+ 6.353414513*nrenbsf
- 10794.94174*SRFX
+ 1023.060909*sb34020
+ 2997.529251*sb33040
- 1839.580969*sb27070
+ 181557.2839*archlist
+ 1553.557404*sb33030
+ 1282.956162*sb33015
- 239.9967575*sb27010
+ 576.3439066*sb27040
- 143.9982248*sb34070
+ 4881.383932*attcfull
+ 15887.48745*biggar
+ 1383.239575*nbsf234
+ 1808.828277*nsrage
- 9507.839231*garage2
- 82590.21638*nbasfull
- 87682.61603*nbaspart
- 90324.3931*nnobase
+ 61.45167253*sb27020
- 26.79561122*sb27030
+ 287.0161366*sb33050
+ 559.9223138*sb34080
- 59628.0603*onharlem
+ 1781.308714*nbsf778
+ 1525.577583*nbsf34
+ 1365.029999*nfirepl
+ 635.6490886*nbsf89
- 322.3869277*sb27080.

save outfile='C:\Users\daaaron\documents\mv27.sav'/keep town pin mv. 


