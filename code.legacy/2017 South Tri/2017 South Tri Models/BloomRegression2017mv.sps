                     *SPSS Regression.
		           *BLOOM REGRESSION  2017.


Get file='C:\Users\daaaron\documents\regt12mergefcl2b.sav'.
*select if (amount1>120000).
*select if (amount1<760000).
*select if (multi<1).

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=16 and amount1>0) year1=2016.
If (yr=15 and amount1>0) year1=2015.
If (yr=14 and amount1>0) year1=2014.
if (yr=13 and amount1>0) year1=2013.
if (yr=12 and amount1>0) year1=2012.
if (yr=11 and amount1>0) year1=2011. 
If (yr=10 and amount1>0) year1=2010.

*select if sqftb<6000.
*select if (year1>2011).
*select if puremarket=1.

COMPUTE FX = cumfile14151617.
If FX >=7 FX=7.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	32061100100000
or pin=	32061140320000
or pin=	32061240170000
or pin=	32064000380000
or pin=	32052230210000
or pin=	32052250150000
or pin=	32051150090000
or pin=	32052120420000
or pin=	32052120580000
or pin=	32053220250000
or pin=	32053220330000
or pin=	32054010020000
or pin=	32054050060000
or pin=	32081010300000
or pin=	32033080100000
or pin=	32111120080000
or pin=	33053070090000
or pin=	32063000010000
or pin=	32063000260000
or pin=	32063000280000
or pin=	32063000490000
or pin=	32063010140000
or pin=	32063010300000
or pin=	32063100340000
or pin=	32063160020000
or pin=	32063170040000
or pin=	32063170060000
or pin=	32063170190000
or pin=	32063180120000
or pin=	32071090020000
or pin=	32071090140000
or pin=	32071050260000
or pin=	32071050300000
or pin=	32072020130000
or pin=	32074030040000
or pin=	32074030230000
or pin=	32181040060000
or pin=	32181050130000
or pin=	32072030620000
or pin=	32074080060000
or pin=	32074080190000
or pin=	32074090120000
or pin=	32074100150000
or pin=	32114030180000
or pin=	32122000020000
or pin=	32122070090000
or pin=	32132040240000
or pin=	32141000200000
or pin=	33062000450000
or pin=	33073110170000
or pin=	33074180050000
or pin=	33074180060000
or pin=	32191030180000
or pin=	32173070160000
or pin=	32183080130000
or pin=	32174050230000
or pin=	32342050020000
or pin=	33311200280000
or pin=	32342060080000
or pin=	32254080070000
or pin=	32254090170000
or pin=	32351020040000
or pin=	32351030350000)   bs=1.
*select if bs=0.

Compute bsf=sqftb.
Compute N=1.
Compute tnb=(town*1000)+nghcde.


If nghcde=10 N=1.58.
If nghcde=21 N=1.34.
If nghcde=22 N=1.62.
If nghcde=31 N=1.05.
If nghcde=32 N=1.02.
If nghcde=33 N=1.49.
If nghcde=41 N=1.32.
If nghcde=42 N=1.43.
If nghcde=43 N=1.75.
If nghcde=44 N=1.45. 
If nghcde=45 N=1.38.
If nghcde=46 N=1.74.
If nghcde=51 N=1.98.
If nghcde=52 N=2.85.
If nghcde=53 N=2.47.
If nghcde=54 N=2.32.
If nghcde=61 N=1.32.
If nghcde=62 N=1.35.
If nghcde=63 N=1.01.
If nghcde=64 N=2.39.
If nghcde=65 N=.91.
If nghcde=71 N=2.57.
If nghcde=72 N=1.31.
If nghcde=73 N=1.56.
If nghcde=81 N=1.15.
If nghcde=82 N=1.39.
If nghcde=84 N=1.09.
If nghcde=85 N=1.05.
If nghcde=86 N=1.02.
If nghcde=87 N=1.01.
If nghcde=88 N=1.08.
If nghcde=91 N=1.14.
If nghcde=92 N=1.03.
If nghcde=93 N=1.04.
If nghcde=101 N=1.14.
If nghcde=111 N=.99.
If nghcde=112 N=1.18.
If nghcde=121 N=.76.
If nghcde=122 N=.72.
If nghcde=131 N=.98.
If nghcde=132 N=1.34.
If nghcde=141 N=1.05.
If nghcde=142 N=1.12.
If nghcde=150 N=1.55.
If nghcde=151 N=1.85.
If nghcde=162 N=1.41.
If nghcde=180 N=.98.
If nghcde=190 N=1.56.
If nghcde=191 N=1.69.


Compute n10=0.
If nghcde=10 n10=1.
Compute n21=0.
If nghcde=21 n21=1.
Compute n22=0.
If nghcde=22 n22=1.
Compute n31=0.
If nghcde=31 n31=1.
Compute n32=0.
If nghcde=32 n32=1.
Compute n33=0.
If nghcde=33 n33=1.
Compute n41=0.
If nghcde=41 n41=1.
Compute n42=0.
If nghcde=42 n42=1.
Compute n43=0.
If nghcde=43 n43=1.
Compute n44=0.
If nghcde=44 n44=1.
Compute n45=0.
If nghcde=45 n45=1.
Compute n46=0.
If nghcde=46 n46=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n52=0.
If nghcde=52 n52=1.
Compute n53=0.
If nghcde=53 n53=1.
Compute n54=0.
If nghcde=54 n54=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n62=0.
If nghcde=62 n62=1.
Compute n63=0.
If nghcde=63 n63=1.
Compute n64=0.
If nghcde=64 n64=1.
Compute n65=0.
If nghcde=65 n65=1.
Compute n71=0.
If nghcde=71 n71=1.
Compute n72=0.
If nghcde=72 n72=1.
Compute n73=0.
If nghcde=73 n73=1.
Compute n81=0.
If nghcde=81 n81=1.
Compute n82=0.
If nghcde=82 n82=1.
Compute n84=0.
If nghcde=84 n84=1.
Compute n85=0.
If nghcde=85 n85=1.
Compute n86=0.
If nghcde=86 n86=1.
Compute n87=0.
If nghcde=87 n87=1.
Compute n88=0.
If nghcde=88 n88=1.
Compute n91=0.
If nghcde=91 n91=1.
Compute n92=0.
If nghcde=92 n92=1.
Compute n93=0.
If nghcde=93 n93=1.
Compute n101=0.
If nghcde=101 n101=1.
Compute n111=0.
If nghcde=111 n111=1.
Compute n112=0.
If nghcde=112 n112=1.
Compute n121=0.
If nghcde=121 n121=1.
Compute n122=0.
If nghcde=122 n122=1.
Compute n131=0.
If nghcde=131 n131=1.
Compute n132=0.
If nghcde=132 n132=1.
Compute n141=0.
If nghcde=141 n141=1.
Compute n142=0.
If nghcde=142 n142=1.
Compute n150=0.
If nghcde=150 n150=1.
Compute n151=0.
If nghcde=151 n151=1.
Compute n162=0.
If nghcde=162 n162=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n190=0.
If nghcde=190 n190=1.
compute n191=0.
if nghcde=191 n191=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute sb10=sqrt(bsf)*n10.
Compute sb21=sqrt(bsf)*n21.
Compute sb22=sqrt(bsf)*n22.
Compute sb31=sqrt(bsf)*n31.
Compute sb32=sqrt(bsf)*n32.
Compute sb33=sqrt(bsf)*n33.
Compute sb41=sqrt(bsf)*n41.
Compute sb42=sqrt(bsf)*n42.
Compute sb43=sqrt(bsf)*n43.
Compute sb44=sqrt(bsf)*n44.
Compute sb45=sqrt(bsf)*n45.
Compute sb46=sqrt(bsf)*n46.
Compute sb51=sqrt(bsf)*n51.
Compute sb52=sqrt(bsf)*n52.
Compute sb53=sqrt(bsf)*n53.
Compute sb54=sqrt(bsf)*n54.
Compute sb61=sqrt(bsf)*n61.
Compute sb62=sqrt(bsf)*n62.
Compute sb63=sqrt(bsf)*n63.
Compute sb64=sqrt(bsf)*n64.
Compute sb65=sqrt(bsf)*n65.
Compute sb71=sqrt(bsf)*n71.
Compute sb72=sqrt(bsf)*n72.
Compute sb73=sqrt(bsf)*n73.
Compute sb81=sqrt(bsf)*n81.
Compute sb82=sqrt(bsf)*n82.
Compute sb84=sqrt(bsf)*n84.
Compute sb85=sqrt(bsf)*n85.
Compute sb86=sqrt(bsf)*n86.
Compute sb87=sqrt(bsf)*n87.
Compute sb88=sqrt(bsf)*n88.
Compute sb91=sqrt(bsf)*n91.
Compute sb92=sqrt(bsf)*n92.
Compute sb93=sqrt(bsf)*n93.
Compute sb101=sqrt(bsf)*n101.
Compute sb111=sqrt(bsf)*n111.
Compute sb112=sqrt(bsf)*n112.
Compute sb121=sqrt(bsf)*n121.
Compute sb122=sqrt(bsf)*n122.
Compute sb131=sqrt(bsf)*n131.
Compute sb132=sqrt(bsf)*n132.
Compute sb141=sqrt(bsf)*n141.
Compute sb142=sqrt(bsf)*n142.
Compute sb150=sqrt(bsf)*n150.
Compute sb151=sqrt(bsf)*n151.
Compute sb162=sqrt(bsf)*n162.
Compute sb180=sqrt(bsf)*n180.
Compute sb190=sqrt(bsf)*n190.
compute sb191=sqrt(bsf)*n191.

Compute nsrbsf=n*sqrt(bsf).
Compute lsf=sqftl.

If firepl>1 firepl=1.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If lsf > 15120  lsf = 15120 + ((lsf - 15120)/3).
If (class=8 or class=9) lsf=sqftl.

if nghcde=43 and class=95  lsf=4500.
if nghcde=53 and class=95  lsf=5000.
if nghcde=162 and class=95 lsf=3500.  

Compute nsrlsf=n*sqrt(lsf).

Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.

Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute winter1516=0.
if (mos > 9 and yr=15) or (mos <= 3 and yr=16) winter1516=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute summer16=0.
if (mos > 3 and yr=16) and (mos <= 9 and yr=16) summer16=1.
Compute jantmar12=0.
if (year1=2012 and (mos>=1 and mos<=3)) jantmar12=1. 
Compute octtdec16=0.
if (year1=2016 and (mos>=10 and mos<=12)) octtdec16=1.

Compute jantmar12cl234=jantmar12*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute winter1516cl234=winter1516*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute summer16cl234=summer16*cl234.
Compute octtdec16cl234=octtdec16*cl234.

Compute jantmar12cl56=jantmar12*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute winter1516cl56=winter1516*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute summer16cl56=summer16*cl56.
Compute octtdec16cl56=octtdec16*cl56.

Compute jantmar12cl778=jantmar12*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute winter1516cl778=winter1516*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute summer16cl778=summer16*cl778.
Compute octtdec16cl778=octtdec16*cl778.

Compute jantmar12cl89=jantmar12*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute winter1516cl89=winter1516*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute summer16cl89=summer16*cl89.
Compute octtdec16cl89=octtdec16*cl89.

Compute jantmar12cl1112=jantmar12*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute winter1516cl1112=winter1516*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute summer16cl1112=summer16*cl1112.
Compute octtdec16cl1112=octtdec16*cl1112.

Compute jantmar12cl1095=jantmar12*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute winter1516cl1095=winter1516*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute summer16cl1095=summer16*cl1095.
Compute octtdec16cl1095=octtdec16*cl1095.

Compute jantmar12clsplt=jantmar12*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute winter1516clsplt=winter1516*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute summer16clsplt=summer16*clsplt.
Compute octtdec16clsplt=octtdec16*clsplt.


Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
compute nbsfair=n*bsfair.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbaseful=n*basefull.
Compute nbaseprt=n*basepart.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.
compute b=1.

Compute lowzonebloom=0.
if town=12 and  (nghcde=10 or nghcde=41  or nghcde=42  or nghcde=51 
or nghcde=81 or nghcde=82 or nghcde=112 or nghcde=122
or nghcde=141 or nghcde=142) lowzonebloom=1.                         	

Compute midzonebloom=0.
if town=12 and (nghcde=21 or nghcde=22 or nghcde=31 or nghcde=33
or nghcde=44 or nghcde=46 or nghcde=52 or nghcde=53 or nghcde=54
or nghcde=61 or nghcde=63 or nghcde=64 or nghcde=71 or nghcde=86
or nghcde=87 or nghcde=88 or nghcde=91 or nghcde=111 or nghcde=132
or nghcde=150 or nghcde=151)  midzonebloom=1. 

Compute highzonebloom=0.
if town=12 and (nghcde=32 or nghcde=43 or nghcde=65  or nghcde=72 or nghcde=73 
or nghcde=84 or nghcde=85 or nghcde=93 or nghcde=101 or nghcde=121 or nghcde=162
or nghcde=190 or nghcde=191)  highzonebloom=1.


Compute srfxlowblockbloom=0.
if lowzonebloom=1 srfxlowblockbloom=srfx*lowzonebloom.

Compute srfxmidblockbloom=0.
if midzonebloom=1 srfxmidblockbloom=srfx*midzonebloom.

Compute srfxhighblockbloom=0.
if highzonebloom=1 srfxhighblockbloom=srfx*highzonebloom.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res = bnum - comm.
compute bnumb = bnum*bsf.
compute nres = n*res.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute masbsf=mason*bsf.
Compute frmsbsf=framas*bsf.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nfirepl=n*firepl.


compute cathdral=0.
if ceiling=1 cathdral=1.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
Compute biggar=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

compute b=1.

compute mv = 105484.3189	
+ 1028.361886*nbathsum
+ 2197.139624*nbsf89
+ 10088.13332*sb122
+ 1900.445176*sb51
+ 208.7551216*nsrlsf
+ 11.78638518*masbsf
+ 0.719635467*nbsfair
+ 6743.717777*sb88
+ 1692.268805*sb52
+ 21672.9742*biggar
+ 664.0074347*sb22
- 4191.702413*nsrage
- 4143.42122*garage1
+ 763.4452446*sb10
+ 350.9525134*sb46
+ 863.4303617*sb54
+ 1083.312451*sb53
+ 1596.0457795*nbsf1095
+ 8.665543165*frmsbsf
+ 8.23848393*frabsf
+ 1950.654239*nbsf778
- 333.5501675*sb82
+ 471.2299714*sb71
+ 616.1419872*sb64
- 187.1895555*sb190
- 307.1998588*sb81
+ 3778.667969*garage2
- 109.3413792*sb85
- 62947.71818*nbaseful
- 69227.94263*nbaseprt
+ 1389.85696*nfirepl
+ 11825.75858*nres
- 142.6490761*sb141
+ 1666.849694*nbsf234
+ 2147.850576*nbsf34
+ 1586.777586*nbsf1112
+ 312.064969*sb33
+ 519.437527*sb42
+ 278.601372*sb44
+ 337.605247*sb43
- 610.0454382*midzonebloom
- 14667.1646*highzonebloom
- 661.2530531*srfxlowblockbloom
- 388.770853*srfxmidblockbloom
- 187.7827956*srfxhighblockbloom
- 71930.68994*nnobase
- 10254.85817*garnogar
+ 195.9572631*sb73
+1786.229357*nbsf56.



save outfile='C:\Users\daaaron\documents\mv12.sav'/keep town pin mv. 

  
 

