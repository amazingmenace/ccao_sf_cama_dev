
                *SPSS Regression.
		*LAKEVIEW AND NORTH REGRESSION  2015.

Get file='C:\Program Files\IBM\SPSS\statistics\19\1\regt74andregt73mergefcl2.sav'.

*select if (amount1>250000).
*select if (amount1<3000000).
*select if (multi<1).
*select if sqftb<9000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr1. 
If  (yr1=14 and amount1>0) year1=2014.
If  (yr1=13 and amount1>0) year1=2013.
If  (yr1=12 and amount1>0) year1=2012.  
If  (yr1=11 and amount1>0) year1=2011.
If  (yr1=10 and amount1>0) year1=2010.
If  (yr1=9 and amount1>0)  year1=2009.
If  (yr1=8 and amount1>0)  year1=2008.
If  (yr1=7 and amount1>0)  year1=2007.  
If  (yr1=6 and amount1>0)  year1=2006. 
If  (yr1=5 and amount1>0)  year1=2005.
If  (yr1=4 and amount1>0)  year1=2004.  
If  (yr1=3 and amount1>0)  year1=2003. 
If  (yr1=2 and amount1>0)  year1=2002.  
If  (yr1=1 and amount1>0)  year1=2001.   
If  (yr1=0 and amount1>0)  year1=2000.

 *NEW - ONLY USE 5 YEARS OF SALES.                                           
*select if (year1>2009). 

                                              
*NEW - ONLY *select PURE SALES(CREATE PURESALE VARIABLE IN REGT).                                        
*select if puremarket=1.                                               

Compute bs=0.
*if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	14053110230000
or pin=	14053110260000
or pin=	14053160290000
or pin=	14061040040000
or pin=	14061070030000
or pin=	14061100090000
or pin=	14061180240000
or pin=	14062050090000
or pin=	14062100070000
or pin=	14062150210000
or pin=	14062170120000
or pin=	14062200710000
or pin=	14064010550000
or pin=	17041080100000
or pin=	17041080180000
or pin=	17041080200000
or pin=	17041080510000
or pin=	17041220410000
or pin=	17041310020000
or pin=	17042010150000
or pin=	17042020280000
or pin=	17042031130000
or pin=	17042031340000
or pin=	17042031350000
or pin=	17042120060000
or pin=	17042140720000
or pin=	17042190740000
or pin=	17042200640000
or pin=	17042200650000
or pin=	14071090180000
or pin=	14071090210000
or pin=	14071090460000
or pin=	14071090470000
or pin=	14071100240000
or pin=	14071120090000
or pin=	14071130060000
or pin=	14071130100000
or pin=	14071130110000
or pin=	14071150150000
or pin=	14071200030000
or pin=	14071200060000
or pin=	14071200280000
or pin=	14072100060000
or pin=	14072170150000
or pin=	14072200020000
or pin=	14072220100000
or pin=	14072260190000
or pin=	14072270150000
or pin=	14073000160000
or pin=	14073000170000
or pin=	14073030030000
or pin=	14073030130000
or pin=	14073040340000
or pin=	14073050300000
or pin=	14073070110000
or pin=	14073070160000
or pin=	14073100090000
or pin=	14073130030000
or pin=	14073130310000
or pin=	14073140140000
or pin=	14073140250000
or pin=	14073220180000
or pin=	14073230060000
or pin=	14073240200000
or pin=	14321010090000
or pin=	14321010220000
or pin=	14321010290000
or pin=	14321010360000
or pin=	14321010510000
or pin=	14321020140000
or pin=	14321020230000
or pin=	14321020240000
or pin=	14321020330000
or pin=	14321020360000
or pin=	14321030300000
or pin=	14321030380000
or pin=	14321030400000
or pin=	14321040070000
or pin=	14321040270000
or pin=	14321070420000
or pin=	14321080110000
or pin=	14321080260000
or pin=	14321080330000
or pin=	14321080390000
or pin=	14321090190000
or pin=	14321090200000
or pin=	14321090210000
or pin=	14321090220000
or pin=	14321090250000
or pin=	14321090260000
or pin=	14321100080000
or pin=	14321100180000
or pin=	14321100250000
or pin=	14321120190000
or pin=	14321130020000
or pin=	14321130180000
or pin=	14321240220000
or pin=	14321240270000
or pin=	14321240300000
or pin=	14321250300000
or pin=	14321250380000
or pin=	14321270160000
or pin=	14321340010000
or pin=	14321340270000
or pin=	14322040160000
or pin=	14322040260000
or pin=	14322040320000
or pin=	14322040330000
or pin=	14322060310000
or pin=	14322060390000
or pin=	14322070530000
or pin=	14322090210000
or pin=	14322090260000
or pin=	14322090290000
or pin=	14322090530000
or pin=	14322090540000
or pin=	14322100040000
or pin=	14322110180000
or pin=	14322110380000
or pin=	14322120100000
or pin=	14322120180000
or pin=	14322120210000
or pin=	14322120300000
or pin=	14322120320000
or pin=	14322140210000
or pin=	14322160270000
or pin=	14322160370000
or pin=	14322170230000
or pin=	14322170290000
or pin=	14322170320000
or pin=	14322170400000
or pin=	14322170410000
or pin=	14322170430000
or pin=	14322180280000
or pin=	14322180410000
or pin=	14322190420000
or pin=	14322210130000
or pin=	14322210220000
or pin=	14322220160000
or pin=	14322230090000
or pin=	14322230240000
or pin=	14322250080000
or pin=	14322250110000
or pin=	14322250230000
or pin=	14322260170000
or pin=	14322260450000
or pin=	14322270400000
or pin=	14322280320000
or pin=	14324030120000
or pin=	14324030560000
or pin=	14324030680000
or pin=	14324070680000
or pin=	14324080100000
or pin=	14324080140000
or pin=	14324080240000
or pin=	14324080480000
or pin=	14324090100000
or pin=	14324100530000
or pin=	14324110100000
or pin=	14324110220000
or pin=	14324110580000
or pin=	14324110610000
or pin=	14324120200000
or pin=	14324120230000
or pin=	14324120440000
or pin=	14324140320000
or pin=	14324150020000
or pin=	14324210010000
or pin=	14324210160000
or pin=	14324210190000
or pin=	14324210230000
or pin=	14324210240000
or pin=	14324250490000
or pin=	14324260120000
or pin=	14331000150000
or pin=	14331000240000
or pin=	14331020250000
or pin=	14331020340000
or pin=	14331070420000
or pin=	14331080070000
or pin=	14331080090000
or pin=	14331100380000
or pin=	14331130280000
or pin=	14331210580000
or pin=	14331220350000
or pin=	14331220470000
or pin=	14331230210000
or pin=	14331240080000
or pin=	14331280420000
or pin=	14331280470000
or pin=	14331280570000
or pin=	14331280580000
or pin=	14331280680000
or pin=	14331290510000
or pin=	14331300350000
or pin=	14331300420000
or pin=	14331300470000
or pin=	14331300480000
or pin=	14331300550000
or pin=	14331300620000
or pin=	14332050040000
or pin=	14332050640000
or pin=	14333000620000
or pin=	14333000750000
or pin=	14333010060000
or pin=	14333010890000
or pin=	14333031400000
or pin=	14333050010000
or pin=	14333070350000
or pin=	14333080220000
or pin=	14333080590000
or pin=	14333090450000
or pin=	14333090500000
or pin=	14333110200000
or pin=	14333110400000
or pin=	14333170300000
or pin=	14333170460000
or pin=	14333180620000
or pin=	14333240270000
or pin=	14333310240000
or pin=	14333310350000
or pin=	14333310780000
or pin=	14334000440000
or pin=	14334010280000
or pin=	14334040190000
or pin=	14334060100000
or pin=	14334060110000
or pin=	14334070120000
or pin=	14334080330000
or pin=	14334120480000
or pin=	14334140060000
or pin=	14334140070000
or pin=	14334140080000
or pin=	14334200140000
or pin=	14334210040000
or pin=	14334210150000
or pin=	14051200040000
or pin=	14051220420000
or pin=	14051230300000
or pin=	14051280050000
or pin=	17031030200000
or pin=	17032000420000
or pin=	17032010060000
or pin=	17032010080000
or pin=	17042070800000
or pin=	17042150500000
or pin=	17042170810000
or pin=	17042170820000
or pin=	17042171050000
or pin=	17042180210000
or pin=	17042180230000
or pin=	17044240310000
or pin=	17044240420000
or pin=	17044240460000
or pin=	17044240480000
or pin=	17044240490000
or pin=	17044310090000
or pin=	17044400130000
or pin=	17092390220000
or pin=	17102210280000
or pin=	17102210380000
or pin=	14051000360000
or pin=	14051030220000
or pin=	14051060160000
or pin=	14051060170000
or pin=	14051140070000
or pin=	14051160070000
or pin=	14051190030000
or pin=	14053020190000
or pin=	14053100550000
or pin=	14053130120000
or pin=	14053180110000
or pin=	14053180220000
or pin=	14053180290000
or pin=	14053210320000
or pin=	14053210410000
or pin=	14053230300000
or pin=	14053260030000
or pin=	14053300210000
or pin=	14054010200000
or pin=	14064070290000
or pin=	14064080290000
or pin=	14064100190000
or pin=	14064100300000
or pin=	14064120270000
or pin=	14072050140000
or pin=	14072060240000
or pin=	14072080380000
or pin=	14072180140000
or pin=	14072190380000
or pin=	14072300020000
or pin=	14081070250000
or pin=	14081230300000
or pin=	14081230350000
or pin=	14081240070000
or pin=	14082040320000
or pin=	14082070170000
or pin=	14082080050000
or pin=	14082080550000
or pin=	14082110150000
or pin=	14074030040000
or pin=	14074040080000
or pin=	14074040120000
or pin=	14074040130000
or pin=	14074080500000
or pin=	14074090210000
or pin=	14074130060000
or pin=	14074130290000
or pin=	14074220320000
or pin=	14074220400000
or pin=	14074230110000
or pin=	14083020320000
or pin=	14083040290000
or pin=	14083050240000
or pin=	14083060130000
or pin=	14083070110000
or pin=	14083070390000
or pin=	14083080150000
or pin=	14083080370000
or pin=	14083090180000
or pin=	14083110140000
or pin=	14083120030000
or pin=	14083170250000
or pin=	14084090060000
or pin=	14171020070000
or pin=	14171140160000
or pin=	14171140250000
or pin=	14171190020000
or pin=	14173030330000
or pin=	14173080150000
or pin=	14173080240000
or pin=	14173080370000
or pin=	14182030160000
or pin=	14182100380000
or pin=	14182160030000
or pin=	14182220060000
or pin=	14182220310000
or pin=	14184040180000
or pin=	14184040200000
or pin=	14184080180000
or pin=	14184080260000
or pin=	14184130240000
or pin=	14184140340000
or pin=	14184140410000
or pin=	14184150370000
or pin=	14184200240000
or pin=	14184210030000
or pin=	14184210150000
or pin=	14184220090000
or pin=	14081040150000
or pin=	14081100280000
or pin=	14081110090000
or pin=	14081120070000
or pin=	14081130240000
or pin=	14081190230000
or pin=	14081200070000
or pin=	14081260040000
or pin=	14181020150000
or pin=	14181020240000
or pin=	14181060120000
or pin=	14181170190000
or pin=	14181210030000
or pin=	14181220180000
or pin=	14181220210000
or pin=	14181230200000
or pin=	14181240080000
or pin=	14181240090000
or pin=	14181240390000
or pin=	14183000140000
or pin=	14183010170000
or pin=	14183010290000
or pin=	14183010390000
or pin=	14183020020000
or pin=	14183040110000
or pin=	14183090130000
or pin=	14183090150000
or pin=	14183090470000
or pin=	14183100150000
or pin=	14183150090000
or pin=	14183150180000
or pin=	14183150410000
or pin=	14183160360000
or pin=	14183180140000
or pin=	14183180170000
or pin=	14183180290000
or pin=	14183210220000
or pin=	14183270010000
or pin=	14183270180000
or pin=	14183270190000
or pin=	14183290080000
or pin=	14183290290000
or pin=	14183290440000
or pin=	14183290490000
or pin=	14183300020000
or pin=	14181080180000
or pin=	14181090170000
or pin=	14181130020000
or pin=	14181130070000
or pin=	14181140030000
or pin=	14181140100000
or pin=	14181250100000
or pin=	14181270320000
or pin=	14181280310000
or pin=	14181310170000
or pin=	14181320080000
or pin=	14181320170000
or pin=	14182000310000
or pin=	14182180100000
or pin=	14182190310000
or pin=	14183050030000
or pin=	14184020110000
or pin=	14184060380000
or pin=	14184070060000
or pin=	14184110100000
or pin=	14184120020000
or pin=	14184160290000
or pin=	14184170070000
or pin=	14184180250000
or pin=	14184180280000
or pin=	14184190180000
or pin=	14184190240000
or pin=	14184230270000
or pin=	14173020090000
or pin=	14173040130000
or pin=	14173040280000
or pin=	14173040440000
or pin=	14173050010000
or pin=	14173050310000
or pin=	14173090450000
or pin=	14173100030000
or pin=	14173130210000
or pin=	14173130300000
or pin=	14173140140000
or pin=	14173150190000
or pin=	14173150200000
or pin=	14173150210000
or pin=	14173150220000
or pin=	14173150230000
or pin=	14173150240000
or pin=	14173150260000
or pin=	14173150300000
or pin=	14173150320000
or pin=	14173150380000
or pin=	14173150440000
or pin=	14173150450000
or pin=	14173150460000
or pin=	14173150600000
or pin=	14084110040000
or pin=	14084150040000
or pin=	14171160050000
or pin=	14174010610000
or pin=	14084130120000
or pin=	14084180140000
or pin=	14084180300000
or pin=	14084180330000
or pin=	14163040520000
or pin=	14213050250000
or pin=	14281050570000
or pin=	14281090310000
or pin=	14281180210000
or pin=	14282030350000
or pin=	14283040190000
or pin=	14283050500000
or pin=	14283090240000
or pin=	14283110180000
or pin=	14283110520000
or pin=	14283120310000
or pin=	14283120400000
or pin=	14283120650000
or pin=	14283150260000
or pin=	14283150270000
or pin=	14283150340000
or pin=	14283170210000
or pin=	14283170250000
or pin=	14283180450000
or pin=	14283210280000
or pin=	14283210290000
or pin=	14191040340000
or pin=	14191050300000
or pin=	14191080060000
or pin=	14191080350000
or pin=	14191090130000
or pin=	14191090290000
or pin=	14191090310000
or pin=	14191090380000
or pin=	14191100020000
or pin=	14191100070000
or pin=	14191100180000
or pin=	14191100310000
or pin=	14191120080000
or pin=	14191120260000
or pin=	14191140200000
or pin=	14191150210000
or pin=	14191150320000
or pin=	14191190150000
or pin=	14191200010000
or pin=	14191210180000
or pin=	14191220020000
or pin=	14191230010000
or pin=	14191230240000
or pin=	14191250160000
or pin=	14191260200000
or pin=	14191270040000
or pin=	14191270140000
or pin=	14191270240000
or pin=	14191280200000
or pin=	14191280240000
or pin=	14191280370000
or pin=	14191290120000
or pin=	14191290320000
or pin=	14191290380000
or pin=	14191300130000
or pin=	14191300280000
or pin=	14191310230000
or pin=	14193000060000
or pin=	14193010020000
or pin=	14193010360000
or pin=	14193020210000
or pin=	14193030210000
or pin=	14193030340000
or pin=	14193040410000
or pin=	14193060070000
or pin=	14193060330000
or pin=	14193090080000
or pin=	14193090390000
or pin=	14193100150000
or pin=	14193120100000
or pin=	14193140420000
or pin=	14193150030000
or pin=	14193220380000
or pin=	14193240180000
or pin=	14193250120000
or pin=	14193280110000
or pin=	14194000110000
or pin=	14194000270000
or pin=	14194020070000
or pin=	14194020230000
or pin=	14194020320000
or pin=	14194030410000
or pin=	14194090070000
or pin=	14194110060000
or pin=	14194210240000
or pin=	14194280190000
or pin=	14194320510000
or pin=	14194330380000
or pin=	14192010170000
or pin=	14192020020000
or pin=	14192020040000
or pin=	14192020060000
or pin=	14192030080000
or pin=	14192080140000
or pin=	14192080220000
or pin=	14192080260000
or pin=	14192090170000
or pin=	14192100190000
or pin=	14192110170000
or pin=	14192110200000
or pin=	14192120140000
or pin=	14192120240000
or pin=	14192130020000
or pin=	14192130130000
or pin=	14192130140000
or pin=	14192140330000
or pin=	14192160040000
or pin=	14192170350000
or pin=	14192220100000
or pin=	14192220430000
or pin=	14192230050000
or pin=	14192230170000
or pin=	14192240090000
or pin=	14192270260000
or pin=	14192310070000
or pin=	14192310190000
or pin=	14194070140000
or pin=	14194130140000
or pin=	14194140130000
or pin=	14194150010000
or pin=	14194160060000
or pin=	14194170230000
or pin=	14194220280000
or pin=	14194230340000
or pin=	14194250030000
or pin=	14194290030000
or pin=	14194290150000
or pin=	14194290350000
or pin=	14194340170000
or pin=	14194350170000
or pin=	14302000010000
or pin=	14302000300000
or pin=	14302020420000
or pin=	14302030210000
or pin=	14302030390000
or pin=	14302050060000
or pin=	14302050100000
or pin=	14302060150000
or pin=	14302080440000
or pin=	14302090080000
or pin=	14302100160000
or pin=	14302100240000
or pin=	14302100290000
or pin=	14302110020000
or pin=	14302130150000
or pin=	14302130460000
or pin=	14302150300000
or pin=	14302150380000
or pin=	14302170070000
or pin=	14302170140000
or pin=	14302170300000
or pin=	14302170370000
or pin=	14302180140000
or pin=	14302180210000
or pin=	14302190120000
or pin=	14302200390000
or pin=	14302210020000
or pin=	14302210240000
or pin=	14302210380000
or pin=	14302250100000
or pin=	14304050260000
or pin=	14304050360000
or pin=	14304080060000
or pin=	14304080500000
or pin=	14304090060000
or pin=	14304090410000
or pin=	14304090420000
or pin=	14304100080000
or pin=	14304100100000
or pin=	14304100110000
or pin=	14304100130000
or pin=	14201000260000
or pin=	14201000330000
or pin=	14201000570000
or pin=	14201010200000
or pin=	14201010270000
or pin=	14201030560000
or pin=	14201070050000
or pin=	14201070110000
or pin=	14201100320000
or pin=	14201130100000
or pin=	14201150230000
or pin=	14201150240000
or pin=	14201190150000
or pin=	14201210220000
or pin=	14201240270000
or pin=	14201240340000
or pin=	14201240480000
or pin=	14201250100000
or pin=	14201260050000
or pin=	14201260060000
or pin=	14202010020000
or pin=	14202010130000
or pin=	14202010170000
or pin=	14202010240000
or pin=	14202050040000
or pin=	14202050080000
or pin=	14202100050000
or pin=	14202110030000
or pin=	14202110380000
or pin=	14202120130000
or pin=	14202120150000
or pin=	14202140320000
or pin=	14202140350000
or pin=	14202140360000
or pin=	14202150320000
or pin=	14202160180000
or pin=	14202170090000
or pin=	14202180300000
or pin=	14202200040000
or pin=	14202200050000
or pin=	14202200300000
or pin=	14202250250000
or pin=	14202290020000
or pin=	14202290030000
or pin=	14202290200000
or pin=	14203000020000
or pin=	14203000110000
or pin=	14203010280000
or pin=	14203020110000
or pin=	14203040180000
or pin=	14203070050000
or pin=	14203070140000
or pin=	14203070150000
or pin=	14203070210000
or pin=	14203090270000
or pin=	14203100030000
or pin=	14203120020000
or pin=	14203140090000
or pin=	14203150250000
or pin=	14203150290000
or pin=	14203160250000
or pin=	14203180360000
or pin=	14203190280000
or pin=	14203190330000
or pin=	14203210340000
or pin=	14203220270000
or pin=	14203230070000
or pin=	14203240350000
or pin=	14203250150000
or pin=	14203260340000
or pin=	14203260510000
or pin=	14203300060000
or pin=	14203300310000
or pin=	14204030520000
or pin=	14204050230000
or pin=	14204100230000
or pin=	14204110170000
or pin=	14204120110000
or pin=	14204120120000
or pin=	14204120220000
or pin=	14204120250000
or pin=	14204120400000
or pin=	14204200530000
or pin=	14204220090000
or pin=	14204230100000
or pin=	14204240300000
or pin=	14204260440000
or pin=	14292000370000
or pin=	14292000470000
or pin=	14292030160000
or pin=	14292040310000
or pin=	14292060460000
or pin=	14213030310000
or pin=	14213110460000
or pin=	14213130180000
or pin=	14213130290000
or pin=	14281040630000
or pin=	14281040950000
or pin=	14283030380000
or pin=	14283030680000
or pin=	14293000040000
or pin=	14293000380000
or pin=	14293000570000
or pin=	14293000900000
or pin=	14293000910000
or pin=	14293000930000
or pin=	14293000940000
or pin=	14293000960000
or pin=	14293000970000
or pin=	14293000990000
or pin=	14293001030000
or pin=	14293001040000
or pin=	14293001090000
or pin=	14293010400000
or pin=	14293010620000
or pin=	14293021800000
or pin=	14293030050000
or pin=	14293030390000
or pin=	14293040670000
or pin=	14293080350000
or pin=	14293080490000
or pin=	14293110520000
or pin=	14293130170000
or pin=	14293130400000
or pin=	14293140300000
or pin=	14293150340000
or pin=	14293150460000
or pin=	14293150590000
or pin=	14293150630000
or pin=	14293150760000
or pin=	14293170070000
or pin=	14293180190000
or pin=	14293190020000
or pin=	14293190270000
or pin=	14293200120000
or pin=	14293200250000
or pin=	14294010280000
or pin=	14294060360000
or pin=	14294080110000
or pin=	14294080210000
or pin=	14294090540000
or pin=	14294110120000
or pin=	14294120390000
or pin=	14294130150000
or pin=	14294130250000
or pin=	14294160760000
or pin=	14294160770000
or pin=	14294160810000
or pin=	14294200440000
or pin=	14294210040000
or pin=	14294210290000
or pin=	14294220130000
or pin=	14294230240000
or pin=	14294230430000
or pin=	14294270370000
or pin=	14301020030000
or pin=	14301040160000
or pin=	14301050010000
or pin=	14301050060000
or pin=	14301061090000
or pin=	14301070110000
or pin=	14301090290000
or pin=	14301110090000
or pin=	14301110320000
or pin=	14301130190000
or pin=	14301190250000
or pin=	14301210070000
or pin=	14291040190000
or pin=	14291050200000
or pin=	14291050460000
or pin=	14291080170000
or pin=	14291080250000
or pin=	14291090290000
or pin=	14291120180000
or pin=	14291120220000
or pin=	14291140190000
or pin=	14291160260000
or pin=	14291180260000
or pin=	14291200190000
or pin=	14291250230000
or pin=	14291250280000
or pin=	14291280140000
or pin=	14281140010000
or pin=	14281150320000
or pin=	14281150330000
or pin=	14291030210000
or pin=	14291070130000
or pin=	14291150040000
or pin=	14291150330000
or pin=	14291190020000
or pin=	14291190140000
or pin=	14291230360000
or pin=	14291270130000
or pin=	14291310340000
or pin=	14292090180000
or pin=	14292150300000
or pin=	14292160030000
or pin=	14292160060000
or pin=	14292190060000
or pin=	14292200200000
or pin=	14292200260000
or pin=	14292230090000
or pin=	14292270550000
or pin=	14292280060000
or pin=	14302220310000
or pin=	14302220530000
or pin=	14302220550000
or pin=	14302220580000
or pin=	14302220670000
or pin=	14302220750000
or pin=	14302220760000
or pin=	14302220790000
or pin=	14302220810000
or pin=	14302220850000
or pin=	14302220900000
or pin=	14302221010000
or pin=	14302221040000
or pin=	14302221060000
or pin=	14302221150000
or pin=	14302221220000
or pin=	14302221340000
or pin=	14302221390000
or pin=	14302221400000
or pin=	14304031200000)  bs=1.
*select if bs=0.

compute bsf=sqftb.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
Compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

compute tnb=(town*1000) + nghcde.

if (tnb= 74011 ) n=8.35.
if (tnb= 74012 ) n=12.00.
if (tnb= 74013 ) n=7.50.
if (tnb= 74022 ) n=16.65.
if (tnb= 74030 ) n=11.57.
if (tnb= 73011 ) n=3.86.
if (tnb= 73012 ) n=4.75.
if (tnb= 73022 ) n=5.60.
if (tnb= 73031 ) n=5.15.
if (tnb= 73032 ) n=6.70.
if (tnb= 73034 ) n=7.63.
if (tnb= 73041 ) n=6.75.
if (tnb= 73042 ) n=6.75.
if (tnb= 73044 ) n=6.43.
if (tnb= 73050 ) n=5.77.
if (tnb= 73060 ) n=7.90.
if (tnb= 73062 ) n=7.12.
if (tnb= 73063 ) n=11.30.
if (tnb= 73070 ) n=7.06.
if (tnb= 73081 ) n=7.21.
if (tnb= 73084 ) n=8.36.
if (tnb= 73092 ) n=11.16.
if (tnb= 73093 ) n=8.57.
if (tnb= 73110 ) n=6.15.
if (tnb= 73120 ) n=7.11.
if (tnb= 73150 ) n=8.80.
if (tnb= 73200 ) n=8.02.

Compute nbsf=n*bsf.



Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute cl10=0.
if class=10 cl10=1.
Compute nbsf10=n*cl10*sqrt(bsf).
compute cl95=0.
if class=95 cl95=1.
Compute nbsf95=n*cl95*sqrt(bsf).

compute cl34=0.
if class=34 cl34=1.
Compute nbsf34=n*cl34*sqrt(bsf).  

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


*CREATE NEW TOWN AND NEIGHBORHOOD VARIABLES(FOR BUILDING SQUARE FOOT). 
compute sb74011=0.                              
compute sb74012=0.
compute sb74013=0.
compute sb74022=0.
compute sb74030=0.
compute sb73011=0.
compute sb73012=0.
compute sb73022=0.
compute sb73031=0.
compute sb73032=0.
compute sb73034=0.
compute sb73041=0.
compute sb73042=0.
compute sb73044=0.
compute sb73050=0.
compute sb73060=0.
compute sb73062=0.
compute sb73063=0.
compute sb73070=0.
compute sb73081=0.
compute sb73084=0.
compute sb73092=0.
compute sb73093=0.
compute sb73110=0.
compute sb73120=0.
compute sb73150=0.
compute sb73200=0.

compute n74011=0.
compute n74012=0.
compute n74013=0.
compute n74022=0.
compute n74030=0.
compute n73011=0.
compute n73012=0.
compute n73022=0.
compute n73031=0.
compute n73032=0.
compute n73034=0.
compute n73041=0.
compute n73042=0.
compute n73044=0.
compute n73050=0.
compute n73060=0.
compute n73062=0.
compute n73063=0.
compute n73070=0.
compute n73081=0.
compute n73084=0.
compute n73092=0.
compute n73093=0.
compute n73110=0.
compute n73120=0.
compute n73150=0.
compute n73200=0.

if (tnb= 74011 ) n74011=1.                
if (tnb= 74012 ) n74012=1.
if (tnb= 74013 ) n74013=1.
if (tnb= 74022 ) n74022=1.
if (tnb= 74030 ) n74030=1.
if (tnb= 73011 ) n73011=1.
if (tnb= 73012 ) n73012=1.
if (tnb= 73022 ) n73022=1.
if (tnb= 73031 ) n73031=1.
if (tnb= 73032 ) n73032=1.
if (tnb= 73034 ) n73034=1.
if (tnb= 73041 ) n73041=1.
if (tnb= 73042 ) n73042=1.
if (tnb= 73044 ) n73044=1.
if (tnb= 73050 ) n73050=1.
if (tnb= 73060 ) n73060=1.
if (tnb= 73062 ) n73062=1.
if (tnb= 73063 ) n73063=1.
if (tnb= 73070 ) n73070=1.
if (tnb= 73081 ) n73081=1.
if (tnb= 73084 ) n73084=1.
if (tnb= 73092 ) n73092=1.
if (tnb= 73093 ) n73093=1.
if (tnb= 73110 ) n73110=1.
if (tnb= 73120 ) n73120=1.
if (tnb= 73150 ) n73150=1.
if (tnb= 73200 ) n73200=1.


*NEW - CREATE TOWN AND NEIGHBORHOOD COMBINATION VARIABLES FOR SQRT*BUILDING SQUARE FOOT.
if (tnb= 74011 ) sb74011=sqrt(bsf).              
if (tnb= 74012 ) sb74012=sqrt(bsf).
if (tnb= 74013 ) sb74013=sqrt(bsf).
if (tnb= 74022 ) sb74022=sqrt(bsf).
if (tnb= 74030 ) sb74030=sqrt(bsf).
if (tnb= 73011 ) sb73011=sqrt(bsf).
if (tnb= 73012 ) sb73012=sqrt(bsf).
if (tnb= 73022 ) sb73022=sqrt(bsf).
if (tnb= 73031 ) sb73031=sqrt(bsf).
if (tnb= 73032 ) sb73032=sqrt(bsf).
if (tnb= 73034 ) sb73034=sqrt(bsf).
if (tnb= 73041 ) sb73041=sqrt(bsf).
if (tnb= 73042 ) sb73042=sqrt(bsf).
if (tnb= 73044 ) sb73044=sqrt(bsf).
if (tnb= 73050 ) sb73050=sqrt(bsf).
if (tnb= 73060 ) sb73060=sqrt(bsf).
if (tnb= 73062 ) sb73062=sqrt(bsf).
if (tnb= 73063 ) sb73063=sqrt(bsf).
if (tnb= 73070 ) sb73070=sqrt(bsf).
if (tnb= 73081 ) sb73081=sqrt(bsf).
if (tnb= 73084 ) sb73084=sqrt(bsf).
if (tnb= 73092 ) sb73092=sqrt(bsf).
if (tnb= 73093 ) sb73093=sqrt(bsf).
if (tnb= 73110 ) sb73110=sqrt(bsf).
if (tnb= 73120 ) sb73120=sqrt(bsf).
if (tnb= 73150 ) sb73150=sqrt(bsf).
if (tnb= 73200 ) sb73200=sqrt(bsf).

*******************************************************************************************************************.

***** Compute Block-Level Filings.
* There are two variations of this variable created below:.
* 1) FX includes the own property when counting "block-level filings".
* 2) FX2 excludes the own property when counting "block-level filings".
* Either variable can be included one at a time in the regression; they should not both be included in the same regression.
* Either variable yields similar regression results.
*****.
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile12131415.
RECODE FX (SYSMIS=0).
If FX >= 3  FX=3.


********************************************************************************************.
************* The next section computes low , mid and high foreclosure zones  for Lakeview.
********************************************************************************************.

compute midzonelakeview=0.
if town=73 and (nghcde=11 or nghcde=110) midzonelakeview=1.

compute lowzonelakeview=0.
if town=73 and (midzonelakeview=0) lowzonelakeview=1.


compute fxlowblocklakeview=0.
if lowzonelakeview=1 fxlowblocklakeview=fx*lowzonelakeview.

compute fxmidblocklakeview=0.
if midzonelakeview=1  fxmidblocklakeview=fx*midzonelakeview.


*******************************************************************************************.
*************** This section computes low, mid and high foreclosure zones for North.
*******************************************************************************************.


compute lowzonenorth=0.
if town=74 and (nghcde=11 or nghcde=12 or nghcde=22 or nghcde=30) lowzonenorth=1.

compute midzonenorth=0.
if town=74 and (nghcde=13)  midzonenorth=1.

 
compute fxlowblocknorth=0.
if lowzonenorth=1 fxlowblocknorth=fx*lowzonenorth.

compute fxmidblocknorth=0.
if midzonenorth=1 fxmidblocknorth=fx*midzonenorth.


*******************************************************************************************************************.
******.


*NEW - CREATE NEW  TIME DUMMY VARIABLES BROKEN DOWN INTO 6 MONTH TIME PERIODS, BY TIME OF YEAR(SUMMER AND WINTER) FOR LAST 5 YEARS.   


Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1. 
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute jantmar10=0.
if (year1=2010 and (mos>=1 and mos<=3)) jantmar10=1. 
Compute octtdec14=0.
if (year1=2014 and (mos>=11 and mos<=13)) octtdec14=1.

Compute jantmar10cl234=jantmar10*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute octtdec14cl234=octtdec14*cl234.

Compute jantmar10cl56=jantmar10*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute octtdec14cl56=octtdec14*cl56.

Compute jantmar10cl778=jantmar10*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute octtdec14cl778=octtdec14*cl778.

Compute jantmar10cl89=jantmar10*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute octtdec14cl89=octtdec14*cl89.

Compute jantmar10cl1112=jantmar10*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute octtdec14cl1112=octtdec14*cl1112.

Compute jantmar10cl1095=jantmar10*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute octtdec14cl1095=octtdec14*cl1095.

Compute jantmar10cl34=jantmar10*cl34.
Compute winter1011cl34=winter1011*cl34.
Compute winter1112cl34=winter1112*cl34.
Compute winter1213cl34=winter1213*cl34.
Compute winter1314cl34=winter1314*cl34.
Compute summer10cl34=summer10*cl34.
Compute summer11cl34=summer11*cl34.
Compute summer12cl34=summer12*cl34.
Compute summer13cl34=summer13*cl34.
Compute summer14cl34=summer14*cl34.
Compute octtdec14cl34=octtdec14*cl34.

Compute cathdral=0.
If ceiling=1 cathdral=1.

Compute LV=0.
If town=73 LV=1.
Compute NO=0.
If town=74 NO=1.

*If sqftl >= 3250  sqftl=3250. 
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute lvlsf=lv*sqrt(lsf).
compute nolsf=no*sqrt(lsf).

if tnb=73011 and class=95   lsf=1373.
if tnb=73012 and class=95   lsf=1213.
if tnb=73031 and class=95   lsf=1070.
if tnb=73032 and class=95   lsf=1339.
if tnb=73042 and class=95   lsf=1091.
if tnb=73044 and class=95   lsf=1048.
if tnb=73050 and class=95   lsf=1238.
if tnb=73062 and class=95   lsf=1804.
if tnb=73081 and class=95   lsf=1182.
if tnb=73084 and class=95   lsf=982.
if tnb=73093 and class=95   lsf=1187.
if tnb=73120 and class=95   lsf=1028.
if tnb=73150 and class=95   lsf=1368.
if tnb=73200 and class=95   lsf=1260.
if tnb=74011 and class=95   lsf=1167.
if tnb=74012 and class=95   lsf=1137.
if tnb=74013 and class=95   lsf=891.
if tnb=74022 and class=95   lsf=1362.
if tnb=74030 and class=95   lsf=1231.

Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.


Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.
Compute srbsfage=srbsf*srage.


Compute nclass95=n*class95.
Compute nclass10=n*class10.
Compute no95=0.
If class95=0 no95=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.



Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=npremrf*bsf.

If firepl>=2 firepl=2.
compute nfirepl=0.
compute nfirepl=n*firepl.

Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.
compute garage1=0.
if gar=1 or gar=2 garage1=1.
compute garage2=0.
if gar=3 or gar=4 garage2=1.
compute biggar=0.
if gar=5 or gar=6 or gar=8 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 and (class=2 or class=3 or class=4 or class=5 or class=6 or class=7 or class=8 or class=9 or class=10 or class=34
or class=78 or class=95)  bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.


Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute planarch=0.
Compute planstoc=0.
If plan=1 planarch=1.
If plan=2 planstoc=1.
Compute archbsf=planarch*bsf.
Compute narchbsf=n*planarch*bsf.
Compute deluxbsf=qualdlux*bsf.
Compute poorbsf=qualpoor*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute sitedbsf=sitedetr*bsf.

Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.

Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.


Compute noapt=0.
Compute apt=class11 + class12.
if apt<1 noapt=1.

compute nsrbsfs=nsrbsf*noapt.
compute napsrbsf=nsrbsf*apt.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.

compute rs1519sf=0.
if sqftb>1200 rs1519sf=rs1519*sqftb.
compute nmasbsf=n*masbsf.
compute aptmas=masbsf*apt.
compute frmsbsf=sqftb*framas.

*compute lfrno=0.
*if(14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
(17031000000000<pin and pin <17033999999999) lfrno=1.
*compute lake=0.
*if ((14332000000000<pin and pin<14332999999999) or (14334000000000 <pin and pin<14334999999999) or 
  tnb=73062 or tnb= 73063 or tnb=74030 or tnb=74022) lake=1.
*compute good95=0.
*if ((14331230520000<pin and pin<14331230570000) or (14331240380000 <pin and pin<14331240490000) or 
   (17044240270000<pin and pin<17044240490000) or (17044310240000 <pin and pin<17044310270000)) good95=1.

*compute lakel=lake*lsf.
*compute lakesrl=lake*srlsf.
*compute aptlakel=apt*lake*lsf.
*compute aptnlsf=nlsf*apt.
*compute aptlux=apt*nlux.
*compute aptlsrl=apt*lake*srlsf.

*compute lsf95=class95*lsf.
*compute nlsf95=n*lsf95.
*compute lux95=nlux*class95.
*compute good95b=good95*srbsf.

compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.

compute nrenbsf=n*renbsf.
compute nbathsum=n*bathsum.

compute singlef=0.
if class11=0 and class12=0 singlef=1.
compute nbsfs=nbsf*singlef.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totuntb=totunit*sqrt(bsf).

Compute b=1.
**select if (year1=2014 and puremarket=1).
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
 /statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

select if town=73.

compute mv = (172635.7818	
+ 3773.406366*nbathsum
+ 2528.799741*nbsf89
+ 2330.674376*nbsf778
+ 1251.460872*nsrlsf
+ 1.096390308*nbsfair
+ 2568.041797*nbsf56
- 3343.028121*nsrage
+ 19.56515234*masbsf
- 102051.9385*nnobase
+ 1866.992746*nbsf1095
+ 2274.692529*nbsf234
+ 1454.616121*nbsf1112
+ 1985.219999*nbsf34
- 6623.462805*sb73060
- 2331.547368*sb73032
+ 2270.649332*sb73093
+ 26990.329898*garage1
- 20.20489754*frmsbsf
+ 43662.321899*garage2
+ 3094.569505*sb73044
+ 73595.59834*biggar
+ 1904.645022*nfirepl
- 2239.06288*sb73034
- 283.7630221*sb73150
- 2441.329826*sb73092
+ 1284.859363*sb73012
+ 24513.09574*nsiteben
+ 1613.622109*sb73084
+ 1230.446635*sb73070
+ 1503.429748*sb73120
- 1705.745103*sb73050
+ 3938.351419*sb74030
+ 2148.606831*sb74011
+ 1008.893778*sb73041
+ 2146.086262*sb73062
- 93224.6916*nbasfull
- 95052.4441*nbaspart
+ 1690.227774*sb74012
- 30.8314943*stubsf
+ 1769.594483*sb74013
- 44.02531042*frastbsf
+ 2.490522707*nrepabsf
- 3.028707298*nrepbesf
- 8990.517722*garnogar
+ 2036.359378*sb74022)*1.0.	

  
SAVE OUTFILE='C:\Program Files\IBM\SPSS\Statistics\19\1\mv73.sav'/keep town pin mv.
  
