                                  *SPSS Regression.
				    *Hyde Park and South Multiple Regression 2015.


Get file='C:\Program Files\IBM\SPSS\statistics\19\1\regt70andregt76mergefcl2aug20.sav'.
*select if (amount1>65000).
*select if (amount1<550000).
*select if (multi<1).
*select if sqftb<9000.

Compute year1=0.
If (amount1>0) year1=1900 + yr.
If (yr=14 and amount1>0) year1=2014.
If (yr=13 and amount1>0) year1=2013.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If (yr=9 and amount1>0)  year1=2009.
If (yr=8 and amount1>0)  year1=2008.
If (yr=7 and amount1>0)  year1=2007.   
If (yr=6 and amount1>0)  year1=2006. 
If (yr=5 and amount1>0)  year1=2005.
If (yr=4 and amount1>0)  year1=2004.
If (yr=3 and amount1>0)  year1=2003.   
If (yr=2 and amount1>0)  year1=2002.  
If (yr=1 and amount1>0)  year1=2001.   
If (yr=0 and amount1>0)  year1=2000.
*select if (year1>2009).

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if  (pin=	20021130620000
or pin=	20021160220000
or pin=	20021170270000
or pin=	20023070620000
or pin=	20023090020000
or pin=	20023120360000
or pin=	20023140130000
or pin=	20024040160000
or pin=	20031030410000
or pin=	20032030040000
or pin=	20032030060000
or pin=	20032130130000
or pin=	20032200380000
or pin=	20032220030000
or pin=	20032250390000
or pin=	20033170180000
or pin=	20034020170000
or pin=	20034190280000
or pin=	20101200170000
or pin=	20101210160000
or pin=	20101210240000
or pin=	20102040370000
or pin=	20102130280000
or pin=	20102150240000
or pin=	20102190070000
or pin=	20103070390000
or pin=	20111000240000
or pin=	20151070430000
or pin=	20153000180000
or pin=	20153140130000
or pin=	17212110420000
or pin=	17221070440000
or pin=	20112000230000
or pin=	20113000050000
or pin=	20113060330000
or pin=	20113140290000
or pin=	20113150360000
or pin=	20113170110000
or pin=	20113270080000
or pin=	20113310050000
or pin=	20114160320000
or pin=	20114270390000
or pin=	17281320200000
or pin=	17282090530000
or pin=	17282190290000
or pin=	17283110520000
or pin=	17283130020000
or pin=	17283140210000
or pin=	17283140260000
or pin=	17283180280000
or pin=	17283220120000
or pin=	17283310030000
or pin=	17284000240000
or pin=	17284010350000
or pin=	17284120210000
or pin=	17284350260000
or pin=	17331020330000
or pin=	17331030340000
or pin=	17331030350000
or pin=	17331120060000
or pin=	17331120180000
or pin=	17331180020000
or pin=	17331180140000
or pin=	17331200870000
or pin=	17331210220000
or pin=	17331220470000
or pin=	17332020110000
or pin=	17333000220000
or pin=	17333020030000
or pin=	17333020430000
or pin=	17333030350000
or pin=	17333040050000
or pin=	17333040150000
or pin=	17333080290000
or pin=	17333090510000
or pin=	17333160030000
or pin=	17333160170000
or pin=	17333160390000
or pin=	17333170060000
or pin=	17333260090000
or pin=	20154030210000
or pin=	20221060250000
or pin=	20223210080000
or pin=	20223230290000
or pin=	20224100410000
or pin=	20271020110000
or pin=	20272080300000
or pin=	20272130300000
or pin=	20272160070000
or pin=	20272180150000
or pin=	20274150010000
or pin=	20274210170000
or pin=	20274280270000
or pin=	17341200510000
or pin=	17343080020000
or pin=	17343080090000
or pin=	17343120220000
or pin=	17343120620000
or pin=	17343180360000
or pin=	17343260410000
or pin=	17344260130000
or pin=	17341050310000
or pin=	17342191200000
or pin=	17293050060000
or pin=	17293120060000
or pin=	17293200280000
or pin=	17293210620000
or pin=	17293250310000
or pin=	17293260620000
or pin=	17293280160000
or pin=	17294010110000
or pin=	17294020050000
or pin=	17294030250000
or pin=	17294080030000
or pin=	17294090330000
or pin=	17294090470000
or pin=	17294240640000
or pin=	17294250040000
or pin=	17294250340000
or pin=	17294250790000
or pin=	17294250800000
or pin=	17294260660000
or pin=	17321051180000
or pin=	17321070080000
or pin=	17321070210000
or pin=	17321100320000
or pin=	17322000130000
or pin=	17322010070000
or pin=	17322060160000
or pin=	17322150140000
or pin=	17322160140000
or pin=	17322160760000
or pin=	17322171190000
or pin=	17322171260000
or pin=	17322171360000
or pin=	17322180330000
or pin=	17322220350000
or pin=	17322220400000
or pin=	17322260360000
or pin=	17322270060000
or pin=	17324030180000
or pin=	17324070210000
or pin=	17324120130000
or pin=	16354060130000
or pin=	16354080550000
or pin=	16362010650000
or pin=	16363040240000
or pin=	16363080420000
or pin=	16363190200000
or pin=	16363190370000
or pin=	16363200350000
or pin=	16364010280000
or pin=	16364020210000
or pin=	16364170040000
or pin=	16364200080000
or pin=	16364200190000
or pin=	16364200280000
or pin=	17311040030000
or pin=	17311110090000
or pin=	17311150200000
or pin=	17311150430000
or pin=	17311220100000
or pin=	17311240020000
or pin=	17312130030000
or pin=	17312250180000
or pin=	17312280340000
or pin=	17313000300000
or pin=	17313070030000
or pin=	17313070190000
or pin=	17313160160000
or pin=	17314030300000
or pin=	17314050180000
or pin=	17314050200000
or pin=	17314100110000
or pin=	17314100140000
or pin=	17314110230000
or pin=	17314140310000
or pin=	17314210050000
or pin=	17314210110000
or pin=	17314300140000
or pin=	20144080200000
or pin=	20231010250000
or pin=	20231050570000
or pin=	20231060230000
or pin=	20231060340000
or pin=	20231130420000
or pin=	20231170170000
or pin=	20231180210000
or pin=	20231240170000
or pin=	20231250060000
or pin=	20231250230000
or pin=	20232000160000
or pin=	20232210050000
or pin=	20234020240000
or pin=	20234050210000
or pin=	20234140110000
or pin=	20261030160000
or pin=	20261110050000
or pin=	20261160390000
or pin=	20261170150000
or pin=	20261210020000
or pin=	20261210030000
or pin=	20261210250000
or pin=	20261220190000
or pin=	20262080180000
or pin=	20262100220000
or pin=	20262220130000
or pin=	20262260150000
or pin=	20263220120000
or pin=	20263220360000
or pin=	20264040230000
or pin=	20243160400000
or pin=	20251010130000
or pin=	20251260170000
or pin=	20251280090000
or pin=	20253090370000
or pin=	20253150520000
or pin=	20253220250000
or pin=	20253220260000
or pin=	20253300020000
or pin=	20254030230000
or pin=	20254030300000
or pin=	20254160350000
or pin=	20254170370000
or pin=	20254190090000
or pin=	20351241460000
or pin=	20352030310000
or pin=	20352030350000
or pin=	20354020470000
or pin=	20354160120000
or pin=	20362120030000
or pin=	20362120250000
or pin=	20362210010000
or pin=	20362290140000
or pin=	20363090060000
or pin=	20363290650000
or pin=	20364120370000
or pin=	21301040270000
or pin=	21301200270000
or pin=	21303000230000
or pin=	21303100230000
or pin=	21303160030000
or pin=	21303250380000
or pin=	25022040410000
or pin=	25022110490000
or pin=	20244150020000
or pin=	20244180050000
or pin=	20252020030000
or pin=	20252100120000
or pin=	20252200220000
or pin=	20252220070000
or pin=	20252240040000
or pin=	20252250210000
or pin=	21311010130000
or pin=	21311020090000
or pin=	21311020110000
or pin=	21311090040000
or pin=	21311160490000
or pin=	21311180230000
or pin=	21312140490000
or pin=	21312150010000
or pin=	21312170050000
or pin=	21313070400000
or pin=	21313070410000
or pin=	21313180050000
or pin=	21313280400000
or pin=	21314120210000
or pin=	21314160390000
or pin=	26062210230000
or pin=	26062220150000
or pin=	26062220160000
or pin=	26081020170000
or pin=	26081040370000
or pin=	26081210480000
or pin=	20273030250000
or pin=	20273100300000
or pin=	20341060440000
or pin=	20341130640000
or pin=	20341140270000
or pin=	20341140280000
or pin=	20343110710000
or pin=	20343200110000
or pin=	25031010020000
or pin=	25031030900000
or pin=	25031040850000
or pin=	25031050140000
or pin=	25033270270000
or pin=	25101090040000
or pin=	25101180360000
or pin=	25101210260000
or pin=	25101210270000
or pin=	20342050100000
or pin=	20342110260000
or pin=	20342120230000
or pin=	20342180060000
or pin=	20342190020000
or pin=	20342240140000
or pin=	20342240350000
or pin=	20342250030000
or pin=	20342250190000
or pin=	20344020050000
or pin=	20344090070000
or pin=	20344100120000
or pin=	20344120090000
or pin=	20351050250000
or pin=	20351070170000
or pin=	20351160090000
or pin=	20353010360000
or pin=	20353060460000
or pin=	20353130160000
or pin=	25032070450000
or pin=	25032090480000
or pin=	25032120250000
or pin=	25032160260000
or pin=	25032300400000
or pin=	25021000200000
or pin=	25023060380000
or pin=	25024140170000
or pin=	25011060440000
or pin=	25011150310000
or pin=	25011260580000
or pin=	25011290080000
or pin=	25012180580000
or pin=	25012190480000
or pin=	25012220090000
or pin=	25013070530000
or pin=	25014090550000
or pin=	26061300270000
or pin=	26063150620000
or pin=	26071050350000
or pin=	26062170140000
or pin=	20243040080000
or pin=	20243060280000
or pin=	20243080170000
or pin=	20243090190000
or pin=	20243090200000
or pin=	20243130060000
or pin=	25111230190000
or pin=	25124090200000
or pin=	25124240950000
or pin=	25153160250000
or pin=	25221020110000
or pin=	25223030010000
or pin=	25223060800000
or pin=	25223120100000
or pin=	25104080030000
or pin=	25104140160000
or pin=	25104170160000
or pin=	25152040200000
or pin=	25152070350000
or pin=	25152190030000
or pin=	25154140060000
or pin=	25141020390000
or pin=	25222080010000
or pin=	25222120440000
or pin=	26074020360000
or pin=	26172150200000
or pin=	26173200660000
or pin=	26173270490000
or pin=	26173290670000
or pin=	26174110190000
or pin=	26182020440000
or pin=	26201060890000
or pin=	25271260450000
or pin=	26303100700000
or pin=	26303160220000
or pin=	26303230140000
or pin=	26303310260000
or pin=	26311130070000
or pin=	26314070460000
or pin=	26314130120000
or pin=	26321090020000
or pin=	26321100600000
or pin=	26321120010000
or pin=	26323110100000)    bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute apt=0.
If class=11 or class=12 apt=1.
Compute aptbsf=apt*bsf.
Compute naptbsf=n*aptbsf.

compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
compute nbsf1095=n*cl1095*sqrt(bsf).


compute cl34=0.
if class=34 cl34=1.
compute nbsf34=n*cl34*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

compute cl95=0.
if class=95 cl95=1.                  	
compute nbsf95=n*cl95*sqrt(bsf).

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile12131415.
If (FX >= 6 and town=76)  FX=6.
If (FX >= 8 and town=70)  FX=8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.


****************************************************************************************************************.
* The next section computes a  mid-frequency foreclosure zone .
* and a high frequency foreclosure zone based on foreclosure rates. 
* in specific Hyde Park and South neighborhoods (see Hyde Park and South foreclosure maps).
********************************************************************************************.

Compute lowzonehydepark=0.
if town=70 and (nghcde=20 or nghcde=91 or nghcde=101  or nghcde=150  or nghcde=151
or nghcde=240 or nghcde=241  or nghcde=280) lowzonehydepark=1.                         	

Compute midzonehydepark=0.
if town=70 and (nghcde=80 or nghcde=83  or nghcde=111  or nghcde=120 or  nghcde=130
or nghcde=170 or nghcde=180 or nghcde=220 or nghcde=230 or nghcde=250 or nghcde=260) midzonehydepark=1. 

Compute highzonehydepark=0.
if town=70 and (nghcde=10 or nghcde=30  or nghcde=70  or nghcde=100   or nghcde=121
or nghcde=140 or nghcde=210 ) highzonehydepark=1.


Compute srfxlowblockhydepark=0.
if lowzonehydepark=1 srfxlowblockhydepark=srfx*lowzonehydepark.

Compute srfxmidblockhydepark=0.
if midzonehydepark=1 srfxmidblockhydepark=srfx*midzonehydepark.

Compute srfxhighblockhydepark=0.
if highzonehydepark=1 srfxhighblockhydepark=srfx*highzonehydepark.


Compute lowzonesouth=0.
if town=76 and (nghcde=10 or nghcde=11 or nghcde=12  or nghcde=30  
or nghcde=42) lowzonesouth=1. 

Compute midzonesouth=0.
if town=76 and (nghcde=50  or nghcde=60)  midzonesouth=1.   

Compute highzonesouth=0.
if town=76 and (nghcde=40  or nghcde=41 )  highzonesouth=1.


Compute srfxlowblocksouth=0.
if lowzonesouth=1 srfxlowblocksouth=srfx*lowzonesouth.

Compute srfxmidblocksouth=0.
if midzonesouth=1 srfxmidblocksouth=srfx*midzonesouth.

Compute srfxhighblocksouth=0.
if highzonesouth=1 srfxhighblocksouth=srfx*highzonesouth.




*********************************************************************************************.

Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1. 
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute jantmar10=0.
if (year1=2010 and (mos>=1 and mos<=3)) jantmar10=1. 
Compute octtdec14=0.
if (year1=2014 and (mos>=11 and mos<=13)) octtdec14=1.

Compute jantmar10cl234=jantmar10*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute octtdec14cl234=octtdec14*cl234.

Compute jantmar10cl56=jantmar10*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute octtdec14cl56=octtdec14*cl56.

Compute jantmar10cl778=jantmar10*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute octtdec14cl778=octtdec14*cl778.

Compute jantmar10cl89=jantmar10*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute octtdec14cl89=octtdec14*cl89.

Compute jantmar10cl1112=jantmar10*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute octtdec14cl1112=octtdec14*cl1112.

Compute jantmar10cl1095=jantmar10*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute octtdec14cl1095=octtdec14*cl1095.

Compute jantmar10cl34=jantmar10*cl34.
Compute winter1011cl34=winter1011*cl34.
Compute winter1112cl34=winter1112*cl34.
Compute winter1213cl34=winter1213*cl34.
Compute winter1314cl34=winter1314*cl34.
Compute summer10cl34=summer10*cl34.
Compute summer11cl34=summer11*cl34.
Compute summer12cl34=summer12*cl34.
Compute summer13cl34=summer13*cl34.
Compute summer14cl34=summer14*cl34.
Compute octtdec14cl34=octtdec14*cl34.

compute tnb=(town*1000) + nghcde.

If (tnb=70010) n=2.08. 
If (tnb=70020) n=4.95.
If (tnb=70030) n=1.27.
If (tnb=70070) n=1.80. 
If (tnb=70080) n=1.52. 
If (tnb=70083) n=1.61. 
If (tnb=70091) n=1.51. 
If (tnb=70100) n=1.43. 
If (tnb=70101) n=1.19.
If (tnb=70111) n=1.39. 
If (tnb=70120) n=1.52. 
If (tnb=70121) n=1.34. 
If (tnb=70130) n=1.32. 
If (tnb=70140) n=1.53. 
If (tnb=70150) n=3.62.
If (tnb=70151) n=1.62. 
If (tnb=70170) n=1.12. 
If (tnb=70180) n=1.07. 
If (tnb=70210) n=1.25. 
If (tnb=70220) n=1.42. 
If (tnb=70230) n=1.23. 
If (tnb=70240) n=1.05. 
If (tnb=70241) n=1.93. 
If (tnb=70250) n=1.21. 
If (tnb=70260) n=1.10.
If (tnb=70280) n=1.25.
if (tnb=76010) n=4.65. 
if (tnb=76011) n=3.30.
if (tnb=76012) n=4.60.
if (tnb=76030) n=3.00.
if (tnb=76040) n=2.80.
if (tnb=76041) n=2.23.
if (tnb=76042) n=2.84.
if (tnb=76050) n=2.96.
if (tnb=76060) n=1.84.


compute sb70010=0.
compute sb70020=0.
compute sb70030=0.
compute sb70070=0.
compute sb70080=0.
compute sb70083=0.
compute sb70091=0.
compute sb70100=0.
compute sb70101=0.
compute sb70111=0.
compute sb70120=0.
compute sb70121=0.
compute sb70130=0.
compute sb70140=0.
compute sb70150=0.
compute sb70151=0.
compute sb70170=0.
compute sb70180=0.
compute sb70210=0.
compute sb70220=0.
compute sb70230=0.
compute sb70240=0.
compute sb70241=0.
compute sb70250=0.
compute sb70260=0.
compute sb70280=0.
compute sb70300=0.
compute sb76010=0.
compute sb76011=0.
compute sb76012=0.
compute sb76030=0.
compute sb76040=0.
compute sb76041=0.
compute sb76042=0.
compute sb76050=0.
compute sb76060=0.


if (tnb=70010) sb70010=sqrt(bsf).
if (tnb=70020) sb70020=sqrt(bsf).
if (tnb=70030) sb70030=sqrt(bsf).
if (tnb=70070) sb70070=sqrt(bsf).
if (tnb=70080) sb70080=sqrt(bsf).
if (tnb=70083) sb70083=sqrt(bsf).
if (tnb=70091) sb70091=sqrt(bsf).
if (tnb=70100) sb70100=sqrt(bsf).
if (tnb=70101) sb70101=sqrt(bsf).
if (tnb=70111) sb70111=sqrt(bsf).
if (tnb=70120) sb70120=sqrt(bsf).
if (tnb=70121) sb70121=sqrt(bsf).
if (tnb=70130) sb70130=sqrt(bsf).
if (tnb=70140) sb70140=sqrt(bsf).
if (tnb=70150) sb70150=sqrt(bsf).
if (tnb=70151) sb70151=sqrt(bsf).
if (tnb=70170) sb70170=sqrt(bsf).
if (tnb=70180) sb70180=sqrt(bsf).
if (tnb=70210) sb70210=sqrt(bsf).
if (tnb=70220) sb70220=sqrt(bsf).
if (tnb=70230) sb70230=sqrt(bsf).
if (tnb=70240) sb70240=sqrt(bsf).
if (tnb=70241) sb70241=sqrt(bsf).
if (tnb=70250) sb70250=sqrt(bsf).
if (tnb=70260) sb70260=sqrt(bsf).
if (tnb=70280) sb70280=sqrt(bsf).
if (tnb=76010) sb76010=sqrt(bsf).
if (tnb=76011) sb76011=sqrt(bsf).
if (tnb=76012) sb76012=sqrt(bsf).
if (tnb=76030) sb76030=sqrt(bsf).
if (tnb=76040) sb76040=sqrt(bsf).
if (tnb=76041) sb76041=sqrt(bsf).
if (tnb=76042) sb76042=sqrt(bsf).
if (tnb=76050) sb76050=sqrt(bsf).
if (tnb=76060) sb76060=sqrt(bsf).

compute n70010=0.
compute n70020=0.
compute n70030=0.
compute n70070=0.
compute n70080=0.
compute n70083=0.
compute n70091=0.
compute n70100=0.
compute n70101=0.
compute n70111=0.
compute n70120=0.
compute n70121=0.
compute n70130=0.
compute n70140=0.
compute n70150=0.
compute n70151=0.
compute n70170=0.
compute n70180=0.
compute n70210=0.
compute n70220=0.
compute n70230=0.
compute n70240=0.
compute n70241=0.
compute n70250=0.
compute n70260=0.
compute n70280=0.
compute n76010=0.
compute n76011=0.
compute n76012=0.
compute n76030=0.
compute n76040=0.
compute n76041=0.
compute n76042=0.
compute n76050=0.
compute n76060=0.


if (tnb=70010) n70010=1.0.
if (tnb=70020) n70020=1.0.
if (tnb=70030) n70030=1.0.
if (tnb=70070) n70070=1.0.
if (tnb=70080) n70080=1.0.
if (tnb=70083) n70083=1.0.
if (tnb=70091) n70091=1.0.
if (tnb=70100) n70100=1.0.
if (tnb=70101) n70101=1.0.
if (tnb=70111) n70111=1.0.
if (tnb=70120) n70120=1.0.
if (tnb=70121) n70121=1.0.
if (tnb=70130) n70130=1.0.
if (tnb=70140) n70140=1.0.
if (tnb=70150) n70150=1.0.
if (tnb=70151) n70151=1.0.
if (tnb=70170) n70170=1.0.
if (tnb=70180) n70180=1.0.
if (tnb=70210) n70210=1.0.
if (tnb=70220) n70220=1.0.
if (tnb=70230) n70230=1.0.
if (tnb=70240) n70240=1.0.
if (tnb=70241) n70241=1.0.
if (tnb=70250) n70250=1.0.
if (tnb=70260) n70260=1.0.
if (tnb=70280) n70280=1.0.
if (tnb=76010) n76010=1.0.
if (tnb=76011) n76011=1.0.
if (tnb=76012) n76012=1.0.
if (tnb=76030) n76030=1.0.
if (tnb=76040) n76040=1.0.
if (tnb=76041) n76041=1.0.
if (tnb=76042) n76042=1.0.
if (tnb=76050) n76050=1.0.
if (tnb=76060) n76060=1.0.

               	
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*srbsf.


Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=npremrf*bsf.

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

compute garnogar=0.
if gar=7 garnogar=1.
Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.

Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=0.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

if tnb=70010 and class=95 lsf=1195.
if tnb=70020 and class=95 lsf=1764.
if tnb=70070 and class=95 lsf=1526.
if tnb=70080 and class=95 lsf=1058.
if tnb=70100 and class=95 lsf=2085.
if tnb=70111 and class=95 lsf=2740.
if tnb=70120 and class=95 lsf=1657.
if tnb=70121 and class=95 lsf=1792.
if tnb=70180 and class=95 lsf=3095.
if tnb=70260 and class=95 lsf=2502.
if tnb=76011 and class=95 lsf=1121.
if tnb=76012 and class=95 lsf=1007.
if tnb=76030 and class=95 lsf=1176.
if tnb=76040 and class=95 lsf=875.
if tnb=76041 and class=95 lsf=1987.
if tnb=76042 and class=95 lsf=1710.
if tnb=76051 and class=95 lsf=1152.
if tnb=76060 and class=95 lsf=1059.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.
compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.
compute nbathsum=n*bathsum.

Compute srage=sqrt(age).
compute nage=n*age.
compute nsrage=n*srage.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
compute nbnum=n*bnum.
compute bnumb=bnum*bsf.



Compute b=1.
*select if (year1=2014 and puremarket=1).
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
 /statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

select if town=76.

compute  reg = (- 63600.70447	
+ 4029.415062*nbathsum
+ 0.824385806*nbsfair
+ 2121.171643*sb76030
+ 4771.056075*sb76011
+ 313.4138922*nsrlsf
+ 6156.592559*nbsf778
+ 5051.750108*nbsf1095
- 46.53512068*frabsf
+ 1394.199545*sb70010
- 11745.58626*midzonesouth
- 11.1707392*nrepbesf
- 1492.975972*srfxhighblocksouth
+ 1551.107822*sb70020
- 2986.927191*nsrage
+ 51053.90267*nbasfull
+ 4811.459236*nbsf56
+ 1777.159001*sb76042
+ 4265.305833*sb76012
+ 1483.050141*sb76050
+ 1530.64711*sb76060
+ 14977.70837*highzonehydepark
+ 46373.34767*nbaspart
+ 41957.70425*nnobase
+ 4627.697453*nfirepl
- 28.76002992*masbsf
- 7.065144606*stubsf
+ 3795.472549*srfxmidblockhydepark
+ 5918.559121*nbsf34
- 6977.790889*srfxlowblocksouth
+ 936.8504336*sb70140
- 2090.411645*garage1
+ 4522.420237*garage2
- 28511.478*highzonesouth
- 1013.460997*srfxmidblocksouth
- 2085.764286*midzonehydepark
+ 141.9285294*srfxlowblockhydepark
+ 1520.14305*srfxhighblockhydepark
+ 4566.895279*nbsf234
+ 8963.502532*nbsf89
+ 4284.376945*nbsf1112
+ 17798.70953*biggar
- 7025.112947*garnogar
- 31.32559193*frmsbsf
- 131.5234528*sb76040
+ 472.6985386*sb76041)*1.0.




 
SAVE OUTFILE='C:\Program Files\IBM\SPSS\Statistics\19\1\mv76.sav'/keep town pin mv.	