                     *SPSS Regression.
                     *Niles Regression 2010.

get file = 'C:\Program Files\SPSS\spssa\regt24mergefcl2.sav'.
*select if (amount1>100000).
*select if (amount1<1500000).
*select if (multi<1).
*select if sqftb<7000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  
If  (yr=3 and amount1>0)  year1=2003.
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.


*select if (year1>2004).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
*if (pin=	10072000480000
or pin=	10182060420000
or pin=	10071050130000
or pin=	10071060100000
or pin=	10071070060000
or pin=	10153010420000
or pin=	10153010450000
or pin=	10153080510000
or pin=	10163020430000
or pin=	10163180240000
or pin=	10163270410000
or pin=	10164140470000
or pin=	10164210060000
or pin=	10164210090000
or pin=	10164210220000
or pin=	10164220120000
or pin=	10164290180000
or pin=	10172140440000
or pin=	10174080670000
or pin=	10181110380000
or pin=	10181110390000
or pin=	10181110410000
or pin=	10181170240000
or pin=	10182050240000
or pin=	10202200090000
or pin=	10204260120000
or pin=	10213240200000
or pin=	10213320560000
or pin=	10223190370000
or pin=	10223280380000
or pin=	10282020070000
or pin=	10282080130000
or pin=	10282080150000
or pin=	10282140080000
or pin=	10282180050000
or pin=	10201130310000
or pin=	10201130470000
or pin=	10144010310000
or pin=	10144050570000
or pin=	10144210410000
or pin=	10151110390000
or pin=	10152030230000
or pin=	10152090270000
or pin=	10153050270000
or pin=	10153120060000
or pin=	10221020070000
or pin=	10141270210000
or pin=	10142000780000
or pin=	10193110580000
or pin=	10222010380000
or pin=	10224000540000
or pin=	10231030080000
or pin=	10231030090000
or pin=	10231080540000
or pin=	10231110280000
or pin=	10232240440000
or pin=	10272050440000
or pin=	10272110530000
or pin=	10272230200000
or pin=	10273060560000
or pin=	10274060270000
or pin=	10303020370000
or pin=	10273230010000
or pin=	10274180230000
or pin=	10332290340000
or pin=	10332300380000
or pin=	10342050240000
or pin=	10342140070000
or pin=	10353220030000
or pin=	10354140220000
or pin=	10354220570000
or pin=	10334230350000
or pin=	10334270370000
or pin=	10343100540000
or pin=	10143180270000
or pin=	10143180290000) bs=1.
*select if bs=0.

compute tnb=(town*1000) + nghcde.
compute bsf=sqftb.
compute lsf=sqftl.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute n24013=0.
compute n24021=0.
compute n24022=0.
compute n24023=0.
compute n24031=0.
compute n24032=0.
compute n24051=0.
compute n24052=0.
compute n24060=0.
compute n24081=0.
compute n24101=0.
compute n24102=0.
compute n24103=0.
compute n24140=0.
compute n24150=0.
compute n24160=0.


compute sb24013=0.
compute sb24021=0.
compute sb24022=0.
compute sb24023=0.
compute sb24031=0.
compute sb24032=0.
compute sb24051=0.
compute sb24052=0.
compute sb24060=0.
compute sb24081=0.
compute sb24101=0.
compute sb24102=0.
compute sb24103=0.
compute sb24140=0.
compute sb24150=0.
compute sb24160=0.


if (tnb=24013) n=2.65.
if (tnb=24021) n=9.05.
if (tnb=24022) n=4.25.
if (tnb=24023) n=6.90.
if (tnb=24031) n=2.59.
if (tnb=24032) n=2.75.
if (tnb=24051) n=3.48.
if (tnb=24052) n=3.21.
if (tnb=24060) n=4.46.
if (tnb=24081) n=2.65.
if (tnb=24101) n=3.40.
if (tnb=24102) n=6.90.
if (tnb=24103) n=4.13.
if (tnb=24140) n=3.05.
if (tnb=24150) n=3.67.
if (tnb=24160) n=6.70.


if (tnb=24013) n24013=1.
if (tnb=24021) n24021=1.
if (tnb=24022) n24022=1.
if (tnb=24023) n24023=1.
if (tnb=24031) n24031=1.
if (tnb=24032) n24032=1.
if (tnb=24051) n24051=1.
if (tnb=24052) n24052=1.
if (tnb=24060) n24060=1.
if (tnb=24081) n24081=1.
if (tnb=24101) n24101=1.
if (tnb=24102) n24102=1.
if (tnb=24103) n24103=1.
if (tnb=24140) n24140=1.
if (tnb=24150) n24150=1.
if (tnb=24160) n24160=1.

if tnb=24013  sb24013=sqrt(bsf).
if tnb=24021  sb24021=sqrt(bsf).
if tnb=24022  sb24022=sqrt(bsf).
if tnb=24023  sb24023=sqrt(bsf).
if tnb=24031  sb24031=sqrt(bsf).
if tnb=24032  sb24032=sqrt(bsf).
if tnb=24051  sb24051=sqrt(bsf).
if tnb=24052  sb24052=sqrt(bsf).
if tnb=24060  sb24060=sqrt(bsf).
if tnb=24081  sb24081=sqrt(bsf).
if tnb=24101  sb24101=sqrt(bsf).
if tnb=24102  sb24102=sqrt(bsf).
if tnb=24103  sb24103=sqrt(bsf).
if tnb=24140  sb24140=sqrt(bsf).
if tnb=24150  sb24150=sqrt(bsf).
if tnb=24160  sb24160=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute lowzoneniles=0.
if town=24 and  (nghcde=13 or nghcde=21  or nghcde=22 or nghcde=23 
or nghcde=60 or nghcde=150 or nghcde=160) lowzoneniles=1.                         	

Compute midzoneniles=0.
if town=24 and (nghcde=31 or nghcde=32 or nghcde=51 
or nghcde=81 or nghcde=102)  midzoneniles=1.

Compute highzoneniles=0.
if town=24 and (nghcde=52 or nghcde=101 or nghcde=103 
or nghcde=140)    highzoneniles=1.


Compute srfxlowblockniles=0.
if lowzoneniles=1 srfxlowblockniles=srfx*lowzoneniles.

Compute srfxmidblockniles=0.
if midzoneniles=1 srfxmidblockniles=srfx*midzoneniles.

Compute srfxhighblockniles=0.
if highzoneniles=1 srfxhighblockniles=srfx*highzoneniles.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.



Compute lsf=sqftl.

if class=95 and tnb=24031   lsf=1160.
if class=95 and tnb=24032   lsf=3100.
if class=95 and tnb=24051   lsf=1852.
if class=95 and tnb=24052   lsf=1018.
if class=95 and tnb=24081   lsf=2540.
if class=95 and tnb=24101   lsf=2156.
if class=95 and tnb=24103   lsf=3024.	
if class=95 and tnb=24140   lsf=2056.	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

Compute firepl=0.
If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute mv= (290043.1108	
+ 949.1289519*nbathsum
+ 980.424437*nbsf1095
+ 6.811901067*masbsf
+ 1961.108526*nbsf778
+ 596.1590325*nsrlsf
- 8733.609486*nsrage
+ 1452.476434*sb24101
+ 2784.492718*nbsf56
- 8574.672817*sb24021
+ 15223.63534*garage2
- 4350.856787*sb24023
- 3894.447937*sb24022
+ 1697.444565*sb24032
+ 1032.368283*sb24103
- 7416.33655*sb24160
- 25777.38356*nnobase
+ 1.0785436*nbsfair
+ 3.577498785*frabsf
+ 2060.914028*nbsf234
+ 2465.242657*nbsf34
+ 1227.231637*nbsf1112
+ 1140.874691*nbsf89
+ 4.784452*frmsbsf
- 2218.499219*sb24102
+ 446.3505381*sb24031
+ 247.1982287*sb24081
+ 15054.71821*nsiteben
- 30626.56554*midzoneniles
- 50743.5517*highzoneniles
- 27001.68089*srfxlowblockniles
- 3649.913659*srfxmidblockniles
- 11514.59888*srfxhighblockniles
- 15109.95508*nbasfull
- 18453.29396*nbaspart
+ 7663.001775*garage1
+ 26695.2287*biggar)*.6848179.

  

save outfile='C:\Program Files\SPSS\spssa\mv24.sav'/keep town pin mv.
  
