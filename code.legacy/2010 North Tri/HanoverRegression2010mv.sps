                        *SPSS Regression.
	                   *HANOVER  2010.

Get file='C:\Program Files\SPSS\spssa\regt18mergefcl2.sav'.
*select if (amount1>90000).
*select if (amount1<750000).
*select if (multi<1).
*select if sqftb<7000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0) year1=2009. 
If  (yr=8 and amount1>0) year1=2008.
If  (yr=7 and amount1>0) year1=2007.
If  (yr=6 and amount1>0) year1=2006. 
If  (yr=5 and amount1>0) year1=2005.
If  (yr=4 and amount1>0) year1=2004.
If  (yr=3 and amount1>0) year1=2003.  
If  (yr=2 and amount1>0) year1=2002.  
If  (yr=1 and amount1>0) year1=2001.   
If  (yr=0 and amount1>0) year1=2000.

*select if (year1>2004).
set mxcells=2000500.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	6143040120000
or pin=	6061120180000
or pin=	6061140420000
or pin=	6061200280000
or pin=	6071190030000
or pin=	6071210020000
or pin=	6182090230000
or pin=	6182120110000
or pin=	6182140110000
or pin=	6182140190000
or pin=	6192020080000
or pin=	6192030200000
or pin=	6212040330000
or pin=	6224180040000
or pin=	6273000220000
or pin=	6134080350000
or pin=	6231100200000
or pin=	6231110120000
or pin=	6232040010000
or pin=	6232110390000
or pin=	6233040140000
or pin=	6233060270000
or pin=	6233070180000
or pin=	6233080010000
or pin=	6261020210000
or pin=	6262080150000
or pin=	6262200080000
or pin=	6304040120000
or pin=	6312030140000
or pin=	6312080300000
or pin=	6091090120000
or pin=	6183010310000
or pin=	6193210060000
or pin=	6194020430000
or pin=	6344070050000
or pin=	6361230260000
or pin=	6361230310000
or pin=	6361230460000
or pin=	6362050010000
or pin=	6363000100000
or pin=	6363100190000
or pin=	6363120170000
or pin=	6343070060000
or pin=	6343070090000
or pin=	6343070260000
or pin=	6343070280000
or pin=	6343080330000
or pin=	6343080360000
or pin=	6343080440000
or pin=	6344160150000
or pin=	6082030030000
or pin=	6082030050000
or pin=	6084040290000) bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1 or (yr=5).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.



compute tnb=(town*1000) + nghcde.

if (tnb= 18010 ) n=1.82.
if (tnb= 18011 ) n=1.78.
if (tnb= 18012 ) n=2.80.
if (tnb= 18013 ) n=2.31.
if (tnb= 18014 ) n=2.10.
if (tnb= 18015 ) n=2.95.
if (tnb= 18018 ) n=4.25.
if (tnb= 18019 ) n=3.69.
if (tnb= 18020 ) n=1.63.
if (tnb= 18030 ) n=4.12.
if (tnb= 18035 ) n=4.33.
if (tnb= 18040 ) n=1.44.
if (tnb= 18050 ) n=2.12.
if (tnb= 18060 ) n=1.69. 
if (tnb= 18070 ) n=1.65.
if (tnb= 18075 ) n=2.08.
if (tnb= 18080 ) n=1.98.
if (tnb= 18082 ) n=3.68.
if (tnb= 18090 ) n=3.64.


compute sb18010=0.
compute sb18011=0.
compute sb18012=0.
compute sb18013=0.
compute sb18014=0.
compute sb18015=0.
compute sb18018=0.
compute sb18019=0.
compute sb18020=0.
compute sb18030=0.
compute sb18035=0.
compute sb18040=0.
compute sb18050=0.
compute sb18060=0.
compute sb18070=0.
compute sb18075=0. 
compute sb18080=0.
compute sb18082=0.
compute sb18090=0.

compute n18010=0.
compute n18011=0.
compute n18012=0.
compute n18013=0.
compute n18014=0.
compute n18015=0.
compute n18018=0.
compute n18019=0.
compute n18020=0.
compute n18030=0.
compute n18035=0.
compute n18040=0.
compute n18050=0.
compute n18060=0. 
compute n18070=0.
compute n18075=0.
compute n18080=0.
compute n18082=0.
compute n18090=0.

if (tnb= 18010 ) n18010=1.
if (tnb= 18011 ) n18011=1.
if (tnb= 18012 ) n18012=1.
if (tnb= 18013 ) n18013=1.
if (tnb= 18014 ) n18014=1.
if (tnb= 18015 ) n18015=1.
if (tnb= 18018 ) n18018=1.
if (tnb= 18019 ) n18019=1.
if (tnb= 18020 ) n18020=1.
if (tnb= 18030 ) n18030=1.
if (tnb= 18035 ) n18035=1.
if (tnb= 18040 ) n18040=1.
if (tnb= 18050 ) n18050=1.
if (tnb= 18060 ) n18060=1.
if (tnb= 18070 ) n18070=1.
if (tnb= 18075 ) n18075=1.
if (tnb= 18080 ) n18080=1.
if (tnb= 18082 ) n18082=1.
if (tnb= 18090)  n18090=1.

if (tnb= 18010 ) sb18010=sqrt(bsf).
if (tnb= 18011 ) sb18011=sqrt(bsf).
if (tnb= 18012 ) sb18012=sqrt(bsf).
if (tnb= 18013 ) sb18013=sqrt(bsf).
if (tnb= 18014 ) sb18014=sqrt(bsf).
if (tnb= 18015 ) sb18015=sqrt(bsf).
if (tnb= 18018 ) sb18018=sqrt(bsf).
if (tnb= 18019 ) sb18019=sqrt(bsf).
if (tnb= 18020 ) sb18020=sqrt(bsf).
if (tnb= 18030 ) sb18030=sqrt(bsf).
if (tnb= 18035 ) sb18035=sqrt(bsf).
if (tnb= 18040 ) sb18040=sqrt(bsf).
if (tnb= 18050 ) sb18050=sqrt(bsf).
if (tnb= 18060 ) sb18060=sqrt(bsf).
if (tnb= 18070 ) sb18070=sqrt(bsf).
if (tnb= 18075 ) sb18075=sqrt(bsf).
if (tnb= 18080 ) sb18080=sqrt(bsf).
if (tnb= 18082 ) sb18082=sqrt(bsf).
if (tnb= 18090)  sb18090=sqrt(bsf).

Compute lowzonehanover=0.
if town=18 and  (nghcde=12 or nghcde=15  or nghcde=18  or nghcde=19 
or nghcde=35 or nghcde=50 or nghcde=82 or nghcde=90)  lowzonehanover=1.                         	

Compute midzonehanover=0.
if town=18 and (nghcde=10 or nghcde=13 or nghcde=14 
or nghcde=30 or nghcde=75 or nghcde=80)   midzonehanover=1. 

Compute highzonehanover=0.
if town=18 and (nghcde=11 or nghcde=20 or nghcde=40 
or nghcde=60 or nghcde=70)   highzonehanover=1.


Compute srfxlowblockhanover=0.
if lowzonehanover=1 srfxlowblockhanover=srfx*lowzonehanover.

Compute srfxmidblockhanover=0.
if midzonehanover=1 srfxmidblockhanover=srfx*midzonehanover.

Compute srfxhighblockhanover=0.
if highzonehanover=1 srfxhighblockhanover=srfx*highzonehanover.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.



Compute lsf=sqftl.


if class=95 and tnb=18010   lsf=6300.
if class=95 and tnb=18011   lsf=7722.
if class=95 and tnb=18012   lsf=7668.
if class=95 and tnb=18013   lsf=7800.
if class=95 and tnb=18014   lsf=8838.
if class=95 and tnb=18015   lsf=8325.
if class=95 and tnb=18019   lsf=10580.	
if class=95 and tnb=18060   lsf=1320.
if class=95 and tnb=18070   lsf=7920.
if class=95 and tnb=18090   lsf=10781.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.





Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.

*select if (year1=2009).
*select if puremarket=1.
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

compute reg = (134031.2452	
+ 1443.682999*nbsf1095
- 2471.635983*sb18030
+ 1245.676741*nbathsum
+ 52545.77702*biggar
- 4997.999377*nsrage
+ 184.3418507*nsrlsf
+ 2498.742627*nbsf1112
- 3793.7806*SRFX
- 44384.30469*nnobase
+ 2713.701415*nbsf89
+ 1496.713581*sb18013
+ 24816.85243*garage2
- 572.0572954*sb18011
+ 1646.585786*sb18050
+ 511.0255578*sb18082
- 555.5997259*sb18040
+ 689.0623636*sb18014
- 6217.185016*srfxlowblockhanover
+ 688.2032987*sb18012
+ 4.161395504*masbsf
- 103.0543243*sb18019
- 793.1971509*sb18035
+ 4019.280491*nbsf56
- 339.1002275*sb18090
+ 1616.223214*sb18010
+ 43533.29009*highzonehanover
+ 1110.980638*sb18075
+ 1639.856915*sb18080
+ 16499.55037*garage1
+ 2145.006555*nbsf234
+ 2031.575281*nfirepl
+ 1998.064391*nbsf778
+ 1592.76864*nbsf34
- 36516.5631*nbaspart
- 34683.30126*nbasfull
+ 260.9554368*sb18060
- 16935.71686*midzonehanover
+ 1217.371156*srfxmidblockhanover)*.8836264.


* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=18.
*sel if puremarket=1 and year1=2009.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\SPSS\spssa\mv18.sav'/keep town pin mv.


