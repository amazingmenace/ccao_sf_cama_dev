			*NORWOOD PARK AND LEYDEN REGRESSION 2010.

GET FILE='C:\PROGRAM FILES\SPSS\SPSSA\regt20mergefcl3.SAV'.
*select if (amount1>85000).
*select if (amount1<550000).
set mxcells=100000.
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.   
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  
If  (yr=3 and amount1>0)  year1=2003. 
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.

*select if (year1>2004).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
*select if bs=0.

compute t26=0.
if town=26 t26=1.
compute t20=0.                                                                             
if town=20 t20=1.

compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 , 2008 and 2009 block filings.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1 .

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000)+nghcde.

compute N20010=0.
compute N20011=0.
compute N20020=0.
compute N20030=0.
compute N20040=0.
compute N20050=0.
compute N20060=0.
compute N20070=0.
compute N20080=0.
compute N20081=0.
compute N20090=0.
compute N20091=0.
IF TNB=20010 N20010=1.
IF TNB=20011 N20011=1.
IF TNB=20020 N20020=1.
IF TNB=20030 N20030=1.
IF TNB=20040 N20040=1.
IF TNB=20050 N20050=1.
IF TNB=20060 N20060=1.
IF TNB=20070 N20070=1.
IF TNB=20080 N20080=1.
IF TNB=20081 N20081=1.
IF TNB=20090 N20090=1.
IF TNB=20091 N20091=1.

compute SB20010=0.
compute SB20011=0.
compute SB20020=0.
compute SB20030=0.
compute SB20040=0.
compute SB20050=0.
compute SB20060=0.
compute SB20070=0.
compute SB20080=0.
compute SB20081=0.
compute SB20090=0.
compute SB20091=0.

IF TNB=20010 SB20010=SQRT(BSF).
IF TNB=20011 SB20011=SQRT(BSF).
IF TNB=20020 SB20020=SQRT(BSF).
IF TNB=20030 SB20030=SQRT(BSF).
IF TNB=20040 SB20040=SQRT(BSF).
IF TNB=20050 SB20050=SQRT(BSF).
IF TNB=20060 SB20060=SQRT(BSF).
IF TNB=20070 SB20070=SQRT(BSF).
IF TNB=20080 SB20080=SQRT(BSF).
IF TNB=20081 SB20081=SQRT(BSF).
IF TNB=20090 SB20090=SQRT(BSF).
IF TNB=20091 SB20091=SQRT(BSF).

compute n26010=0.
compute n26030=0.
compute n26040=0.
compute n26050=0.

IF TNB=26010 N26010=1.
IF TNB=26030 N26030=1.
IF TNB=26040 N26040=1.
IF TNB=26050 N26050=1.

compute SB26010=0.
compute SB26030=0.
compute SB26040=0.
compute SB26050=0.

IF TNB=26010 SB26010=SQRT(BSF).
IF TNB=26030 SB26030=SQRT(BSF).
IF TNB=26040 SB26040=SQRT(BSF).
IF TNB=26050 SB26050=SQRT(BSF).
  
compute n=1.
if tnb=20010 n=4.25.
if tnb=20011 n=3.18.
If tnb=20020 n=2.55.
if tnb=20030 n=2.32.
If tnb=20040 n=4.05.
if tnb=20050 n=2.30.
If tnb=20060 n=2.32.
If tnb=20070 n=3.45.
If tnb=20080 n=2.86.
if tnb=20081 n=2.60.
If tnb=20090 n=2.75.
If tnb=20091 n=3.35.
If tnb=26010 n=3.19.
If tnb=26030 n=3.50.
if tnb=26040 n=4.12.
If tnb=26050 n=3.32.


*Compute lowzonenorwood=0.
*if town=26 and  (nghcde= or nghcde=  or nghcde=   or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde=)  lowzonenorwood=1.                         	

*Compute midzonenorwood=0.
*if town=26 and (nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde=)   midzonenorwood=1. 

*Compute highzonenorwood=0.
*if town=26 and (nghcde= or nghcde= or nghcde= or nghcde= or nghcde= 
or nghcde= or nghcde= or nghcde= or nghcde= or nghcde=
or nghcde= or nghcde=)   highzonenorwood=1.


*Compute srfxlowblocknorwood=0.
*if lowzonenorwood=1 srfxlowblocknorwood=srfx*lowzonenorwood.

*Compute srfxmidblocknorwood=0.
*if midzonenorwood=1 srfxmidblocknorwood=srfx*midzonenorwood.

*Compute srfxhighblocknorwood=0.
*if highzonenorwood=1 srfxhighblocknorwood=srfx*highzonenorwood.

Compute lowzoneleyden=0.
if town=20 and  (nghcde=10 or nghcde=11 or nghcde=40  or nghcde=70)  lowzoneleyden=1.                         	

Compute midzoneleyden=0.
if town=20 and (nghcde=20 or nghcde=30 or nghcde=80 or nghcde=81 or nghcde=91) midzoneleyden=1. 

Compute highzoneleyden=0.
if town=20 and (nghcde=50 or nghcde=60 or nghcde=90) highzoneleyden=1.

Compute srfxlowblockleyden=0.
if lowzoneleyden=1 srfxlowblockleyden=srfx*lowzoneleyden.

Compute srfxmidblockleyden=0.
if midzoneleyden=1 srfxmidblockleyden=srfx*midzoneleyden.

Compute srfxhighblockleyden=0.
if highzoneleyden=1 srfxhighblockleyden=srfx*highzoneleyden.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.

Compute lsf=sqftl.
if lsf > 6750 lsf = 6750  + ((lsf - 6750)/4).

if class=95 and tnb=26010 	lsf=1063.
if class=95 and tnb=20020 	lsf=1875.
if class=95 and tnb=20040 	lsf=2712.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
compute frms=0.
If extcon=1 or extcon=2 frms=1.
compute frmsbsf=frms*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.


Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=26 and year1>=2009) or (town=20 and year1>=2009).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').


compute mv=(159640.3931	
+ 4695.185486*nbathsum
+ 648.8349869*nsrlsf
- 45593.4862*highzoneleyden
+ 651.3492824*SB26010
+ 1566.394098*SB20080
- 4321.198065*nsrage
+ 11632.31198*nbasfull
- 2847.473926*SB20010
- 1879.553569*SB20091
+ 1079.571478*nbsf1112
+ 4734.835588*nbaspart
- 1030.840067*SB20040
+ 1314.371109*nbsf34
+ 1.209651106*nbsfair
+ 3682.615743*garage1
+ 1228.032563*nbsf778
+ 1273.102778*nbsf234
+ 1305.219553*nbsf56
+ 1075.93582*nbsf1095
- 9.349378542*frabsf
+ 167.5548911*totunitb
+ 780.5841726*SB20050
- 451.582799*SB26050
+ 1053.171018*SB20081
+ 1029.723719*SB20020
+ 5.132250611*nprembsf
+ 14257.11734*biggar
+ 1765.092592*nfirepl
+ 6864.844596*nsiteben
+ 7868.306773*garage2
- 6.801773208*stubsf
- 1.233464929*frmsbsf
- 1310.861904*FX
+ 854.3325672*SB20030
+ 628.8482304*SB20060
- 10476.60978*lowzoneleyden
- 35396.0757*midzoneleyden
- 7597.280069*srfxhighblockleyden
- 4536.677559*srfxmidblockleyden
- 3140.74039*srfxlowblockleyden
+ 644.4481926*SB20090)*.683937.

* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=20.
*sel if puremarket=1 and year1=2009.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

*compute newav=mv*.10.
*compute landpart=mv*.2.
*compute bldgpart=mv*.8.
*compute landpp=landpart/sqftl.


save outfile='C:\Program Files\SPSS\spssa\mv20oct26.sav'/keep town pin mv.
exe.










