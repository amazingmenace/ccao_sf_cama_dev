                        *SPSS Regression.
			*BERWYN + CALUMET + CICERO + STICKNEY  2014.


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regts11141536mergefcl2.sav'.
set wid=125.
set len=59.
*select if (amount1>130000).
*select if (amount1<850000).
*select if (multi<1).
*select if sqftb<6000.

COMPUTE YEAR1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=13 and amount1>0)  year1=2013.
If  (yr=12 and amount1>0)  year1=2012.
If  (yr=11 and amount1>0)  year1=2011. 
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.



COMPUTE FX = cumfile11121314.
If FX > 8 FX=8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1>2008).
*select if puremarket=1.


compute bsf=sqftb.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	16191030020000
or pin=	16193010070000
or pin=	16193070320000
or pin=	16193130040000
or pin=	16194070470000
or pin=	16201020140000
or pin=	16201180010000
or pin=	16203000210000
or pin=	16203210270000
or pin=	16302060340000
or pin=	16303000120000
or pin=	16303210330000
or pin=	16303260200000
or pin=	16311070230000
or pin=	16311300170000
or pin=	16312030320000
or pin=	16312160180000
or pin=	16312280430000
or pin=	16313070140000
or pin=	16313180040000
or pin=	16314010210000
or pin=	16314100870000
or pin=	16323180240000
or pin=	25293110530000
or pin=	25301110290000
or pin=	25301230060000
or pin=	25304070520000
or pin=	25304190310000
or pin=	16214170060000
or pin=	16282190250000
or pin=	16282270150000
or pin=	16332000010000
or pin=	19061080410000
or pin=	19091160280000
or pin=	19283160410000
or pin=	19313090160000
or pin=	19321240080000
or pin=	19322310190000
or pin=	19323050120000
or pin=	19331090290000
or pin=	19334011130000)  bs=1.
*select if bs=0.


Compute N=1.
compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


	
Compute midzoneberwyn=0.
if town=11 and (nghcde=40 or nghcde=60) midzoneberwyn=1. 
Compute highzoneberwyn=0.
if town=11 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=50 or nghcde=70)   highzoneberwyn=1.

Compute srfxmidblockberwyn=0.
if midzoneberwyn=1 srfxmidblockberwyn=srfx*midzoneberwyn.
Compute srfxhighblockberwyn=0.
if highzoneberwyn=1 srfxhighblockberwyn=srfx*highzoneberwyn.

Compute highzonecalumet=0.
if town=14 and (nghcde=10 or nghcde=20 or nghcde=30 
or nghcde=40)   highzonecalumet=1.

Compute srfxhighblockcalumet=0.
if highzonecalumet=1 srfxhighblockcalumet=srfx*highzonecalumet.

Compute highzonecicero=0.
if town=15 and (nghcde=15 or nghcde=20 or nghcde=25  
or nghcde=30 or nghcde=40 or nghcde=65 or nghcde=71 or nghcde=72
or nghcde=80 or nghcde=85 or nghcde=90 )   highzonecicero=1.

Compute srfxhighblockcicero=0.
if highzonecicero=1 srfxhighblockcicero=srfx*highzonecicero.

	
Compute midzonestickney=0.
if town=36 and (nghcde=36 or nghcde=41 or nghcde=50 
or nghcde=60)  midzonestickney=1. 
Compute highzonestickney=0.
if town=36 and (nghcde=12 or nghcde=21 or nghcde=22)  highzonestickney=1.

Compute srfxmidblockstickney=0.
if midzonestickney=1 srfxmidblockstickney=srfx*midzonestickney.
Compute srfxhighblockstickney=0.
if highzonestickney=1 srfxhighblockstickney=srfx*highzonestickney.

*Neighborhood change in Berwyn - 
 lower part of nbhd 40 (south of 35th st
 to Ogden Ave on the south and Harlem on the west)
 becomes part of nbhd 60 .

Compute bern10=0.
If town=11 and nghcde=10 bern10=1.
Compute bern20=0.
If town=11 and nghcde=20 bern20=1.
Compute bern30=0.
If town=11 and nghcde=30 bern30=1.
Compute bern40=0.
If town=11 and nghcde=40 bern40=1.
Compute bern50=0.
If town=11 and nghcde=50 bern50=1.
Compute bern60=0.
If town=11 and nghcde=60 bern60=1.
Compute bern70=0.
If town=11 and nghcde=70 bern70=1.
Compute caln10=0.
If town=14 and nghcde=10 caln10=1.
Compute caln20=0.
If town=14 and nghcde=20 caln20=1.
Compute caln30=0.
If town=14 and nghcde=30 caln30=1.
Compute caln40=0.
If town=14 and nghcde=40 caln40=1.
Compute cicn10=0.
If town=15 and nghcde=10 cicn10=1.
Compute cicn15=0.
If town=15 and nghcde=15 cicn15=1.
Compute cicn20=0.
If town=15 and nghcde=20 cicn20=1.
Compute cicn25=0.
If town=15 and nghcde=25 cicn25=1.
Compute cicn30=0.
If town=15 and nghcde=30 cicn30=1.
Compute cicn40=0.
If town=15 and nghcde=40 cicn40=1.
Compute cicn50=0.
If town=15 and nghcde=50 cicn50=1.
Compute cicn60=0.
If town=15 and nghcde=60 cicn60=1.
Compute cicn65=0.
If town=15 and nghcde=65 cicn65=1.
Compute cicn71=0.
If town=15 and nghcde=71 cicn71=1.
Compute cicn72=0.
If town=15 and nghcde=72 cicn72=1.
Compute cicn80=0.
If town=15 and nghcde=80 cicn80=1.
Compute cicn85=0.
If town=15 and nghcde=85 cicn85=1.
Compute cicn90=0.
If town=15 and nghcde=90 cicn90=1.
Compute stin12=0.
If town=36 and nghcde=12 stin12=1.
Compute stin21=0.
If town=36 and nghcde=21 stin21=1.
Compute stin22=0.
If town=36 and nghcde=22 stin22=1.
Compute stin36=0.
If town=36 and nghcde=36 stin36=1.
Compute stin41=0.
If town=36 and nghcde=41 stin41=1.
Compute stin50=0.
If town=36 and nghcde=50 stin50=1.
Compute stin60=0.
If town=36 and nghcde=60 stin60=1.

If town=11 and nghcde=10 N=1.71.
If town=11 and nghcde=20 N=1.71.
If town=11 and nghcde=30 N=1.60.
If town=11 and nghcde=40 N=1.80.
If town=11 and nghcde=50 N=1.69.
If town=11 and nghcde=60 N=2.40.
If town=11 and nghcde=70 N=1.58.
If town=14 and nghcde=10 N=1.35.
If town=14 and nghcde=20 N=1.34.
If town=14 and nghcde=30 N=1.28.
If town=14 and nghcde=40 N=1.44.
If town=15 and nghcde=15 N=1.39.
If town=15 and nghcde=20 N=1.36.		
If town=15 and nghcde=25 N=1.56.
If town=15 and nghcde=30 N=1.45.
If town=15 and nghcde=40 N=1.75.
If town=15 and nghcde=65 N=1.47.
If town=15 and nghcde=71 N=1.60.
If town=15 and nghcde=72 N=1.42.
If town=15 and nghcde=80 N=1.53.
If town=15 and nghcde=85 N=1.39.
If town=15 and nghcde=90 N=1.56.
If town=36 and nghcde=12 N=1.51.
If town=36 and nghcde=21 N=1.85.
If town=36 and nghcde=22 N=1.65.
If town=36 and nghcde=36 N=1.85.
If town=36 and nghcde=41 N=1.74.
If town=36 and nghcde=50 N=1.70.
If town=36 and nghcde=60 N=1.52.

Compute T11=0.
If town=11 T11=1.
Compute T14=0.
If town=14 T14=1.
Compute T15=0.
If town=15 T15=1.
Compute T36=0.
If town=36 T36=1.

*Cutpoints for land square foot are 1.75 * median lsf.
Compute lsf=sqftl.
If town=11 and lsf > 7140 lsf= 7140 + ((lsf - 7140)/3).
If town=14 and lsf > 8202 lsf= 8202 + ((lsf - 8202)/3).
If town=15 and lsf > 6615 lsf= 6615 + ((lsf - 6615)/3).
If town=36 and lsf > 11637 lsf= 11637 + ((lsf - 11637)/3).
If (class=8 or class=9) lsf=sqftl.


Compute sbebsf=0.
If T11=1 sbebsf=T11*sqrt(bsf).
Compute scabsf=0.
If T14=1 scabsf=T14*sqrt(bsf).
Compute scibsf=0.
If T15=1 scibsf=T15*sqrt(bsf).
Compute sstbsf=0.
If T36=1 sstbsf=T36*sqrt(bsf).

Compute srbelsf=0.
If T11=1 srbelsf=T11*sqrt(lsf).
Compute srcalsf=0.
If T14=1 srcalsf=T14*sqrt(lsf).
Compute srcilsf=0.
If T15=1 srcilsf=T15*sqrt(lsf).
Compute srstlsf=0.
If T36=1 srstlsf=T36*sqrt(lsf).

Compute nsrbelsf=n*srbelsf.
Compute nsrcalsf=n*srcalsf.
Compute nsrcilsf=n*srcilsf.
Compute nsrstlsf=n*srstlsf.

Compute sbsfbe10=0.
Compute sbsfbe20=0.
Compute sbsfbe30=0.
Compute sbsfbe40=0.
Compute sbsfbe50=0.
Compute sbsfbe60=0.
Compute sbsfbe70=0.
Compute sbsfbe71=0.
If town=11 and nghcde=10 sbsfbe10=sqrt(bsf).
If town=11 and nghcde=20 sbsfbe20=sqrt(bsf).
If town=11 and nghcde=30 sbsfbe30=sqrt(bsf).
If town=11 and nghcde=40 sbsfbe40=sqrt(bsf).
If town=11 and nghcde=50 sbsfbe50=sqrt(bsf).
If town=11 and nghcde=60 sbsfbe60=sqrt(bsf).
If town=11 and nghcde=70 sbsfbe70=sqrt(bsf).
If town=11 and nghcde=71 sbsfbe71=sqrt(bsf).
Compute sbsfca10=0.
Compute sbsfca20=0.
Compute sbsfca30=0.
Compute sbsfca40=0.
If town=14 and nghcde=10 sbsfca10=sqrt(bsf).
If town=14 and nghcde=20 sbsfca20=sqrt(bsf).
If town=14 and nghcde=30 sbsfca30=sqrt(bsf).
If town=14 and nghcde=40 sbsfca40=sqrt(bsf).
Compute sbsfci10=0.
Compute sbsfci15=0.
Compute sbsfci20=0.
Compute sbsfci25=0.
Compute sbsfci30=0.
Compute sbsfci40=0.
Compute sbsfci50=0.
Compute sbsfci60=0.
Compute sbsfci65=0.
Compute sbsfci71=0.
Compute sbsfci72=0.
Compute sbsfci80=0.
Compute sbsfci85=0.
Compute sbsfci90=0.
If town=15 and nghcde=10 sbsfci10=sqrt(bsf).
If town=15 and nghcde=20 sbsfci20=sqrt(bsf).
If town=15 and nghcde=30 sbsfci30=sqrt(bsf).
If town=15 and nghcde=40 sbsfci40=sqrt(bsf).
If town=15 and nghcde=50 sbsfci50=sqrt(bsf).
If town=15 and nghcde=60 sbsfci60=sqrt(bsf).
If town=15 and nghcde=65 sbsfci65=sqrt(bsf).
If town=15 and nghcde=71 sbsfci71=sqrt(bsf).
If town=15 and nghcde=72 sbsfci72=sqrt(bsf).
If town=15 and nghcde=80 sbsfci80=sqrt(bsf).
If town=15 and nghcde=85 sbsfci85=sqrt(bsf).
If town=15 and nghcde=90 sbsfci90=sqrt(bsf).
Compute sbsfst12=0.
Compute sbsfst21=0.
Compute sbsfst22=0.
Compute sbsfst36=0.
Compute sbsfst41=0.
Compute sbsfst50=0.
Compute sbsfst60=0.
If town=36 and nghcde=12 sbsfst12=sqrt(bsf).
If town=36 and nghcde=21 sbsfst21=sqrt(bsf).
If town=36 and nghcde=22 sbsfst22=sqrt(bsf).
If town=36 and nghcde=36 sbsfst36=sqrt(bsf).
If town=36 and nghcde=41 sbsfst41=sqrt(bsf).
If town=36 and nghcde=50 sbsfst50=sqrt(bsf).
If town=36 and nghcde=60 sbsfst60=sqrt(bsf).

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute nsrbsf=n*sqrt(bsf).
Compute nsrlsf=n*sqrt(lsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

If firepl>1 firepl = 1.
Compute nfirepl=n*firepl.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
Compute garnogar=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1. 
if gar=7 garnogar=1. 
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.

Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.

Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute nluxbsf=n*qualdlux*bsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

sel if town=14.
compute mv = (113445.5734	
+ 9347.220968*nbathsum
+ 9643.841352*nfirepl
- 18024.23435*highzonecicero
- 4199.099953*nsrage
+ 0.996211627*nbsfair
+ 36914.72315*nnobase
+ 183.8592281*nsrlsf
+ 3441.010197*nbsf89
+ 1.629146744*frabsf
+ 3840.251706*nbsf778
+ 491.6838651*sbsfbe40
- 927.948334*sbsfst22
+ 210.5967902*sbsfci71
- 110.4305122*sbsfst12
+ 1.851388066*masbsf
- 4816.467426*midzoneberwyn
- 886.136784*midzonestickney
- 13720.12666*highzonestickney
- 3420.944248*srfxmidblockberwyn
- 361.4372549*srfxhighblockberwyn
- 12814.81596*srfxhighblockcalumet
- 4926.616854*srfxhighblockcicero
- 4340.896387*srfxmidblockstickney
- 660.155094*srfxhighblockstickney
+ 1399.578077*nbsf234
+ 2551.494023*nbsf56
+ 1998.160992*nbsf34
+ 2480.580404*nbsf1112
+ 1955.2391*nbsf1095
- 3.474351434*stubsf
-14555.2882*garnogar
+ 7544.616896*npremrf
- 10807.49012*garage1
- 5529.931204*garage2
+ 51961.76798*nbaspart
+ 56681.58843*nbasfull
+ 5114.211334*biggar)*1.0.


save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv14.sav'/keep town pin mv.

