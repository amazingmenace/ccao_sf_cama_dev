                        *SPSS Regression.
			    *BARRINGTON AND PALATINE REGRESSION  2013.


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt10andregt29mergefcl2.sav'.
*select if (amount1>265000).
*select if (amount1<1900000).
*select if (multi<1).
*select if sqftb<9000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr1=12 and amount1>0)  year1=2012.
If (yr1=11 and amount1>0)  year1=2011.
If (yr1=10 and amount1>0)  year1=2010.  
If  (yr1=9 and amount1>0)  year1=2009.
If  (yr1=8 and amount1>0)  year1=2008.
If  (yr1=7 and amount1>0)  year1=2007. 
If  (yr1=6 and amount1>0)  year1=2006. 
If  (yr1=5 and amount1>0)  year1=2005.  
If  (yr1=4 and amount1>0)  year1=2004. 

*select if (year1>2007).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if  (pin=	1011090050000
or pin=	1011230280000
or pin=	1012060160000
or pin=	1013110100000
or pin=	1013200120000
or pin=	1013200290000
or pin=	1042020040000
or pin=	1151000240000
or pin=	1181030080000
or pin=	1212060030000
or pin=	1273040100000
or pin=	1152010090000
or pin=	1284120060000
or pin=	1341060110000
or pin=	2052060030000
or pin=	2081060030000
or pin=	2171100060000
or pin=	2184170100000
or pin=	2083020010000
or pin=	2083020040000
or pin=	2172030030000
or pin=	2172050030000
or pin=	2202000220000
or pin=	2202000390000
or pin=	2164050180000
or pin=	2212100020000
or pin=	2222050060000
or pin=	2224080150000
or pin=	2272020220000
or pin=	2092040290000
or pin=	2164190010000)  bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute lsf=sqftl.
Compute N=1.



COMPUTE FX =  cumfile78910111213.
If FX >= 5  FX = 5.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute bar=0.
compute pal=0.
if town=10 bar=1.
if town=29 pal=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000) + nghcde.

if (tnb= 10011 ) n=3.69.
if (tnb= 10012 ) n=4.17.
if (tnb= 10014 ) n=4.19.
if (tnb= 10021 ) n=6.79.
if (tnb= 10022 ) n=3.25.
if (tnb= 10023 ) n=7.93.
if (tnb= 10024 ) n=6.66.
if (tnb= 10025 ) n=5.75.
if (tnb= 10030 ) n=5.85.
if (tnb= 10031 ) n=8.86.
if (tnb= 10040 ) n=3.08.
if (tnb= 29011 ) n=5.35.
if (tnb= 29013 ) n=4.09.
if (tnb= 29022 ) n=3.21.
if (tnb= 29031 ) n=4.90.
if (tnb= 29032 ) n=4.17.
if (tnb= 29033 ) n=3.07.
if (tnb= 29041 ) n=6.02.
if (tnb= 29042 ) n=5.40.
if (tnb= 29043 ) n=3.49.
if (tnb= 29050 ) n=3.17.
if (tnb= 29060 ) n=3.21.
if (tnb= 29070 ) n=3.04.
if (tnb= 29080 ) n=3.12.
if (tnb= 29082 ) n=4.35.
if (tnb= 29083 ) n=4.38.
if (tnb= 29084 ) n=4.53.
if (tnb= 29090 ) n=4.06.
if (tnb= 29100 ) n=2.85.
if (tnb= 29110 ) n=5.66.
if (tnb= 29120 ) n=6.90.
if (tnb= 29130 ) n=6.99.
if (tnb= 29140 ) n=5.20.
if (tnb= 29170 ) n=3.45.
if (tnb= 29180 ) n=3.95.

compute sb10011=0.
compute sb10012=0.
compute sb10014=0.
compute sb10021=0.
compute sb10022=0.
compute sb10023=0.
compute sb10024=0.
compute sb10025=0.
compute sb10030=0.
compute sb10031=0.
compute sb10040=0.
compute sb29011=0.
compute sb29013=0.
compute sb29021=0. 
compute sb29022=0.
compute sb29031=0.
compute sb29032=0.
compute sb29033=0.
compute sb29041=0.
compute sb29042=0.
compute sb29043=0.
compute sb29050=0.
compute sb29060=0.
compute sb29070=0.
compute sb29080=0.
compute sb29082=0.
compute sb29083=0.
compute sb29084=0.
compute sb29090=0.
compute sb29100=0.
compute sb29110=0.
compute sb29120=0.
compute sb29130=0.
compute sb29140=0.
compute sb29160=0.
compute sb29170=0.
compute sb29180=0.


compute n10011=0.
compute n10012=0.
compute n10014=0.
compute n10021=0.
compute n10022=0.
compute n10023=0.
compute n10024=0.
compute n10025=0.
compute n10030=0.
compute n10031=0.
compute n10040=0.
compute n29011=0.
compute n29013=0.
compute n29021=0.
compute n29022=0. 
compute n29031=0.
compute n29032=0.
compute n29033=0.
compute n29041=0.
compute n29042=0.
compute n29043=0.
compute n29050=0.
compute n29060=0.
compute n29070=0.
compute n29080=0.
compute n29082=0.
compute n29083=0.
compute n29084=0.
compute n29090=0.
compute n29100=0.
compute n29110=0.
compute n29120=0.
compute n29130=0.
compute n29140=0.
compute n29160=0.
compute n29170=0.
compute n29180=0.


if (tnb= 10011 )  n10011=1.
if (tnb= 10012 )  n10012=1.
if (tnb= 10014 )  n10014=1.
if (tnb= 10021 )  n10021=1.
if (tnb= 10022 )  n10022=1.
if (tnb= 10023 )  n10023=1.
if (tnb= 10024 )  n10024=1.
if (tnb= 10025 )  n10025=1.
if (tnb= 10030 )  n10030=1.
if (tnb= 10031 )  n10031=1.
if (tnb= 10040 )  n10040=1.
if (tnb= 29011 )  n29011=1.
if (tnb= 29013 )  n29013=1.
if (tnb= 29021 )  n29021=1.
if (tnb= 29022 )  n29022=1.
if (tnb= 29031 )  n29031=1.
if (tnb= 29032 )  n29032=1.
if (tnb= 29033 )  n29033=1.
if (tnb= 29041 )  n29041=1.
if (tnb= 29042 )  n29042=1.
if (tnb= 29043 )  n29043=1.
if (tnb= 29050 )  n29050=1.
if (tnb= 29060 )  n29060=1.
if (tnb= 29070 )  n29070=1.
if (tnb= 29080 )  n29080=1.
if (tnb= 29082 )  n29082=1.
if (tnb= 29083 )  n29083=1.
if (tnb= 29084 )  n29084=1.
if (tnb= 29090 )  n29090=1.
if (tnb= 29100 )  n29100=1.
if (tnb= 29110 )  n29110=1.
if (tnb= 29120 )  n29120=1.
if (tnb= 29130 )  n29130=1.
if (tnb= 29140 )  n29140=1.
if (tnb= 29160 )  n29160=1.
if (tnb= 29170 )  n29170=1.
if (tnb= 29180 )  n29180=1.

if (tnb= 10011 ) sb10011=sqrt(bsf).
if (tnb= 10012 ) sb10012=sqrt(bsf).
if (tnb= 10014 ) sb10014=sqrt(bsf).
if (tnb= 10021 ) sb10021=sqrt(bsf).
if (tnb= 10022 ) sb10022=sqrt(bsf).
if (tnb= 10023 ) sb10023=sqrt(bsf).
if (tnb= 10024 ) sb10024=sqrt(bsf).
if (tnb= 10025 ) sb10025=sqrt(bsf).
if (tnb= 10030 ) sb10030=sqrt(bsf).
if (tnb= 10031 ) sb10031=sqrt(bsf).
if (tnb= 10040 ) sb10040=sqrt(bsf).
if (tnb= 29011 ) sb29011=sqrt(bsf).
if (tnb= 29013 ) sb29013=sqrt(bsf).
if (tnb= 29022 ) sb29022=sqrt(bsf).
if (tnb= 29031 ) sb29031=sqrt(bsf).
if (tnb= 29032 ) sb29032=sqrt(bsf).
if (tnb= 29033 ) sb29033=sqrt(bsf).
if (tnb= 29041 ) sb29041=sqrt(bsf).
if (tnb= 29042 ) sb29042=sqrt(bsf).
if (tnb= 29043)  sb29043=sqrt(bsf).
if (tnb= 29050 ) sb29050=sqrt(bsf).
if (tnb= 29060 ) sb29060=sqrt(bsf).
if (tnb= 29070 ) sb29070=sqrt(bsf).
if (tnb= 29080 ) sb29080=sqrt(bsf).
if (tnb= 29082 ) sb29082=sqrt(bsf).
if (tnb= 29083 ) sb29083=sqrt(bsf).
if (tnb= 29084 ) sb29084=sqrt(bsf).
if (tnb= 29090 ) sb29090=sqrt(bsf).
if (tnb= 29100 ) sb29100=sqrt(bsf).
if (tnb= 29110 ) sb29110=sqrt(bsf).
if (tnb= 29120 ) sb29120=sqrt(bsf).
if (tnb= 29130 ) sb29130=sqrt(bsf).
if (tnb= 29140 ) sb29140=sqrt(bsf).
if (tnb= 29170 ) sb29170=sqrt(bsf).
if (tnb= 29180 ) sb29180=sqrt(bsf).



Compute lowzonebar=0.
if town=10 and  (nghcde=12 or nghcde=14 or nghcde=21 or nghcde=22
 or nghcde=23  or nghcde=24 or nghcde=30 or nghcde=31 or nbhcde=40) lowzonebar=1.                         	

Compute midzonebar=0.
if town=10 and (nghcde=11 or nghcde=25) midzonebar=1.

Compute srfxlowblockbar=0.
if lowzonebar=1 srfxlowblockbar=srfx*lowzonebar.

Compute srfxmidblockbar=0.
if midzonebar=1 srfxmidblockbar=srfx*midzonebar.

Compute midzonepal=0.
if town=29 and (nghcde=22 or nghcde=50 or nghcde=100) midzonepal=1.

Compute lowzonepal=0.
if (town=29  and  midzonepal=1)   lowzonepal=1.  
                  	
Compute srfxlowblockpal=0.
if lowzonepal=1 srfxlowblockpal=srfx*lowzonepal.

Compute srfxmidblockpal=0.
if midzonepal=1 srfxmidblockpal=srfx*midzonepal.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.


Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.


if class=95 and tnb=10031  	lsf=7375.
if class=95 and tnb=29021  	lsf=2340.
if class=95 and tnb=29022  	lsf=2665.
if class=95 and tnb=29033  	lsf=3298.
if class=95 and tnb=29043  	lsf=2988.
if class=95 and tnb=29050  	lsf=1179.
if class=95 and tnb=29060  	lsf=2244.
if class=95 and tnb=29070  	lsf=1760.
if class=95 and tnb=29080  	lsf=2516.
if class=95 and tnb=29084  	lsf=2630.
if class=95 and tnb=29090  	lsf=2887.
if class=95 and tnb=29160 	lsf=2593.
if class=95 and tnb=29170 	lsf=2340.


compute nsrpall=0.
compute nsrbarl=0.
if pal=1 nsrpall=n*pal*sqrt(lsf).
if bar=1 nsrbarl=n*bar*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrpalb=n*pal*sqrt(bsf).
Compute nsrbarb=n*bar*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.

select if town=29.
compute mv =(171168.3786	
+ 0.858674715*nbsfair
+ 3282.763239*nbathsum
+ 44089.8697*biggar
+ 2792.522482*sb10012
+ 85.61852289*masbsf
+ 1824.105465*sb10021
+ 93.14937862*nsrpall
- 5942.785183*nsrage
+ 639.2184312*nsrbarb
+ 444.3861708*nsrpalb
+ 76.80766528*nbsf56
- 1549.870887*sb29130
+ 2002.60969*sb10014
+ 1149.489084*sb29042
+ 9471.20414*nsiteben
+ 547.561928*sb29080
+ 461.8962895*sb29060
- 10396.10347*SRFX
+ 2928.889035*nfirepl
+ 73.96416332*frmsbsf
+ 50.23865609*frastbsf
+ 641.7665267*nbsf34
+ 6494.754938*nbasfull
+ 101.8858861*nbsf234
- 1115.192735*sb10023
- 413.430936*sb29022
+ 855.6596687*sb29170
+ 21.26013408*frabsf
- 1957.64095*sb10025
+ 74.73707281*nbsf778
+ 27696.55513*garage2
+ 18073.50051*garage1
- 26071.45909*srfxmidblockbar
- 3236.556407*srfxmidblockpal
+ 11.17469808*nsrbarl
+ 2463.918071*nbaspart
- 1276.068401*nnobase
+ 709.5651655*sb10011
+ 427.7850004*sb10022
+ 616.3962874*sb29013
+ 158.5846574*sb29090
- 11697.967*midzonepal)*1.0.

* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=29.
*sel if puremarket=1 and yr1=12.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv29.sav'/keep town pin mv.


