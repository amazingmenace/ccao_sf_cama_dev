                                  *SPSS Regression.
		        	                    *SCHAUMBURG  2013.

***Split block 07-28-313-001 thru 010 is in nbhd 112 should be in 040.
Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt35mergefcl2.sav'.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004. 

*select if (amount1 > 100000).
*select if (amount1 < 990000).
*select if year1 > 2007.

compute n=1.
Compute bs=0.
if (pin=	7091050180000
or pin=	7184120110000
or pin=	7141050140000
or pin=	7153050130000
or pin=	7162060170000
or pin=	7212080220000
or pin=	7311040010000
or pin=	7312070260000
or pin=	7313050190000
or pin=	7174060040000
or pin=	7202170220000
or pin=	7203110170000
or pin=	7203160220000
or pin=	7204030320000
or pin=	7204040160000
or pin=	7213000370000
or pin=	7213010520000
or pin=	7213090570000
or pin=	7292040110000
or pin=	7292100120000
or pin=	7341170250000
or pin=	7223160470000
or pin=	7362160060000
or pin=	7343170270000
or pin=	7163210200000
or pin=	7151070120000
or pin=	7283070170000
or pin=	7272110400000
or pin=	7012000220000
or pin=	7074040100000
or pin=	7092040540000
or pin=	7092120270000
or pin=	7092130130000
or pin=	7092180260000)  bs=1.
*select if bs=0.

COMPUTE FX = cumfile78910111213.
if FX >= 7   FX = 7.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute n=1.
compute bsf=sqftb.
compute nbsf=n*bsf.
Compute lsf=sqftl.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute tnb=(town*1000) + nghcde.

if (tnb=35011) n=2.12.
if (tnb=35012) n=2.95.
if (tnb=35020) n=1.87.
if (tnb=35030) n=1.90.
if (tnb=35031) n=2.17.
if (tnb=35032) n=1.58.
if (tnb=35035) n=3.85.
if (tnb=35040) n=2.18.
if (tnb=35060) n=4.60.
if (tnb=35065) n=2.67.
if (tnb=35071) n=2.75.
if (tnb=35072) n=3.22.
if (tnb=35074) n=2.62.
if (tnb=35075) n=3.39.
if (tnb=35080) n=2.40.
if (tnb=35085) n=2.40.
if (tnb=35090) n=2.76.
if (tnb=35091) n=3.35.
if (tnb=35101) n=2.99.
if (tnb=35102) n=1.90.
if (tnb=35103) n=1.70.
if (tnb=35104) n=1.72.
if (tnb=35105) n=1.35.
if (tnb=35106) n=2.46.
if (tnb=35107) n=1.76.
if (tnb=35108) n=1.37.
if (tnb=35109) n=1.97.
if (tnb=35110) n=3.57.
if (tnb=35111) n=3.25.
if (tnb=35112) n=3.74.
if (tnb=35113) n=3.00.
if (tnb=35114) n=2.98.
if (tnb=35115) n=2.50.
if (tnb=35116) n=3.25.
if (tnb=35117) n=5.45.
if (tnb=35140) n=1.79.
if (tnb=35150) n=4.67.
if (tnb=35151) n=5.45.
if (tnb=35160) n=2.46.
if (tnb=35170) n=1.99.



Compute sb35011=0.
Compute sb35012=0.
Compute sb35020=0.
Compute sb35030=0.
Compute sb35031=0.
Compute sb35032=0.
Compute sb35035=0.
Compute sb35040=0.
Compute sb35060=0.
Compute sb35065=0.
Compute sb35071=0.
Compute sb35072=0.
Compute sb35074=0.
Compute sb35075=0.
Compute sb35080=0.
Compute sb35085=0.
Compute sb35090=0.
Compute sb35091=0.
Compute sb35101=0.
Compute sb35102=0.
Compute sb35103=0.
Compute sb35104=0.
Compute sb35105=0.
Compute sb35106=0.
Compute sb35107=0.
Compute sb35108=0.
Compute sb35109=0.
Compute sb35110=0.
Compute sb35111=0.
Compute sb35112=0.
Compute sb35113=0.
Compute sb35114=0.
Compute sb35115=0.
Compute sb35116=0.
Compute sb35117=0.
Compute sb35140=0.
Compute sb35150=0.
Compute sb35151=0.
Compute sb35160=0.
Compute sb35170=0.


compute n35011=0.
compute n35012=0.
compute n35020=0.
compute n35030=0.
compute n35031=0.
compute n35032=0.
compute n35035=0.
compute n35040=0.
compute n35060=0.
compute n35065=0.
compute n35071=0.
compute n35072=0.
compute n35074=0.
compute n35075=0.
compute n35080=0.
compute n35085=0.
compute n35090=0.
compute n35091=0.
compute n35101=0.
compute n35102=0.
compute n35103=0.
compute n35104=0.
compute n35105=0.
compute n35106=0.
compute n35107=0.
compute n35108=0.
compute n35109=0.
compute n35110=0.
compute n35111=0.
compute n35112=0.
compute n35113=0.
compute n35114=0.
compute n35115=0.
compute n35116=0.
compute n35117=0.
compute n35140=0.
compute n35150=0.
compute n35151=0.
compute n35160=0.
compute n35170=0.

If (tnb=35011)  n35011=1.
If (tnb=35012)  n35012=1.
If (tnb=35020)  n35020=1.
if (tnb=35030)  n35030=1.
If (tnb=35031)  n35031=1.
If (tnb=35032)  n35032=1.
If (tnb=35035)  n35035=1.
if (tnb=35040)  n35040=1.
If (tnb=35060)  n35060=1.
If (tnb=35065)  n35065=1.
if (tnb=35071)  n35071=1.
If (tnb=35072)  n35072=1.
If (tnb=35074)  n35074=1.
If (tnb=35075)  n35075=1.
if (tnb=35080)  n35080=1.
If (tnb=35085)  n35085=1.
If (tnb=35090)  n35090=1.
If (tnb=35091)  n35091=1.
If (tnb=35101)  n35101=1.
If (tnb=35102)  n35102=1.
If (tnb=35103)  n35103=1.
If (tnb=35104)  n35104=1.
If (tnb=35105)  n35105=1.
If (tnb=35106)  n35106=1.
If (tnb=35107)  n35107=1.
If (tnb=35108)  n35108=1.
If (tnb=35109)  n35109=1.
If (tnb=35110)  n35110=1.
If (tnb=35111)  n35111=1.
If (tnb=35112)  n35112=1.
If (tnb=35113)  n35113=1.
If (tnb=35114)  n35114=1.
If (tnb=35115)  n35115=1.
If (tnb=35116)  n35116=1.
If (tnb=35117)  n35117=1.
If (tnb=35140)  n35140=1.
If (tnb=35150)  n35150=1.
If (tnb=35151)  n35151=1.
If (tnb=35160)  n35160=1.
If (tnb=35170)  n35170=1.


Compute n95=0.
If (tnb=35012) and class=95 n95=2.22.
if (tnb=35020) and class=95 n95=2.12. 
If (tnb=35030) and class=95 n95=3.07.
If (tnb=35032) and class=95 n95=1.53.
If (tnb=35040) and class=95 n95=1.54.
If (tnb=35065) and class=95 n95=1.76.
if (tnb=35071) and class=95 n95=2.04.
if (tnb=35072) and class=95 n95=1.98.
if (tnb=35074) and class=95 n95=2.07.
if (tnb=35080) and class=95 n95=1.97.
if (tnb=35090) and class=95 n95=1.96.
if (tnb=35101) and class=95 n95=2.69.
if (tnb=35102) and class=95 n95=1.61.
if (tnb=35103) and class=95 n95=1.58.
if (tnb=35104) and class=95 n95=1.67.
if (tnb=35105) and class=95 n95=1.11.
if (tnb=35106) and class=95 n95=2.26.
if (tnb=35107) and class=95 n95=1.69.
if (tnb=35108) and class=95 n95=1.35.
if (tnb=35109) and class=95 n95=1.32.
if (tnb=35116) and class=95 n95=2.92.
if (tnb=35140) and class=95 n95=1.53.
if (tnb=35160) and class=95 n95=2.32.
if (tnb=35170) and class=95 n95=1.51.

compute no95=1.
if class=95 no95=0.
Compute nbsf95=n95*sqrt(bsf).

Compute bsfs=sqftb*no95.
If (tnb=35011)  sb35011=sqrt(bsfs).
If (tnb=35012)  sb35012=sqrt(bsfs).
If (tnb=35020)  sb35020=sqrt(bsfs).
if (tnb=35030)  sb35030=sqrt(bsfs).
If (tnb=35031)  sb35031=sqrt(bsfs).
If (tnb=35032)  sb35032=sqrt(bsfs).
If (tnb=35035)  sb35035=sqrt(bsfs).
if (tnb=35040)  sb35040=sqrt(bsfs).
If (tnb=35060)  sb35060=sqrt(bsfs).
If (tnb=35065)  sb35065=sqrt(bsfs).
if (tnb=35071)  sb35071=sqrt(bsfs).
If (tnb=35072)  sb35072=sqrt(bsfs).
If (tnb=35074)  sb35074=sqrt(bsfs).
If (tnb=35075)  sb35075=sqrt(bsfs).
if (tnb=35080)  sb35080=sqrt(bsfs).
If (tnb=35085)  sb35085=sqrt(bsfs).
If (tnb=35090)  sb35090=sqrt(bsfs).
If (tnb=35091)  sb35091=sqrt(bsfs).
If (tnb=35101)  sb35101=sqrt(bsfs).
If (tnb=35102)  sb35102=sqrt(bsfs).
If (tnb=35103)  sb35103=sqrt(bsfs).
If (tnb=35104)  sb35104=sqrt(bsfs).
If (tnb=35105)  sb35105=sqrt(bsfs).
If (tnb=35106)  sb35106=sqrt(bsfs).
If (tnb=35107)  sb35107=sqrt(bsfs).
If (tnb=35108)  sb35108=sqrt(bsfs).
If (tnb=35109)  sb35109=sqrt(bsfs).
If (tnb=35110)  sb35110=sqrt(bsfs).
If (tnb=35111)  sb35111=sqrt(bsfs).
If (tnb=35112)  sb35112=sqrt(bsfs).
If (tnb=35113)  sb35113=sqrt(bsfs).
If (tnb=35114)  sb35114=sqrt(bsfs).
If (tnb=35115)  sb35115=sqrt(bsfs).
If (tnb=35116)  sb35116=sqrt(bsfs).
If (tnb=35117)  sb35117=sqrt(bsfs).
If (tnb=35140)  sb35140=sqrt(bsfs).
If (tnb=35150)  sb35150=sqrt(bsfs).
If (tnb=35151)  sb35151=sqrt(bsfs).
If (tnb=35160)  sb35160=sqrt(bsfs).
If (tnb=35170)  sb35170=sqrt(bsfs).


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95 cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute lowzoneschaumburg=0.
if town=35 and  (nghcde=12 or nghcde=35 or nghcde=60 or nghcde=71 or nghcde=72 or nghcde=74
or nghcde=75 or nghcde=90 or nghcde =91 or nghcde=101 or nghcde=107 or nghcde=110 or nghcde=112
or nghcde=113 or nghcde=114 or nghcde=116 or nghcde=117 or nghcde=140 or nghcde=150
or nghcde=151 or nghcde=160)  lowzoneschaumburg=1.                         	

Compute midzoneschaumburg=0.
if town=35 and (nghcde=11 or nghcde=20 or nghcde=31 
or nghcde=32 or nghcde=40 or nghcde=65 or nghcde=80 or nghcde=85 or nghcde=102 or nghcde=103
or nghcde=104 or nghcde=106 or nghcde=111 or nghcde=115 or nghcde=170)    midzoneschaumburg=1.          
 
Compute highzoneschaumburg=0.
if town=35 and (nghcde=30  or nghcde=105 
or nghcde=108 or nghcde=109)   highzoneschaumburg=1.

Compute srfxlowblockschaumburg=0.
if lowzoneschaumburg=1 srfxlowblockschaumburg=srfx*lowzoneschaumburg.

Compute srfxmidblockschaumburg=0.
if midzoneschaumburg=1 srfxmidblockschaumburg=srfx*midzoneschaumburg.

Compute srfxhighblockschaumburg=0.
if highzoneschaumburg=1 srfxhighblockschaumburg=srfx*highzoneschaumburg.

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.

Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.

Compute lsf=sqftl.

if class=95 and tnb=35012   lsf=5594.
if class=95 and tnb=35020   lsf=3491.
if class=95 and tnb=35030   lsf=3146.
if class=95 and tnb=35032   lsf=2109.
if class=95 and tnb=35040   lsf=4047.
if class=95 and tnb=35065   lsf=5413.
if class=95 and tnb=35071   lsf=2365.	
if class=95 and tnb=35072   lsf=1947.
if class=95 and tnb=35074   lsf=3260.
if class=95 and tnb=35080   lsf=2314.
if class=95 and tnb=35090   lsf=2406.
if class=95 and tnb=35101   lsf=2962.
if class=95 and tnb=35102   lsf=2538.
if class=95 and tnb=35103   lsf=3160.	
if class=95 and tnb=35104   lsf=4585.
if class=95 and tnb=35105   lsf=1400.
if class=95 and tnb=35106   lsf=3535.
if class=95 and tnb=35107   lsf=2992.
if class=95 and tnb=35108   lsf=1998.
if class=95 and tnb=35109   lsf=1800.
if class=95 and tnb=35116   lsf=2255.
if class=95 and tnb=35140   lsf=670.
if class=95 and tnb=35160   lsf=4092.
if class=95 and tnb=35170   lsf=4072.	 	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.
*select if year1 >= 2012.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

compute mv = (195426.095279	
+ 5286.004201*nbathsum
+ 2.212713*nbsfair
+ 72275.549273*biggar
+ 805.476632*nbsf95
+ 13880.141851*garage2
+ 15.103335*masbsf
+ 7530.464285*nnobase
+ 7962.387562*garage1
+ 1199.524728*nbsf778
- 6640.998033*srfxhighblockschaumburg
- 10434.441232*nsrage
+ 77.431420*nsrlsf
+ 2307.633614*nfirepl
+ 1071.460405*nbsf234
+ 1236.397873*nbsf34
+ 2119.266742*nbsf89
+ 15404.210795*nbnum
+ 1488.383942*sb35150
+ 1256.436837*sb35075
- 12.071927*frabsf
+ 640.7154253*sb35020
+ 2191.339616*sb35140
+ 517.5038029*sb35012
+ 1264.250339*sb35110
+ 1012.978443*sb35116
+ 1193.549732*sb35011
+ 993.2265222*sb35040
- 1826.875513*srfxmidblockschaumburg
+ 1316.643963*sb35085
+ 1147.68574*sb35074
+ 1065.4966*sb35170
+ 947.7461561*sb35080
+ 1159.306769*sb35071
+ 913.9504366*sb35112
+ 846.0691018*sb35090
+ 748.0568299*sb35072
+ 975.8029219*sb35113
+ 935.6688254*sb35115
+ 602.3898374*sb35114
- 2224.849699*srfxlowblockschaumburg
+ 18514.659*nbasfull
+ 17465.54471*nbaspart
+ 5414.863793*nsiteben
+ 7.216903237*frastbsf)*1.0.

*RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=35.
*sel if puremarket=1 and yr1=12.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv35.sav'/keep town pin mv.

   
