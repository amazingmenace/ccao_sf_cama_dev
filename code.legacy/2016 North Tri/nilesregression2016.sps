                     *SPSS Regression.
                     *Niles Regression 2016.

get file = 'C:\Users\daaaron\documents\regt24jul11.sav'.
select if (amount1>100000).
select if (amount1<1500000).
select if (multi<1).
select if sqftb<7000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=15 and amount1>0) year1=2015.
If (yr=14 and amount1>0) year1=2014.
If (yr=13 and amount1>0) year1=2013.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.  

select if (year1>2010).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
*if (pin=	10303090200000
or pin=	10303090220000
or pin=	10314020200000
or pin=	10072000250000
or pin=	10182170010000
or pin=	10182170240000
or pin=	10182170250000
or pin=	10072010230000
or pin=	10073150430000
or pin=	10073160140000
or pin=	10073160170000
or pin=	10074001250000
or pin=	10074010350000
or pin=	10074010580000
or pin=	10074030300000
or pin=	10074070210000
or pin=	10074090570000
or pin=	10074090660000
or pin=	10074110420000
or pin=	10074120280000
or pin=	10071040040000
or pin=	10071040050000
or pin=	10071040070000
or pin=	10071040130000
or pin=	10071040200000
or pin=	10071050020000
or pin=	10071050130000
or pin=	10071050150000
or pin=	10071050170000
or pin=	10071060030000
or pin=	10071060060000
or pin=	10071070070000
or pin=	10071070230000
or pin=	10073000150000
or pin=	10073030010000
or pin=	10073050020000
or pin=	10073050030000
or pin=	10073070010000
or pin=	10073070090000
or pin=	10093120280000
or pin=	10153000580000
or pin=	10153180120000
or pin=	10153240080000
or pin=	10161040350000
or pin=	10161150450000
or pin=	10161180430000
or pin=	10161190560000
or pin=	10161210400000
or pin=	10163040750000
or pin=	10163070680000
or pin=	10163130210000
or pin=	10163130240000
or pin=	10163130270000
or pin=	10163220330000
or pin=	10163250290000
or pin=	10163250530000
or pin=	10163290310000
or pin=	10164020190000
or pin=	10164150710000
or pin=	10164170550000
or pin=	10164210390000
or pin=	10164230610000
or pin=	10164280040000
or pin=	10164330410000
or pin=	10172050160000
or pin=	10172110430000
or pin=	10172140500000
or pin=	10172160030000
or pin=	10173030150000
or pin=	10173110390000
or pin=	10173130230000
or pin=	10173130260000
or pin=	10173170240000
or pin=	10174010430000
or pin=	10174040080000
or pin=	10174050140000
or pin=	10174060050000
or pin=	10174060070000
or pin=	10174060510000
or pin=	10174080420000
or pin=	10174080590000
or pin=	10174110420000
or pin=	10174140410000
or pin=	10174160480000
or pin=	10174170560000
or pin=	10174180430000
or pin=	10174190480000
or pin=	10174190490000
or pin=	10174210470000
or pin=	10174220510000
or pin=	10174230480000
or pin=	10174230490000
or pin=	10174300170000
or pin=	10181030040000
or pin=	10181040100000
or pin=	10181040130000
or pin=	10181050040000
or pin=	10181050230000
or pin=	10181070290000
or pin=	10181090230000
or pin=	10181100160000
or pin=	10181100440000
or pin=	10181100600000
or pin=	10181100640000
or pin=	10181110310000
or pin=	10181110320000
or pin=	10181110390000
or pin=	10181110430000
or pin=	10181120130000
or pin=	10181130170000
or pin=	10181140240000
or pin=	10181140320000
or pin=	10181150250000
or pin=	10181200010000
or pin=	10182010480000
or pin=	10182040130000
or pin=	10183010150000
or pin=	10183040010000
or pin=	10183060230000
or pin=	10183070110000
or pin=	10183070190000
or pin=	10183070340000
or pin=	10184050160000
or pin=	10184050240000
or pin=	10184130230000
or pin=	10184140130000
or pin=	10201100020000
or pin=	10201100520000
or pin=	10201220640000
or pin=	10201220840000
or pin=	10202060580000
or pin=	10202070230000
or pin=	10202100190000
or pin=	10202110490000
or pin=	10202130290000
or pin=	10202270430000
or pin=	10202290250000
or pin=	10204010490000
or pin=	10204030540000
or pin=	10204070310000
or pin=	10204070350000
or pin=	10204110330000
or pin=	10204130200000
or pin=	10204140200000
or pin=	10204140540000
or pin=	10204160010000
or pin=	10204190420000
or pin=	10204240050000
or pin=	10204260150000
or pin=	10204260170000
or pin=	10211060330000
or pin=	10211060620000
or pin=	10211070170000
or pin=	10211090150000
or pin=	10211110590000
or pin=	10211130250000
or pin=	10211160410000
or pin=	10211160480000
or pin=	10211160490000
or pin=	10211200340000
or pin=	10211210430000
or pin=	10211230220000
or pin=	10211240100000
or pin=	10211250470000
or pin=	10211250540000
or pin=	10211260160000
or pin=	10211310460000
or pin=	10211310690000
or pin=	10212050600000
or pin=	10212070670000
or pin=	10212080620000
or pin=	10212120600000
or pin=	10212160580000
or pin=	10212170160000
or pin=	10212250780000
or pin=	10212250820000
or pin=	10213030630000
or pin=	10213060130000
or pin=	10213060220000
or pin=	10213080110000
or pin=	10213120770000
or pin=	10213150300000
or pin=	10213180240000
or pin=	10213200140000
or pin=	10213200220000
or pin=	10213240050000
or pin=	10213240130000
or pin=	10214010530000
or pin=	10214050120000
or pin=	10214110180000
or pin=	10214120060000
or pin=	10214130210000
or pin=	10214140580000
or pin=	10214140660000
or pin=	10223090210000
or pin=	10223100410000
or pin=	10223120380000
or pin=	10223130030000
or pin=	10223180170000
or pin=	10223200040000
or pin=	10223200590000
or pin=	10223260140000
or pin=	10223290120000
or pin=	10223290160000
or pin=	10281010200000
or pin=	10281020280000
or pin=	10281020310000
or pin=	10281020420000
or pin=	10281040390000
or pin=	10281080210000
or pin=	10281090280000
or pin=	10281100550000
or pin=	10281110250000
or pin=	10281130090000
or pin=	10281230020000
or pin=	10281300310000
or pin=	10281310350000
or pin=	10282000340000
or pin=	10282030260000
or pin=	10282180020000
or pin=	10082010160000
or pin=	10201020180000
or pin=	10201020350000
or pin=	10201120030000
or pin=	10201120470000
or pin=	10201130030000
or pin=	10201130210000
or pin=	10201140020000
or pin=	10201150020000
or pin=	10201150030000
or pin=	10201150060000
or pin=	10201160330000
or pin=	10104060210000
or pin=	10104150110000
or pin=	10104210470000
or pin=	10104250580000
or pin=	10104260100000
or pin=	10142140450000
or pin=	10142150480000
or pin=	10142210200000
or pin=	10142260220000
or pin=	10142260460000
or pin=	10143070480000
or pin=	10143100450000
or pin=	10143100480000
or pin=	10144020090000
or pin=	10144050650000
or pin=	10144050730000
or pin=	10144080070000
or pin=	10144130430000
or pin=	10144130480000
or pin=	10144130520000
or pin=	10144150130000
or pin=	10144180250000
or pin=	10144220220000
or pin=	10151040140000
or pin=	10151050380000
or pin=	10151100500000
or pin=	10151110150000
or pin=	10151180360000
or pin=	10151190520000
or pin=	10151210230000
or pin=	10152000480000
or pin=	10152010380000
or pin=	10152040340000
or pin=	10152040540000
or pin=	10152060400000
or pin=	10152100450000
or pin=	10152100490000
or pin=	10152140070000
or pin=	10152150500000
or pin=	10152240270000
or pin=	10152240540000
or pin=	10152260060000
or pin=	10152260150000
or pin=	10152280170000
or pin=	10153210440000
or pin=	10153210450000
or pin=	10153270360000
or pin=	10153280050000
or pin=	10154020250000
or pin=	10154050010000
or pin=	10154150090000
or pin=	10154150420000
or pin=	10154230270000
or pin=	10154230360000
or pin=	10154230490000
or pin=	10154260040000
or pin=	10154280050000
or pin=	10154280500000
or pin=	10154290360000
or pin=	10154310500000
or pin=	10154320480000
or pin=	10221050340000
or pin=	10221070050000
or pin=	10222140070000
or pin=	10222140330000
or pin=	10094220070000
or pin=	10094220400000
or pin=	10162020230000
or pin=	10162030650000
or pin=	10162090580000
or pin=	10162230080000
or pin=	10162230320000
or pin=	10141030440000
or pin=	10141070110000
or pin=	10141080450000
or pin=	10141080580000
or pin=	10141140280000
or pin=	10141160560000
or pin=	10141190250000
or pin=	10141200360000
or pin=	10141200370000
or pin=	10141210490000
or pin=	10141240210000
or pin=	10141240490000
or pin=	10141290220000
or pin=	10142060060000
or pin=	10142060100000
or pin=	10142120150000
or pin=	10142200230000
or pin=	10142200390000
or pin=	10183080180000
or pin=	10183180250000
or pin=	10183200380000
or pin=	10191000950000
or pin=	10191070660000
or pin=	10191120940000
or pin=	10191250940000
or pin=	10193020590000
or pin=	10193020650000
or pin=	10193040600000
or pin=	10193080290000
or pin=	10193090610000
or pin=	10193150170000
or pin=	10193210160000
or pin=	10222010120000
or pin=	10222020420000
or pin=	10222050380000
or pin=	10222080440000
or pin=	10222100370000
or pin=	10222160300000
or pin=	10222160470000
or pin=	10222170420000
or pin=	10222180250000
or pin=	10222180570000
or pin=	10223030460000
or pin=	10223230040000
or pin=	10223240140000
or pin=	10223240280000
or pin=	10223320390000
or pin=	10224010270000
or pin=	10224040450000
or pin=	10224060200000
or pin=	10224060360000
or pin=	10224090100000
or pin=	10224090370000
or pin=	10224110230000
or pin=	10224120110000
or pin=	10224130170000
or pin=	10224130330000
or pin=	10224150530000
or pin=	10224170540000
or pin=	10224230160000
or pin=	10224250080000
or pin=	10231030330000
or pin=	10231090280000
or pin=	10231090410000
or pin=	10231100420000
or pin=	10231140350000
or pin=	10231180490000
or pin=	10231190500000
or pin=	10231240470000
or pin=	10231250230000
or pin=	10231270310000
or pin=	10231280570000
or pin=	10231290260000
or pin=	10231300130000
or pin=	10232010410000
or pin=	10232010460000
or pin=	10232010550000
or pin=	10232040270000
or pin=	10232040400000
or pin=	10232040470000
or pin=	10232050080000
or pin=	10232050560000
or pin=	10232080470000
or pin=	10232090430000
or pin=	10232110450000
or pin=	10232120700000
or pin=	10232150180000
or pin=	10232160620000
or pin=	10232180460000
or pin=	10232190450000
or pin=	10232210510000
or pin=	10232230090000
or pin=	10233010150000
or pin=	10233030150000
or pin=	10233040120000
or pin=	10233040150000
or pin=	10233040240000
or pin=	10233040250000
or pin=	10233060140000
or pin=	10233060460000
or pin=	10233100120000
or pin=	10233130420000
or pin=	10233180010000
or pin=	10233180160000
or pin=	10233260390000
or pin=	10234010450000
or pin=	10261040020000
or pin=	10261040460000
or pin=	10261070020000
or pin=	10261070180000
or pin=	10261100030000
or pin=	10261110320000
or pin=	10263040620000
or pin=	10263040650000
or pin=	10263210010000
or pin=	10263220210000
or pin=	10263220230000
or pin=	10271010360000
or pin=	10271020420000
or pin=	10271050290000
or pin=	10271050420000
or pin=	10271070320000
or pin=	10271070430000
or pin=	10271110110000
or pin=	10271110250000
or pin=	10271150540000
or pin=	10271190230000
or pin=	10271190300000
or pin=	10272030040000
or pin=	10272060320000
or pin=	10272110020000
or pin=	10272130030000
or pin=	10272130070000
or pin=	10272220280000
or pin=	10272250560000
or pin=	10272250580000
or pin=	10272280410000
or pin=	10272300540000
or pin=	10272310420000
or pin=	10272310560000
or pin=	10272320460000
or pin=	10273100360000
or pin=	10274050010000
or pin=	10274120540000
or pin=	10282100380000
or pin=	10282200180000
or pin=	10282220100000
or pin=	10282220410000
or pin=	10284000420000
or pin=	10284010570000
or pin=	10284030010000
or pin=	10284070120000
or pin=	10284080050000
or pin=	10284080350000
or pin=	10284100520000
or pin=	10284110220000
or pin=	10284110390000
or pin=	10284210190000
or pin=	10284260080000
or pin=	10301010120000
or pin=	10301010130000
or pin=	10301060500000
or pin=	10301110310000
or pin=	10301120230000
or pin=	10301120250000
or pin=	10301140290000
or pin=	10301160150000
or pin=	10301180010000
or pin=	10301250230000
or pin=	10301250580000
or pin=	10301250800000
or pin=	10301251270000
or pin=	10301251330000
or pin=	10303020420000
or pin=	10303030240000
or pin=	10303100040000
or pin=	10303120040000
or pin=	10263130430000
or pin=	10263140400000
or pin=	10263170440000
or pin=	10263170560000
or pin=	10273071220000
or pin=	10273071380000
or pin=	10273160210000
or pin=	10273190250000
or pin=	10273230100000
or pin=	10274210310000
or pin=	10274220310000
or pin=	10274270460000
or pin=	10274270480000
or pin=	10284240740000
or pin=	10331070300000
or pin=	10331070340000
or pin=	10331110240000
or pin=	10332000200000
or pin=	10332000630000
or pin=	10332020460000
or pin=	10332040210000
or pin=	10332040270000
or pin=	10332060480000
or pin=	10332060490000
or pin=	10332130300000
or pin=	10332140780000
or pin=	10332140870000
or pin=	10332170450000
or pin=	10332170490000
or pin=	10332200240000
or pin=	10332220600000
or pin=	10332240520000
or pin=	10332290340000
or pin=	10341010310000
or pin=	10341020070000
or pin=	10341140040000
or pin=	10341140110000
or pin=	10341160020000
or pin=	10341170110000
or pin=	10341210030000
or pin=	10341210290000
or pin=	10341220510000
or pin=	10341230230000
or pin=	10341250340000
or pin=	10341270010000
or pin=	10341270050000
or pin=	10341270070000
or pin=	10341270200000
or pin=	10342030090000
or pin=	10342040380000
or pin=	10342100560000
or pin=	10342100570000
or pin=	10342170080000
or pin=	10342240290000
or pin=	10343240300000
or pin=	10343240360000
or pin=	10343240390000
or pin=	10343240480000
or pin=	10351000390000
or pin=	10351080430000
or pin=	10353010570000
or pin=	10353020370000
or pin=	10353060330000
or pin=	10353070760000
or pin=	10353160690000
or pin=	10353200290000
or pin=	10353200350000
or pin=	10353210100000
or pin=	10353220340000
or pin=	10353280200000
or pin=	10354060260000
or pin=	10354070150000
or pin=	10354080210000
or pin=	10354080340000
or pin=	10354090330000
or pin=	10354140020000
or pin=	10354160060000
or pin=	10354160370000
or pin=	10354170340000
or pin=	10354190380000
or pin=	10354220570000
or pin=	10354260230000
or pin=	10334000370000
or pin=	10334010420000
or pin=	10334100420000
or pin=	10334240320000
or pin=	10334250150000
or pin=	10334250330000
or pin=	10334260380000
or pin=	10334320360000
or pin=	10334360120000
or pin=	10334370070000
or pin=	10334390060000
or pin=	10312150110000
or pin=	10321300220000
or pin=	10332150450000
or pin=	10332190150000
or pin=	10332310080000
or pin=	10343000360000
or pin=	10343060300000
or pin=	10343070490000
or pin=	10343070540000
or pin=	10343120370000
or pin=	10343120610000
or pin=	10343140290000
or pin=	10343150230000
or pin=	10343230180000
or pin=	10343230320000
or pin=	10343290040000
or pin=	10343290060000
or pin=	10143000570000
or pin=	10143040540000
or pin=	10143190110000
or pin=	10143190200000
or pin=	10143200340000
or pin=	10143220020000) bs=1.
select if bs=0.

compute tnb=(town*1000) + nghcde.
compute bsf=sqftb.
compute lsf=sqftl.
compute srbsf=sqrt(sqftb).
compute srage=sqrt(age).
compute srlsf=sqrt(sqftl).

*COMPUTE FX = cumfile13141516.
*If FX >= 8   FX = 8.
*COMPUTE SRFX = sqrt(fx).
*RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute n24013=0.
compute n24021=0.
compute n24022=0.
compute n24023=0.
compute n24031=0.
compute n24032=0.
compute n24051=0.
compute n24052=0.
compute n24060=0.
compute n24081=0.
compute n24101=0.
compute n24102=0.
compute n24103=0.
compute n24140=0.
compute n24150=0.
compute n24160=0.


compute sb24013=0.
compute sb24021=0.
compute sb24022=0.
compute sb24023=0.
compute sb24031=0.
compute sb24032=0.
compute sb24051=0.
compute sb24052=0.
compute sb24060=0.
compute sb24081=0.
compute sb24101=0.
compute sb24102=0.
compute sb24103=0.
compute sb24140=0.
compute sb24150=0.
compute sb24160=0.


if (tnb=24013) n=2.75.
if (tnb=24021) n=12.00.
if (tnb=24022) n=4.99.
if (tnb=24023) n=6.34.
if (tnb=24031) n=2.63.
if (tnb=24032) n=2.79.
if (tnb=24051) n=3.47.
if (tnb=24052) n=3.17.
if (tnb=24060) n=5.36.
if (tnb=24081) n=2.57.
if (tnb=24101) n=3.45.
if (tnb=24102) n=6.30.
if (tnb=24103) n=4.39.
if (tnb=24140) n=3.35.
if (tnb=24150) n=3.87.
if (tnb=24160) n=6.59.


if (tnb=24013) n24013=1.
if (tnb=24021) n24021=1.
if (tnb=24022) n24022=1.
if (tnb=24023) n24023=1.
if (tnb=24031) n24031=1.
if (tnb=24032) n24032=1.
if (tnb=24051) n24051=1.
if (tnb=24052) n24052=1.
if (tnb=24060) n24060=1.
if (tnb=24081) n24081=1.
if (tnb=24101) n24101=1.
if (tnb=24102) n24102=1.
if (tnb=24103) n24103=1.
if (tnb=24140) n24140=1.
if (tnb=24150) n24150=1.
if (tnb=24160) n24160=1.

if tnb=24013  sb24013=sqrt(bsf).
if tnb=24021  sb24021=sqrt(bsf).
if tnb=24022  sb24022=sqrt(bsf).
if tnb=24023  sb24023=sqrt(bsf).
if tnb=24031  sb24031=sqrt(bsf).
if tnb=24032  sb24032=sqrt(bsf).
if tnb=24051  sb24051=sqrt(bsf).
if tnb=24052  sb24052=sqrt(bsf).
if tnb=24060  sb24060=sqrt(bsf).
if tnb=24081  sb24081=sqrt(bsf).
if tnb=24101  sb24101=sqrt(bsf).
if tnb=24102  sb24102=sqrt(bsf).
if tnb=24103  sb24103=sqrt(bsf).
if tnb=24140  sb24140=sqrt(bsf).
if tnb=24150  sb24150=sqrt(bsf).
if tnb=24160  sb24160=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

*Compute midzoneniles=0.
*if town=24 and (nghcde=31 or nghcde=81 or nghcde=103)  midzoneniles=1.

*Compute highzoneniles=0.
*if town=24 and (nghcde=52 or nghcde=101 or nghcde=140)    highzoneniles=1.

*Compute lowzoneniles=0.
*If town=24 and (midzoneniles=0 and highzoneniles=0)  lowzoneniles=1.

*Compute srfxlowblockniles=0.
*if lowzoneniles=1 srfxlowblockniles=srfx*lowzoneniles.

*Compute srfxmidblockniles=0.
*if midzoneniles=1 srfxmidblockniles=srfx*midzoneniles.

*Compute srfxhighblockniles=0.
*if highzoneniles=1 srfxhighblockniles=srfx*highzoneniles.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.

Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.

Compute lsf=sqftl.

if class=95 and tnb=24031   lsf=1160.
if class=95 and tnb=24032   lsf=3100.
if class=95 and tnb=24051   lsf=1852.
if class=95 and tnb=24052   lsf=1018.
if class=95 and tnb=24081   lsf=2540.
if class=95 and tnb=24101   lsf=2156.
if class=95 and tnb=24103   lsf=3024.	
if class=95 and tnb=24140   lsf=2056.	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.

*select if (town=24 and year1>=2015).
*Table observation = b
                amount1
          /table = nghcde by amount1 + b      
		/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').



reg des = defaults cov
 	/var=amount1 nsrlsf nbsf234 nbsf56 nbsf778 nbsf89 nbsf1112
 	nbsf1095 nbsf34 nbasfull nbaspart nnobase nfirepl nbsfair nsiteben
		nrepabsf garage1 garage2 biggar nbathsum nprembsf nsrage masbsf frmsbsf
		frabsf stubsf frastbsf nbnum sb24013 sb24021 sb24022 sb24023 sb24031 sb24032
		sb24051 sb24052 sb24060 sb24081 sb24101 sb24102 sb24103 sb24140 sb24150 sb24160 
	 jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
  summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
  winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
  summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
  winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
  summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
  winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
  summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
  winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
  octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
  summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
  jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
  summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
		/dep=amount1
 	/method=stepwise
		/method=enter jantmar11cl234 winter1112cl234 winter1213cl234 winter1314cl234 winter1415cl234 summer11cl234
   summer12cl234 summer13cl234 summer14cl234 summer15cl234 octtdec15cl234 jantmar11cl56
   winter1112cl56 winter1213cl56 winter1314cl56 winter1415cl56 summer11cl56 summer12cl56
   summer13cl56 summer14cl56 summer15cl56 octtdec15cl56  jantmar11cl778 winter1112cl778
   winter1213cl778 winter1314cl778 winter1415cl778 summer11cl778 summer12cl778 summer13cl778
   summer14cl778 summer15cl778 octtdec15cl778 jantmar11cl89 winter1112cl89 winter1213cl89
   winter1314cl89 winter1415cl89 summer11cl89 summer12cl89 summer13cl89 summer14cl89
   summer15cl89 octtdec15cl89 jantmar11cl1112 winter1112cl1112 winter1213cl1112 winter1314cl1112 
   winter1415cl1112 summer11cl1112 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112
   octtdec15cl1112 jantmar11cl1095 winter1112cl1095 winter1213cl1095 winter1314cl1095 winter1415cl1095
   summer11cl1095 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 octtdec15cl1095
   jantmar11clsplt winter1112clsplt winter1213clsplt winter1314clsplt winter1415clsplt summer11clsplt summer12clsplt
   summer13clsplt summer14clsplt summer15clsplt octtdec15clsplt 
   /method=enter nbasfull nbaspart garage1 biggar nbathsum nfirepl 
  /method=enter nnobase garage1 biggar nbsfair nrepabsf sb24052 sb24150
		/save pred (pred) resid (resid).
   sort cases by tnb pin.	
   value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
  /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
  /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
  /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
  /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
  /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
*compute badsal=0.
*if perdif>30 badsal=1.
*if perdif<-30 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Niles'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).

execute.
