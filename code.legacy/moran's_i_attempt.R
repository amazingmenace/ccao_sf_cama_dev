temp <- cbind(subset(modeldata, trim=filter_setting_1)$centroid_x, subset(modeldata, trim=filter_setting_1)$centroid_y)
resid_inv <- as.matrix(dist(temp))
rm(temp)
resid_inv <- 1/resid_inv
diag(resid_inv) <- 0
resid_inv[is.infinite(resid_inv)] <- 0

morans_i=Moran.I(subset(modeldata, trim=filter_setting_1)$residuals, resid_inv)